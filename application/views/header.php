<!doctype html>
<html class="no-js" lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon" />

    <title><?php echo $title;?></title>

    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/styles/bootstrap.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/icon-font/css/sp-theme-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/styles/main.css">
    <script type="text/javascript" src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/499416/TweenLite.min.js"></script>
    <script type="text/javascript" src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/499416/EasePack.min.js"></script>
    <!-- <script type="text/javascript" src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/499416/demo.js"></script> -->
    <link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
    
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bdc117ac7a9470012145c1d&product=inline-share-buttons' async='async'></script>
    <style type="text/css">
        .device:hover{
            border-radius: 0px !important;
        }
        .device{
            background-color: transparent !important;
            border-color: transparent !important;
            border: 0px;
        }
      .sp-slick-imac{
         box-shadow: 8px 7px #c3ac6d, 1em 16px 0.4em #444440;
      }
      .bubbles {
         display: inline-block;
         font-family: arial;
         position: relative;
         color: #100c0c;
         font-weight: 900;
         text-shadow:0px 1px 0 #828282;
      }

      .bubbles h1 {
         position: relative;
         margin: 1em 0 0;
         font-family: 'Luckiest Guy', cursive;
         color: #fff;
         z-index: 2;
      }

      .individual-bubble {
         position: absolute;
         border-radius: 100%;
         bottom: 10px;
         background-color: #fff;
         z-index: 1;
      }

        .hit-the-floor {
            color: #fff;
            font-size: 12em;
            font-weight: bold;
            font-family: Helvetica;
            text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
        }

        .hit-the-floor {
            text-align: center;
        }
        #sp-primary-nav .nav_menu li a{
            font-size: 20px;
            font-weight: 550;
            letter-spacing: 2px;
        }
        .container-fluid{
            padding: 0px 15%;
        }
        .nav_menu li a{
            text-decoration: none !important;
        }
        a{
            text-decoration: none !important;
        }
        #sp-primary-nav .nav_menu li a {
            font-size: 21px !important;
            font-weight: 650 !important;
            letter-spacing: 1.2px !important;
        }
        @media screen and (max-width: 768px) {
            #sp-header-inner{
                padding: 0px 0% !important;
            }
        }
    </style>
</head>
<body>

<div id="sp-preloader"></div>
<div id="sp-top-scrolling-anchor"></div>

<a href="#sp-top-scrolling-anchor" class="sp-scroll-top">
    <span class="anno-text">Go to top</span>
    <i class="icon-angle-up"></i>
</a>

<!-- start header.html-->
<header id="sp-header" class="stuck-slidein">
    <div class="container-fluid" id="sp-header-inner">
        <a href="<?php echo base_url();?>" class="brand-logo">
            <h1><span>O</span>Engines</h1>
        </a>
        <nav id="sp-primary-nav">
            <ul class="nav_menu">
                <li>
                    <a href="<?php echo base_url();?>">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url('home/services/games');?>">Games</a>
                </li>
                <li>
                    <a href="<?php echo base_url('home/services/web');?>">Web</a>
                </li>
                <li><a href="<?php echo base_url('home/about');?>">About Us</a></li>
                <li><a href="<?php echo base_url('home/contact');?>">Contact Us</a></li>
            </ul>

            <a href="#" id="sp-mobile-nav-trigger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
        </nav>
        
    </div>
</header>

<!-- fullscreen mobile menu -->
<div id="sp-mobile-nav-container">
    <div class="overlay-inner-wrap">
        <nav>
            <ul class="nav_menu">
                <li class="current-menu-item"><a href="<?php echo base_url();?>">Home</a></li>
                <li class="current-menu-item"><a href="<?php echo base_url('home/services/games');?>">Games</a></li>
                <li class="current-menu-item"><a href="<?php echo base_url('home/services/web');?>">Web</a></li>
                <li class="current-menu-item"><a href="<?php echo base_url('home/about');?>">About Us</a></li>
                <li class="current-menu-item"><a href="<?php echo base_url('home/contact');?>">Contact Us</a></li>
            </ul>
        </nav>
        <div class="sp-soc-icons">
            <a href="https://www.facebook.com/oe.studio091/" title="facebook" target="_blank"><i class="icon-facebook"></i></a>
            <a href="https://play.google.com/store/apps/dev?id=7618782448745223948&hl=en" title="twitter" target="_blank"><i class="icon-ion-android-playstore"></i></a>
        </div>
    </div>
</div>

<div id="sp-mobile-nav-bg"></div>
<!-- end header.html-->

<div id="sp-wrapper">
<!-- start page-content -->


