<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=1'?>">


<!-- start page-content -->
<br><br><br>
<section class="sp-section bg-color-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="sp-title-block"><h3>Popular Articles</h3></div>
                <p>Until yellow dear a that this far squirrel far this and via woodpecker impartially gorilla more reined underneath.</p>
                <div class="sp-faq-articles">
                    <ul>
                    <?php foreach ($all_faq_topic as $key => $value) { ?>
                        <li><a href="javascript:void(0)" onclick="openCity('<?php echo $value['id'] ?>')"><?php echo $value['faq_topic'] ?></a></li>
                    <?php } ?>      
                    </ul>
                </div>
            </div>

            <?php foreach ($all_faq_topic as $key2 => $value2) { ?>
            
            <div id="<?php echo $value2['id'] ?>" class="col-lg-8 col-md-8 city" style="display:<?php echo ($key2 == 0) ? '' : 'none';?>">
                <div class="sp-title-block"><h3>Technical Questions </h3></div>
                <div class="sp-faq-list">
                    <?php 
                                $this->db->select('*');
                                $this->db->where('topic_id', $value2['id']);
                        $data = $this->db->get('faq');
                        $faq = $data->result_array();
                        foreach ($faq as $key3 => $value3) { 
                    ?>
                    
                        <div class="card sp-faq-card <?php echo ($key3 == 0) ? 'card-open' : '' ?>">
                            <div class="card-header">
                                <h5 class="card-title"><?php echo $value3['faq_q'] ;?></h5>
                                <div class="toggle-icon"><i class="icon-ion-ios-plus-empty"></i></div>
                            </div>
                            <div class="card-contents collapse show">
                                <div class="card-contents-inner">
                                    <p><?php echo $value3['faq_a'] ;?></p>
                                </div>
                            </div>
                        </div>

                    <?php }  ?>
                    
                </div>
            </div>

            <?php } ?>
        </div>
    </div>
</section>

<script>
function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(cityName).style.display = "block";  
}
</script>
<!-- end page-content -->
</div>
