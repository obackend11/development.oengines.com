<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=1'?>">


<!-- start page-content -->
<br><br><br>
<section class="sp-section text-center" id="sp-services">
    <div class="container">
        <div class="sp-services-container row wow sequenced fx-fadeIn">
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 style="margin-bottom:5px;text-align: left">Legal Terms & Privacy Policy</h3>
                <p style="margin-bottom:5px;text-align: left"> COPYRIGHT & TRADEMARK,</p>
                <p style="margin-bottom:5px;text-align: left"> (c) 2006-14 Oengines,Inc.</p>
            </div>        
            
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">   
                <p style="text-align: left">             
                No portion of the Oengines, Inc. WWW sites, which includes this site, may be copied or redistributed in any manner without the express written consent of Oengines, Inc.Oengines, OenginesTalk, all Oengines games, which includes Teen Patti by Oengines, and all other Oengines software title names and their respective logos are trademarks of Oengines Inc. Google, Google Talk, Google Play, Gmail and all other Google titles are the trademarks or registered trademarks of Google Inc or its subsidiaries. All other marks and product names may be the trademarks or registered trademarks of their respective owners.
                </p>
                <h4 style="text-align: left">TERMS OF SERVICE</h4>
                <p style="text-align: left"> 
                The terms of this agreement (“Terms of Service”) govern the relationship between you (“The User”) and Oengines, Inc. (“Oengines” or “We”) regarding your use of Oengines, Inc’s software titles including OenginesTalk, social games and related services and Oengines websites (the “Service”). Oengines reserves the right to edit these Terms of Service and policies at any time.
                </p>

                <h4 style="text-align: left">PRIVACY POLICY</h4>
                <p style="text-align: left"> 
                The Privacy Policy describes how we treat your personal information when you use OenginesTalk and other applications including social games and services from Oengines Inc. At Oengines Inc., we respect your right to privacy. We will never sell or otherwise pass on your name, e-mail or other information to a third party without your consent except for when it is required to provide you the service you are using.What information do we collect? When you use software or services from Oengines, we collect some personal information to allow us to provide you the service, fulfill your requirement and provide you the customer support. This information may include your account information for Google, Apple, Facebook and other networks, your name, address, phone number, your device information, registration data, email address, and any other information that you entered while using our software and services. We do not store the credit card information.How is this information used? Only Oengines has access to any such information. We will not give or sell your private information to any other company for any purpose without your written consent. We may use this information to notify you of important announcements regarding our software developments, software upgrades, special offers and to provide you the application/software support. If you do not want to receive any announcement from us, you can ask to be excluded from such announcements by sending us email to this effect.Review of personal information. At any time, you may ask us to remove your registration data and personal information our system. However, some information may be necessary for providing you our services and support. In that event we may not be able to provide you one or more of our services that you are using. In Oengines social games, we do not delete the past game results or records. Even if you delete the game application from your phone or do not join Teen Patti again, the past game results are not be deleted.Security. Oengines will take reasonable steps to protect your personal information from loss, misuse, unauthorized access, disclosure or unauthorized alteration.By downloading any Oengines software or creating an account or accessing or using the Service you accept and agree to be bound by these Terms of Service.
                </p>

                <h4 style="text-align: left">
                GRANT OF A LICENSE TO USE THE SERVICE
                </h4>
                <p style="text-align: left"> 
                Subject to your compliance with Terms of Service, Oengines grants you a revocable, non-exclusive limited license to access and use the Services. This license is non-transferable. You agree to use the Service for your own non-commercial purposes.You play some Oengines social games to win chips, which is virtual currency or virtual money. You do not play with real money. The chips you lose or win have no value in real money. At times you may “earn” or “buy or purchase” virtual currency and virtual in-game items. These virtual currency and items do not refer to any credit balance of real currency or its equivalent.Oengines reserves the right to terminate the license of any user without giving any reason. If Oengines believes that a user has acquired chips from unauthorized sources and from sources that violate the terms of the service, it has right to take away all or a part of the total chips with the user.Downloading any of Oengines games is free. Oengines gives you free chips to begin with and regularly. This gives you limited license to play the game (for example, Teen Patti). If you do not lose all your free chips, you can play the game for unlimited period. If you lose your free chips, you can not play Teen Patti. However, you have option to buy chips and join the game. The chips that you buy and the chips that you win have no value in the real currency. If you do not want to play the game any more, the chips remaining with you have no value in real money but instead constitutes a measurement of the extent of your license. These chips are not transferable.Chips are sold by iTunes Store (Apple), Google Play and Facebook. Oengines, Inc does not sell chips or collect any payment on behalf of Apple, Google or Facebook. No other party is authorized to sell the chips.By accepting to use Oengines’s game application, you fully understand and agree that you can lose chips because of many reasons beside losing while playing the game. Some of the reasons are client’s data connection, Internet connection, improper behaviour of the client (game app), Oengines, Inc. server problem, sudden increase in traffic, someone else using your account or your account is hacked and many more.Oengines Inc, its subsidiaries and associates are not liable for any loss of chips due to functioning as well as malfunctioning of its severs and software including Indian Rummy and Teen Patti. Winning a hand of Teen Patti chiefly depends upon the cards you get. When you download Teen Patti or Indian Rummy by Oengines, you accept and agree to play it only for fun and entertainment and Oengines Inc. is not liable for any damage or loss or what so ever.
                </p>

                <h4 style="text-align: left">
                USE OF SERVICE – RESTRICTIONS
                </h4>
                <p style="text-align: left"> 
                The following restrictions apply to use of the Service:
                1) You must not use the Service is you are under the age of 13. You must deny anyone under 13 to use your account. You are fully responsible for any unauthorized use of the Service including not limited to the use of credit card or any payment by any method.
                <br>
                2) You shall use your account only for non-commercial entertainment purposes. You shall not use the Service for any other purpose.<br>
                3) You shall not use your account for any illegal activity.<br>
                4) You shall not use your account to transmit repetitive messages (spam), junk e-mail, advertise and solicit.<br>
                5) You shall not post any objectionable and offensive information including but not limited to abusive, threatening, racial, sexual, or obscene.<br>
                6) You will not use your account to cheat or hack the game by any means.<br>
                7) You shall not sublicense, lease, trade, gift, sell or otherwise transfer your account or associated virtual items partly or fully to anyone without written permission from Oengines.<br>
                8) You shall not buy or purchase chips and virtual items from any unauthorized source.<br>
                9) You shall not create or use more than one Facebook account to use the Service.
                </p>
            </div>
        </div>
    </div>
</section>



<!-- end page-content -->
</div>