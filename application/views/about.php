<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=1'?>">


<!-- start page-content -->
<br><br><br>
<section class="sp-section little bg-color-light text-center" id="sp-about">
    <div class="container">
        <div class="row">
            <div class="sp-title-block line">
                <span>short story</span>
                <h3>About our company</h3>
            </div>
            <div class="col-md-8 offset-md-2 wow fadeIn" data-wow-delay=".3s">
                <p>Our mission will be to develop more innovative things to realities and get passionate infrastructural designs for controling more user's traffic.<span class="sp-signature">Oengines Studio Inc.</span></p>
            </div>
        </div>
    </div>
</section>

<section class="sp-section text-center" id="sp-services">
    <div class="container">
        <div class="sp-title-block line">
            <span>welcome</span>
            <h3>Great Experience</h3>
        </div>

        <div class="sp-services-container row wow sequenced fx-fadeIn">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-locked-outline"></i></div>
                    <div class="title"><h4>Safe & Protected</h4></div>
                    <p class="text">Indeed, source security with version control system and with encodes, we are always prefer to get use sophisticated systems.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-settings"></i></div>
                    <div class="title"><h4>Very Flexible</h4></div>
                    <p class="text">Needs to check accuracy.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-time-outline"></i></div>
                    <div class="title"><h4>Right On Time</h4></div>
                    <p class="text">Define dealines to codes analytics.</p>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- end page-content -->
</div>