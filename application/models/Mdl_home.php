<?php


class Mdl_home extends CI_Model
{

/*----------------------------------------------------------*/
	function mdl_words($post){

		if($post['page'] == 0){

			return array('error' =>(boolean)true,
	                'msg' => 'please enter page no ! '
	                );
		}else{

			$page = $post['page'] - 1;
			$page = $page * 50;

					$this->db->select('word');
					$this->db->limit(50, $page);
					$this->db->where('type',$post['type']);
			$data = $this->db->get('word');
			$word = $data->result_array();

			$new_ary = array();
			foreach ($word as $value) {
				array_push($new_ary, $value['word']);
			}

					 $this->db->where('type',$post['type']);
			$total = $this->db->count_all_results('word');
			
			if($word){
				return array('error' =>(boolean)false,
		                'word' => $new_ary,
		                'total_words' => $total
		                );
	        }else{
	        	return array('error' =>(boolean)true,
		                'word' => 'no word found !',
		                'total_words' => 0
		                );
	        }   

        }      

	}

	function all_faq_topic(){

				$this->db->select('*');
		$data = $this->db->get('faq_topic');
        $value = $data->result_array();
        return $value;

	}


/*----------------------------------------------------------*/

	function indexgame($id){

				$this->db->select('*');
		$data = $this->db->get('config');
        $value = stdToArray($data->result())[0];

		if($id){
				$this->db->limit($value['home_page_a_game']);
				$this->db->where('id',$id);
				$this->db->where('index_game', 1);
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
		}
				$this->db->limit($value['home_page_game']);
				$this->db->where('active_state', 1);
				$this->db->where('is_deleted', 0);
				$this->db->where('index_game', 1);
				$this->db->order_by('cdate', 'DESC');
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
	}

	function getimagicon(){
				$this->db->limit(10);
				$this->db->select('icon_img');
				$this->db->where('active_state', 1);
				$this->db->where('is_deleted', 0);
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
	}

	function gamecount(){
				$this->db->where('active_state', 1);
				$this->db->where('is_deleted', 0);
		$data = $this->db->get('www_games');	
		$data = $data->num_rows();	
		return $data;
	}

	function getvideo(){
				$this->db->where('active_state', 1);
		$data = $this->db->get('youtubevideo');
		$data = $data->result_array();
		return $data;
	}

	function getconfig(){
				$this->db->select('*');
		$data = $this->db->get('config');
		$data = $data->result_array();
		return $data;
	}

	function fetch_indexgame($start=0) {

        $limit=1;

		        $this->db->limit($limit, $start);
        		$this->db->order_by('sort', 'asc');
        		$this->db->where('active_state', 1);
				$this->db->where('is_deleted', 0);
        $query= $this->db->get("www_games");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }

                     $this->db->limit($limit, ($start+$limit));
                     $this->db->order_by('sort', 'asc');
            		 $this->db->where('active_state', 1);
					 $this->db->where('is_deleted', 0);
            $query = $this->db->get("www_games");
            $count = $query->num_rows();
            
            if($count){
            	$endpoint = 1;
            }else{
            	$endpoint = 0;
            }
            return array('error'=> 'N', 'data'=> $data, 'isendpoint' => $endpoint, 'startpoint' => ($start+$limit));
        }
        return array('error'=> 'Y', 'data'=> [], 'msg' => 'no more games available!');
    }
}
