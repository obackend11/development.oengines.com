<!DOCTYPE html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="_nK">
      <title><?php echo $title;?></title>
      <link rel="icon" type="image/ico" href="<?php echo site_url();?>images/favicon.ico">
      <!-- START: Styles --><!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,700%7cMarcellus+SC" rel="stylesheet">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo site_url();?>bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- FontAwesome -->
      <link rel="stylesheet" href="<?php echo site_url();?>bower_components/font-awesome/css/font-awesome.min.css">
      <!-- IonIcons -->
      <link rel="stylesheet" href="<?php echo site_url();?>bower_components/ionicons/css/ionicons.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/navigation.css">
      <!-- Flickity -->
      <link rel="stylesheet" href="<?php echo site_url();?>bower_components/flickity/dist/flickity.min.css">
      <!-- Photoswipe -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>bower_components/photoswipe/dist/photoswipe.css">
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>bower_components/photoswipe/dist/default-skin/default-skin.css">
      <!-- DateTimePicker -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>bower_components/datetimepicker/build/jquery.datetimepicker.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>plugins/revolution/css/navigation.css">
      <!-- Prism -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>bower_components/prism/themes/prism-tomorrow.css">
      <!-- Summernote -->
      <link rel="stylesheet" type="text/css" href="<?php echo site_url();?>bower_components/summernote/dist/summernote.css">
      <link rel="stylesheet" href="<?php echo site_url();?>css/oengines.min.css">
      <!-- Custom Styles -->
      <link rel="stylesheet" href="<?php echo site_url();?>css/custom.css">
      <script src="<?php echo site_url();?>bower_components/jquery/dist/jquery.min.js"></script>

   </head>
   <!--
      Additional Classes:
          .nk-page-boxed
      -->
   <body>

   <div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content hidden-lg-up">
        <div class="nano">
            <div class="nano-content">
                <a href="<?php echo base_url();?>" class="nk-nav-logo"><img src="<?php echo site_url();?>images/logo.png" alt="" width="90">
                </a>
                <div class="nk-navbar-mobile-content">
                    <ul class="nk-nav">
                        <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                    </ul>
                </div>
            </div>
        </div>
    </div>

      <div class="nk-main">

         <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
            <div class="bg-image">
               <div style="background-image: url('<?php echo site_url();?>images/image-3.jpg')"></div>
            </div>
            <div class="nk-header-table">
               <div class="nk-header-table-cell">
                  <div class="container">
                     <h1 class="myt"><?php echo $inner_title;?></h1>
                  </div>
               </div>
            </div>
         </div>

         <div class="container">


            <div class="nk-gap-4"></div>
            <!-- START: Posts List -->
            <div class="nk-blog-list">
               <!-- START: Post -->
               
               <?php 

                  foreach ($games as $game) { ?>
                  <div class="nk-blog-post" data-mouse-parallax-z="5" data-mouse-parallax-speed="1">
                     <div class="nk-post-thumb">
                        
                        <div class="nk-post-category"><a href="<?php echo $game['link'];?>" target="_blank">PLAY</a></div>
                        <a href="<?php echo $game['link'];?>" target="_blank"><img src="<?php echo $game['image'];?>" class="nk-img-stretch"></a>
                     </div>
                     <div class="nk-post-content">
                        <div data-mouse-parallax-z="2">
                           <h2 class="nk-post-title h1 more_game"><a href="<?php echo $game['link'];?>" target="_blank"><?php echo $game['title'];?></a></h2>
                           <div class="nk-post-date"><?php echo $game['date'];?></div>
                        </div>
                     </div>
                  </div>   
               <?php }?>
               

               
               <!-- END: Post --><!-- START: Pagination -->
               <!-- <div class="nk-pagination nk-pagination-center"><a href="#" class="nk-btn nk-btn-lg nk-btn-circle">Load More ...</a></div> -->
               <!-- END: Pagination -->
            </div>

            <!-- END: Posts List -->
            <div class="nk-gap-4"></div>
            <div class="nk-gap-3"></div>
         </div>
        <!-- footer content load -->
        <footer class="nk-footer nk-footer-parallax nk-footer-parallax-opacity">
<img class="nk-footer-top-corner" src="<?php echo site_url();?>images/footer-corner.png" alt="">
<div class="container">
   <div class="nk-gap-2"></div>
    <div class="nk-footer-logos">
      <a href="https://themeforest.net/user/_nk/portfolio?ref=_nK" target="_blank"><img class="nk-img" src="<?php echo site_url();?>images/logo.png" alt="" width="120"></a> 
      
      
      
      
    </div>
   <div class="nk-gap"></div>
   <p>&copy; 2016 OE Studio Inc. Developed in association with companion. OEngines Studio designing and developing games. Create Innvative themes and make effect graphics with 2D & 3D design. All Rights Reserved.</p>
   <p>OE &reg; is a make amazing designs and develop singleplayer or multiplayers services for clients.</p>
  
   <div class="nk-gap-4"></div>
</div>
</footer>
      </div>
      <!--
         START: Share Buttons
             .nk-share-buttons-left
         -->
       <div class="nk-share-buttons nk-share-buttons-left hidden-sm-down">
         <ul>
            <li><span class="nk-share-icon" title="Share page on Facebook" data-share="facebook"><a href="https://www.facebook.com/oe.studio091/"><span class="icon fa fa-facebook"></span></a></span> <span class="nk-share-name">Facebook</span></li>
<!--            <li><span class="nk-share-icon" title="Share page on Twitter" data-share="twitter"><span class="icon fa fa-twitter"></span></span> <span class="nk-share-name">Twitter</span></li>-->
            <li><span class="nk-share-icon" title="Share page on Google+" data-share="google-plus"><a href="https://plus.google.com/u/2/116446153260416186360"> <span class="icon fa fa-google-plus"></span></a></span> <span class="nk-share-name">Google Plus</span></li>

         </ul>
      </div>
      <!--
         START: Side Buttons
             .nk-side-buttons-visible
         -->
      <div class="nk-side-buttons nk-side-buttons-visible">
         <ul>
            <li class="nk-scroll-top"><span class="nk-btn nk-btn-lg nk-btn-icon"><span class="icon ion-ios-arrow-up"></span></span></li>
         </ul>
      </div>
      
     
      <script src="<?php echo site_url();?>bower_components/gsap/src/minified/TweenMax.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script><!-- Bootstrap -->
      <script src="<?php echo site_url();?>bower_components/tether/dist/js/tether.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script><!-- Sticky Kit -->
      <script src="<?php echo site_url();?>bower_components/sticky-kit/dist/sticky-kit.min.js"></script><!-- Jarallax -->
      <script src="<?php echo site_url();?>bower_components/jarallax/dist/jarallax.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/jarallax/dist/jarallax-video.min.js"></script><!-- imagesLoaded -->
      <script src="<?php echo site_url();?>bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script><!-- Flickity -->
      <script src="<?php echo site_url();?>bower_components/flickity/dist/flickity.pkgd.min.js"></script><!-- Isotope -->
      <script src="<?php echo site_url();?>bower_components/isotope/dist/isotope.pkgd.min.js"></script><!-- Photoswipe -->
      <script src="<?php echo site_url();?>bower_components/photoswipe/dist/photoswipe.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script><!-- Typed.js -->
      <script src="<?php echo site_url();?>bower_components/typed.js/dist/typed.min.js"></script><!-- Jquery Form -->
      <script src="<?php echo site_url();?>bower_components/jquery-form/dist/jquery.form.min.js"></script><!-- Jquery Validation -->
      <script src="<?php echo site_url();?>bower_components/jquery-validation/dist/jquery.validate.min.js"></script><!-- Jquery Countdown + Moment -->
      <script src="<?php echo site_url();?>bower_components/jquery.countdown/dist/jquery.countdown.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/moment/min/moment.min.js"></script>
      <script src="<?php echo site_url();?>bower_components/moment-timezone/builds/moment-timezone-with-data.js"></script><!-- Hammer.js -->
      <script src="<?php echo site_url();?>bower_components/hammer.js/hammer.min.js"></script><!-- NanoSroller -->
      <script src="<?php echo site_url();?>bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script><!-- SoundManager2 -->
      <script src="<?php echo site_url();?>bower_components/SoundManager2/script/soundmanager2-nodebug-jsmin.js"></script><!-- DateTimePicker -->
      <script src="<?php echo site_url();?>bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js"></script><!-- Revolution Slider -->
      <script type="text/javascript" src="<?php echo site_url();?>plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
      <script type="text/javascript" src="<?php echo site_url();?>plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
      <script type="text/javascript" src="<?php echo site_url();?>plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
      <script type="text/javascript" src="<?php echo site_url();?>plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
      <script type="text/javascript" src="<?php echo site_url();?>plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script><!-- Keymaster -->
      <script src="<?php echo site_url();?>bower_components/keymaster/keymaster.js"></script><!-- Summernote -->
      <script src="<?php echo site_url();?>bower_components/summernote/dist/summernote.min.js"></script><!-- Prism -->
      <script src="<?php echo site_url();?>bower_components/prism/prism.js"></script>
      <script src="<?php echo site_url();?>js/oengines.min.js"></script>
      <script src="<?php echo site_url();?>js/oengines-init.js"></script><!-- END: Scripts -->
   </body>
   
</html>