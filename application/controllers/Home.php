<?php

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Common');
		$this->load->model('Mdl_services');
		$this->load->model('Mdl_home');
	}

/*----------------------------------------------------------*/

	function words()
	{

        if(isset($_POST['page']) && $_POST['type'] !== ''){

            echo json_encode($this->Mdl_home->mdl_words($_POST));

        }else{

        	echo json_encode(
                            array('error' => ($_POST) ? (boolean)false : (boolean)true,
                                  'data' => $_POST,
                                  'totalword' => ''
                            )
                        	);
        }	
	}

/*----------------------------------------------------------*/	


	public function index()
	{	

		$flag['home'] = true;
		$flag['title'] = 'HOME | OE STUDIO';	

		$data['games'] = $this->Mdl_home->fetch_indexgame();
		$data['gamecount'] = $this->Mdl_home->gamecount();
		$data['videodata'] = $this->Mdl_home->getvideo()[0];
		$data['all_img'] = $this->Mdl_home->getimagicon();
		$data['configdata']	= $this->Mdl_home->getconfig()[0];

		$this->load->view('header', $flag);
		$this->load->view('home', $data);
		$this->load->view('footer');
	}


	function getindexgamelist($start=1){
		$data = $this->Mdl_home->fetch_indexgame($start);
		echo json_encode($data);
	}

	function getgamelist($start=2){
		$data = $this->Common->fetch_gamelist($start);
		echo json_encode($data);
	}


	function about(){

		$flag['title'] = 'ABOUT US | OE STUDIO';
		$data = array();
		$this->load->view('header', $flag);
		$this->load->view('about', $data);
		$this->load->view('footer');
	}
	function contact(){

		$flag['title'] = 'CONTACT | OE STUDIO';
		$data = array();
		$this->load->view('header', $flag);
		$this->load->view('contact', $data);
		$this->load->view('footer');
	}

	function services($id = '',$gamesid=''){


		if($id == 'web'){
			
			$flag['title'] = 'WEB DEVELOPMENT | OE STUDIO';
			$flag['service_name'] = 'WEB DEVELOPMENT';
			$flag['service_tag'] = 'Get Responsiveness to Make Perfections';

			$data['tags'] = array('e' => 'E-Commerce', 'corp' => 'Corporative');
			$data['header_image'] = base_url().'assets/images/slide14.jpg';

			$data['corp'] = array(
								array('img' => base_url().'assets/images/web_development/bankjoy.png', 'title' => 'bankjoy', 'link' => 'javascript:;', 'content' => 'Mobile Banking with smartphone to easy to use.')
							);

			$data['e'] = array(
							array('img' => base_url().'assets/images/web_development/khe.png', 'title' => 'khe', 'link' => 'javascript:;', 'content' => 'buy your needs and things.')
						);

		}else if($id == 'games'){

			// pr($id);die;
			$flag['title'] = 'GAME DEVELOPMENT | OE STUDIO';
			$flag['service_name'] = 'GAME DEVELOPMENT';
			$flag['service_tag'] = 'Get Success Story about to Games';
			$data['header_image'] = base_url().'assets/images/game_development.png';

			if($gamesid){

				$data['singlegame'] = $this->Mdl_services->get_single_game($gamesid)[0];
				$update = explode("-", $data['singlegame']['updated']);
				$month = $this->Mdl_services->month($update[1]);
				$date = explode(" ", $update[2]);
				$data['updated'] = $month." ".$date[0].", ".$update[0];


				$rel = explode(", ", $data['singlegame']['index_rel']);
				foreach ($rel as $key => $value) {
					$data['relgame'][$key] = $this->Mdl_services->rel_game($value)[0];
				}
		
				if(!$data['singlegame']){
					$data['nofound'] = "This game not found !";
				}

			}else{
			
			$data['tags'] = array('android' => 'Android Development', 'ios' => 'iOS Development', 'hybrid' => 'Hybrid Development', 'unity' => 'Unity 3D Development');

			$data['gamecount'] = $this->Mdl_home->gamecount();
			// pr($data['gamecount']);die;
			$data['games'] = $this->Mdl_services->get_games();				

			}

		}
		// pr($data['gamecount']);die;
		$this->load->view('header', $flag);	
		$this->load->view($id == 'games' ? 'services' : 'webservice', $data);
		$this->load->view('footer');
	}


 	function sendContact(){
		echo "string";
		$this->Common->notify();
	}

	function subscribeNewsLetter(){
		 $from_email = "oengines.studio@gmail.com"; 
         $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Oengines Studio Subscriber'); 
         $this->email->to($to_email);
         $this->email->subject('Oengines Studio Help'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()) {
         	echo json_encode('true');
         }else {
         	echo json_encode('false');
         } 
	}

	function send_mail() { 
		
         $from_email = "oengines.studio@gmail.com"; 
         $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Oengines Studio Support'); 
         $this->email->to($to_email);
         $this->email->subject('Oengines Studio Help'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()) {
         	echo json_encode('true');
         }else {
         	echo json_encode('false');
         }
         // redirect(base_url());
    }

    function privacy() {

    	$flag['title'] = 'Privacy | OE STUDIO';
		$this->load->view('header', $flag);
		$this->load->view('privacy');
		$this->load->view('footer');
    } 

    function faq() {	

    	$data['all_faq_topic'] = $this->Mdl_home->all_faq_topic();
    	// $data['all_faq'] = $this->Mdl_home->all_faq();
    	// pr($data);die;

    	$flag['title'] = 'F.A.Q | OE STUDIO';
		$this->load->view('header', $flag);
		$this->load->view('faq', $data);
		$this->load->view('footer');
    } 

    function signout(){
		$this->session->sess_destroy();
		redirect('','refresh');
	}

     
    function support(){
		$flag['support'] = true;
		$data['title'] = 'SUPPORT | OE STUDIO';
		$data['inner_title'] = 'SUPPORT';
		$this->load->view('header', $flag);	
		$this->load->view('support', $data);
		// $this->load->view('footer');	
	}
}