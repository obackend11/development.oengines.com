<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=3'?>">

<style type="text/css">
  .android{
    background-image: url(../../assets/images/android.png);
  }
  .ios{
    background-image: url(../../assets/images/ios.png);
  }
  .back-sing{
    background-image: url(../../../assets/images/background_repeat.png); 
  }
  .back{
    background-image: url(../../assets/images/background_repeat.png); 
  }
  .carousel-inner .carousel-item {
    transition: -webkit-transform 1s ease;
    transition: transform 1s ease;
    transition: transform 1s ease, -webkit-transform 1s ease;
  }
  .mobile_font{
      font-size: 60px !important;
  }
  .mobile_all_game_title{
      margin-top: 75px;
  }
  @media screen and (max-width: 768px) {
    .mobile_img{
      width: 75% !important;
    }
    .mobile_font{
      font-size: 35px !important;
    }
    .mobile_font_2{
      font-size: 28px !important;
    }
    .mobile_hover:hover{
      border: 2px solid #C3AC6D !important;
      border-radius: 20px !important;
      padding: 2px !important;
    }
    .mobile_all_game_title{
        margin-top: 20px;
    }
    .mobile_br{
      display: none !important;
    }
    .mobile_font3{
      font-size: 16px !important;
    }
  }
</style>
<!-- start page-content -->

<br><br><br>
<section id="sp-blog" class="sp-section <?php echo ($singlegame) ? 'back-sing' : 'back';?>" style="padding-top:0px;padding-bottom:0px;">
    <div class="container-" id="sp-blog-inner" style="padding: 0% 4%;">
        <div class="content-column mobile_all_game_title">

        
            <?php if(isset($games)){ ?>
                      <br>
                      <center>
                        <h1 class="mobile_font_2" style="font-weight:700;margin-bottom:10px;"><span style="color:#C3AC6D;"> OENGINES </span> All GAMES
                        </h1>
                      </center>
                      <br><div style="border-bottom:4px solid #c3ac6d"></div><br><br class="mobile_br">
            <?php } ?>
            
            <div class="col-md-12 <?php echo ($singlegame) ? 'sing' : '';?>" style="padding-right:0px;padding-left:0px;">   
              <div class="row fetchdata">
                <?php   if(isset($games)){
                    
                        $gamesid = array();
                        foreach ($games as $key => $value) { 

                        array_push($gamesid, $value['id']);
                        $this->session->set_userdata('gamesid',$gamesid);
                        $ak = array(0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50);
                        if(in_array($key,$ak)){
                            $right = 'fadeInLeft';
                        }else{
                            $right = 'fadeInRight'; 
                        }

                ?>
                        <div class="col-md-6 mobile-md6 col-bottom wow <?php echo $right ?>" style="margin: 2.5% 0%;box-shadow: 0px 20px 20px 0px rgba(226, 222, 210, 0.65);background-color: #fff;">
                            <a href="<?php echo base_url().'home/services/games/'.$value['id'] ;?>"> 
                            <div class="mobiletop" style="padding: 0px 20px;">
                                <div class="blog-slider__title"><?php echo $value['title'] ;?></div>
                            </div> 
                            <img class="nomobile" src="<?php echo $value['img'] ;?>" style="border-radius:15px;">
                            </a>
                            <div class="blog-slider nomobilecolor">
                                <div class="blog-slider__wrp swiper-wrapper">
                                  <div class="blog-slider__item swiper-slide">
                                  <a href="<?php echo base_url().'home/services/games/'.$value['id'] ;?>">
                                  <div class="blog-slider__img hoverr2">
                                    <img class="addclass" src="<?php echo $value['icon_img'] ;?>" alt="">
                                  </div>
                                  </a>
                                  <div class="blog-slider__content">
                                    <?php if($value['link_android'] != ''){ ?>
                                      <div class="sp-blog-read"><a target="_blank" href="<?php echo $value['link_android'];?>"><button class="btn btn-sm btn-primary device animebtn android" style="width: 200px;background-size: auto;height: 64px;"></button></a></div>
                                      <br>
                                    <?php } ?>
                                    <?php if($value['link_ios'] != ''){ ?>  
                                      <div class="sp-blog-read"><a target="_blank" href="<?php echo $value['link_ios'];?>"><button class="btn btn-sm btn-primary device animebtn ios" style="width: 200px;background-size: auto;height: 64px;"></button></a></div>
                                    <?php } ?>  
                                  </div>
                                  </div>
                                </div>
                            </div>
                        </div> 

                <?php } } ?>
              </div>

                <?php if(isset($singlegame)){ ?>

                    <div style="background-color: #fbfbfb;border: 4px solid #C3AC6D;margin-top: 5%;">     


                        <input type="hidden" name="ss_type" id="ss_type" value="<?php echo $singlegame['ss_type'];?>">

                        <section class="sp-section sp-p-100">
                            <div class="container">
                                <div class="row">
                                <div class="col-md-4 col-xs-6" style="padding: 3%;margin: 3% 0% 0% 0%;">
                                    <img src="<?php echo $singlegame['icon_img'] ;?>" style="padding:10px;" class="img-responsive mobile_img">
                                </div>
                            
                                <div class="col-md-7 col-xs-6" style="padding: 3%;margin: 3% 0% 0% 0%;">
                                    <h1 style="font-weight: 700;margin-bottom:20px;;"><span class="mobile_font" style="color:#C3AC6D;"> <?php echo $singlegame['title'] ;?> </span></h1>

                                    <h6 class="mobile_font3" style="font-weight: 500;margin-bottom:20px;"><span style="color:#000;font-size:16px;"> <?php echo $singlegame['content'] ?></span></h6> 
                                    <br>
                                    <div class="row">
                                    <?php if($singlegame['link_android'] != ''){ ?>
                                        <div class="col-md-5">
                                        <div class="sp-blog-read" style="padding-bottom: 10px"><a target="_blank" href="<?php echo $singlegame['link_android'];?>"><button class="btn btn-sm btn-primary device animebtn" style="background-image:url(../../../assets/images/android.png);width:200px;background-size:auto;height: 64px;"></button></a></div>
                                        </div>
                                    <?php } ?>
                                    <?php if($singlegame['link_ios'] != ''){ ?>
                                        <div class="col-md-5">
                                        <div class="sp-blog-read" style="padding-bottom: 10px"><a target="_blank" href="<?php echo $singlegame['link_ios'];?>"><button class="btn btn-sm btn-primary device animebtn" style="background-image:url(../../../assets/images/ios.png);width:200px;background-size:auto;height: 64px;"></button></a></div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                    <br>
                                    <div class="row">
                                      <?php if($updated){ ?>
                                        <div class="col-md-5">
                                          <strong><h4 style="color:#C3AC6D">Updated</h4></strong>
                                          <span><h5><?php echo $updated;?></h5></span>
                                        </div>
                                      <?php } ?>
                                      <?php if($singlegame['version']){ ?>
                                        <div class="col-md-5">
                                          <strong><h4 style="color:#C3AC6D">Current Version</h4></strong>
                                          <span><h5><?php echo $singlegame['version'];?></h5></span>
                                        </div>
                                      <?php } ?>  
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="sharethis-inline-share-buttons"></div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>    
                        </section>

                        <section class="sp-section sp-p-100 nomobilescreen">
                            <div class="container" style="padding: 15px;">
                                <div class="row" style="margin-bottom: 20px;margin-top: 20px;">
                                    <div class="col-md-12">
                                        <section class="customer-screen slider">
                                        <?php
                                            $img = explode(",", $singlegame['index_img']);
                                            foreach ($img as $key2 => $value2) { ?>

                                            <div class="slide">
                                                <img src="<?php echo 'http://more.oengines.com/uploads/screen/'.$value2 ;?>" style="border: 4px solid #C3AC6D;">
                                            </div>

                                        <?php } ?>    
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div class="container nopcscreen" style="margin-top:30px;margin-bottom:20px;"> 
                          <div id="demo" class="carousel slide" data-ride="carousel" data-interval="1500">
                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                            <?php
                                  $img = explode(",", $singlegame['index_img']);
                                  foreach ($img as $key3 => $value3) { 
                            ?>  
                              <li data-target="#demo" data-slide-to="<?php echo $key3;?>" class="<?php echo ($key3 == 0) ? 'active' : '';?>"></li>
                            <?php } ?>    
                            </ul>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">

                            <?php
                                  $img = explode(",", $singlegame['index_img']);
                                  foreach ($img as $key4 => $value4) { 
                            ?>  
                              <div class="carousel-item <?php echo ($key4 == 0) ? 'active' : '';?>">
                                <!-- <div class="left"></div> -->
                                <img src="<?php echo 'http://more.oengines.com/uploads/screen/'.$value4 ;?>" style="border: 4px solid #C3AC6D;" alt="<?php echo $value4 ?>">
                                <!-- <div class="right"></div> -->
                              </div>
                            <?php } ?>

                            </div>

                            <!--
                            <a class="carousel-control-prev" href="#demo" data-slide="prev" role="button">
                              <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next" role="button">
                              <span class="carousel-control-next-icon"></span>
                            </a>
                            -->
                          
                          </div>
                        </div>

                     </div>
                    
                      <br> 
                      <section class="sp-section sp-p-100">
                          <div class="container" style="padding: 15px;">
                              <div class="row" style="margin-bottom: 20px;">
                                  <div class="col-md-12" style="padding:0 4%;">
                                      <center>
                                      <h2 style="font-weight: 600;margin-bottom:10px;">
                                        <span style="color:#000;">More Description</span>
                                      </h2></center>
                                      <span style="font-size: 1.0rem;font-weight: 400;">
                                      <?php echo $singlegame['content_2'] ;?>   
                                      </span>  
                                      
                                  </div>
                              </div>
                          </div>
                      </section>
                      <br>
                   

                <?php } ?>
                <?php if(isset($nofound)){ ?>
                      <div style="margin-top:250px;margin-bottom: 150px;">
                        <center><h1><?php echo $nofound ;?></h1></center><br/>
                        <center><a href="<?php echo base_url('home/') ?>" class="btn btn-primary find-more fm">Go Back Home Page</a></center>
                      </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>

<?php if(isset($singlegame)){ 
      if(isset($relgame[0])){
?>
<section class="sp-section sp-p-100" style="<?php echo ($singlegame['backimg']) ? 'background-image: url('.$singlegame['backimg'].')' : 'background-color: #f5f5f5;'?>">    
<div class="container-" style="padding:2% 5%;">
        <br>
        <center><h1 class="mobile_font_2" style="font-weight: 700;margin-bottom:10px;"><span style="color:#C3AC6D;"> Recommended </span>Games</h1></center>
        <center><div style="border:2px solid #C3AC6D;width:38%;"></div></center>
        <br><br><br>
   <section class="customer-logos slider">
    <?php foreach ($relgame as $key6 => $value6) { ?>

      <div class="slide">
         <a href="<?php echo base_url().'home/services/games/'.$value6['id'] ;?>">
          <img class="hoverr mobile_hover" src="<?php echo $value6['icon_img'] ?>">
         </a> 
      </div>

    <?php } }?>  
   </section>
</div>
<br><br>
</section>
<?php } ?>

<?php if(isset($games)){ ?>
<br><br><br>
<div class="container">
    <?php if($gamecount > 2){  ?>
    <div class="row" style="text-align: center;">
        <div class="col-md-12 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
            <div class="btn btn-primary find-more2 show_more"><a class="find-more-a2" href="javascript:;">LOAD MORE GAMES</a></div>
        </div>
    </div>
  <?php } ?>
</div>
<br><br><br>
<?php } ?>
<!-- end page-content -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    var ss_type = $('#ss_type').val();
    if(ss_type == 1){
      var img = 3;
    }else{
      var img = 2;
    }
    $('.customer-screen').slick({
        slidesToShow: img,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    var count = 0;
    var endpoint = 0;
    var startpoint = 2;
    $(document).on('click','.show_more',function(){
      var url = "<?php echo base_url() ?>home/getgamelist/"+startpoint;
      var curl = "<?php echo base_url(); ;?>"
      console.log("url :::: ", url);
        $.ajax({
            type:'POST',
            url: url,
            data:{con:count},
            success:function(data){

              data = JSON.parse(data);
              console.log("DATA::::::",data);
              var html = '';
              if(data.error == 'N'){

                for (var i = 0; i < data.data.length; i++) {
              
                  html='<div class="col-md-6 mobile-md6 col-bottom wow';
                  html+=(i == 0) ? " fadeInLeft " : " fadeInRight "
                  html+='" style="margin: 2.5% 0%;box-shadow: 0px 20px 20px 0px rgba(226, 222, 210, 0.65);background-color: #fff;">'
                      +'<a href="'+curl+'/home/services/games/'+data.data[i].id+'">'
                      +'<div class="mobiletop" style="padding: 0px 20px;">'
                      +'<div class="blog-slider__title">'+data.data[i].title+'</div>'
                      +'</div>'
                      +'<img class="nomobile" src="'+data.data[i].img+'" style="border-radius:15px;">'
                      +'</a>'
                      +'<div class="blog-slider nomobilecolor">'
                      +'<div class="blog-slider__wrp swiper-wrapper">'
                      +'<div class="blog-slider__item swiper-slide">'
                      +'<a href="'+curl+'home/services/games/'+data.data[i].id+'">'
                      +'<div class="blog-slider__img hoverr2">'
                      +'<img class="addclass" src="'+data.data[i].icon_img+'" alt="">'
                      +'</div>'
                      +'</a>'
                      +'<div class="blog-slider__content">';
                      if(data.data[i].link_android){
                  html+='<div class="sp-blog-read"><a target="_blank" href="'+data.data[i].link_android+'">'
                      +'<button class="btn btn-sm btn-primary device animebtn android" style="width: 200px;background-size: auto;height: 64px;">'
                      +'</button></a></div>';
                      }
                  html+='<br>'
                      if(data.data[i].link_ios){
                  html+='<div class="sp-blog-read"><a target="_blank" href="'+data.data[i].link_ios+'">'
                      +'<button class="btn btn-sm btn-primary device animebtn ios" style="width: 200px;background-size: auto;height: 64px;">'
                      +'</button></a></div>'
                      }
                  html+='</div>'
                      +'</div>'
                      +'</div>'
                      +'</div>'
                      +'</div>';

                      $('.fetchdata').append(html).fadeIn(1000).show('slow');
                }

              }else{
                $('.show_more').hide();
              }

              if(data.isendpoint == 1){
                $('.show_more').show();
              }else{
                $('.show_more').hide();
              }

              startpoint = data.startpoint;

            },
            error: function(err){
              console.log(err);
            }
        });
    });
});

</script>
<script type="text/javascript">
  $('.carousel-control-prev').click(function(e)
    {
      e.preventDefault();

    });
</script>
<script type="text/javascript">
  $('.carousel-control-next').click(function(e)
    {
      e.preventDefault();

    });
</script>