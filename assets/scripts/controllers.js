/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Animated Circles
 ***********************************************/
(function ($) {

    var el = $('.sp-circle'),
        delay = 0,
        options = {
            value     : 0,
            size      : 125,
            thickness : 2,
            fill      : {color: "#111"},
            emptyFill : "#ddd",
            startAngle: 300,
            animation : {duration: 2500, easing: 'easeInOutQuint'}
        };

    el.one('appear', function () {
        var $el = $(this);
        setTimeout(function () {
            $el.circleProgress('value', $el.data('value'));
        }, delay);
        delay += 150;
    });

    el.circleProgress(options).on('circle-animation-progress', function (event, progress, stepValue) {
        $(this).find('span').text((stepValue * 100).toFixed(1));
    });

    setInterval(function () { delay = 0; }, 1000);

    $(window).one('pzt.preloader_done', function() {
        el.appear({force_process: true});
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Masonry Blog Layout
 ***********************************************/
(function ($) {
    var $grid = $('#sp-blog-grid'); // locate what we want to sort

    // don't run this function if this page does not contain required element
    if ($grid.length <= 0) {
        return;
    }

    // instantiate the plugin
    $grid.pzt_shuffle({
        itemSelector: '[class*="col-"]',
        gutterWidth : 0,
        speed       : 600, // transition/animation speed (milliseconds).
        easing      : 'ease'
    });
})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Masonry Clients Layout
 ***********************************************/
(function ($) {
    var $grid = $('.sp-clients-grid'); // locate what we want to sort

    // don't run this function if this page does not contain required element
    if ($grid.length <= 0) {
        return;
    }

    // instantiate the plugin
    $grid.pzt_shuffle({
        itemSelector: '[class*="col-"]',
        gutterWidth : 0,
        speed       : 600, // transition/animation speed (milliseconds).
        easing      : 'ease'
    });
})(jQuery);
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */


/***********************************************
 * ColorSwarm module API
 ***********************************************/
(function ($) {
    'use strict';

    $.fn.PZT_ColorSwarm = function () {
        var canvas = $(this)[0];
        var ctx;
        var numCircles = 100;

        var resize = window.resize = function () {
            canvas.height = $(canvas).parent().outerHeight();
            canvas.width = window.innerWidth;
        };

        $(function () {
            ctx = canvas.getContext('2d');
            resize();

            var circles = [],
                colors = randomColor({luminosity: 'light', count: numCircles});

            for (var i = 0; i < numCircles; i++) {
                var x = Math.random() * canvas.width;
                var y = Math.random() * canvas.height;
                var c = new Circle(x, y, colors[i]);
                c.draw();
                circles.push(c);
            }

            var requestAnimFrame = function () {
                return window.requestAnimationFrame ||
                    window.webkitRequestAnimationFrame ||
                    window.mozRequestAnimationFrame ||
                    window.oRequestAnimationFrame ||
                    window.msRequestAnimationFrame ||
                    function (a) {
                        window.setTimeout(a, 1E3 / 60);
                    };
            }();

            var loop = function () {
                requestAnimFrame(loop);
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                for (var i = 0; i < circles.length; i++) {
                    circles[i].frame();
                }
            };

            loop();
        });

        var Circle = function (x, y, c) {
            this.pos = [x, y];
            this.r = (1.5 * Math.random()) + 1;
            this.c = c;
            this.v = [
                (Math.random() - 0.5) * 0.3,
                (Math.random() - 0.5) * 0.3
            ];
        };

        Circle.prototype.getBound = function (i) {
            return i ? canvas.height : canvas.width;
        };

        var i;
        Circle.prototype.frame = function () {
            for (i = 0; i < 2; i++) {
                if (this.pos[i] > this.getBound(i) - 10) {
                    this.v[i] *= -1;
                }
                else if (this.pos[i] < 10) {
                    this.v[i] *= -1;
                }
                this.pos[i] += this.v[i] * 10;
            }

            this.draw();
        };

        Circle.prototype.draw = function () {
            ctx.fillStyle = this.c;
            ctx.beginPath();
            ctx.arc(this.pos[0], this.pos[1], this.r, 0, 2 * Math.PI, false);
            ctx.fill();
        };
    };
})(jQuery);


/***********************************************
 * Equip module
 ***********************************************/
(function ($) {
    'use strict';

    $('.sp-color-swarm').each(function () {
        var $canvas = $('<canvas class="sp-color-swarm-svg" />');
        $(this).prepend($canvas);
        $canvas.PZT_ColorSwarm();
    });
})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * FAQ card toggles
 ***********************************************/
(function ($) {

    $('.sp-faq-list .sp-faq-card').find('> .card-header').on('click', function() {
        var $card = $(this).closest('.sp-faq-card'),
            $list = $card.closest('.sp-faq-list');

        $list.find('.sp-faq-card').each(function () {
            if($(this).is($card)) { return; }
            $(this).find('> .card-contents').collapse('hide');
            $(this).removeClass('card-open');
        });

        $card.find('> .card-contents').collapse('show');
        $card.addClass('card-open');
    });

})(jQuery);
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

/***********************************************
 * Footer scrolling animation
 ***********************************************/
(function($) {
    'use strict';

    var $window = $(window),
        $footer = $('#sp-footer'),
        $sizing_helper = $('#sp-footer-sizing-helper');

    if (!$footer.hasClass('sp-footer-fixed')) {
        return;
    }

    // footer sizing helper height calculation
    var last_footer_height = -1;
    setInterval(function () {
        if (last_footer_height === $footer.outerHeight()) {
            return;
        }

        last_footer_height = $footer.outerHeight();
        $sizing_helper.css('height', $footer.outerHeight());

        if ($footer.outerHeight() >= ($window.outerHeight() / 1.5)) {
            $footer.css('position', 'static');
            $footer.find('> div').css('opacity', 1);
            $sizing_helper.hide();
        } else {
            $footer.css('position', 'fixed');
            $sizing_helper.show();
        }
    }, 750);

    // scrolling animation
    PZTJS.scrollRAF(function () {
        var helper_offset = $sizing_helper.offset().top,
            wScrollBottom = $window.scrollTop() + $window.outerHeight();

        if (wScrollBottom <= helper_offset || $footer.css('position') === 'static') {
            return;
        }

        $footer.find('> div').css('opacity', (wScrollBottom - helper_offset) / $footer.outerHeight());
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Google maps integration
 ***********************************************/
(function ($) {

    // select map placements
    var $mapPlaces = $('.sp-map-place');

    // map colour theme
    var gmapStyle = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#dddddd"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"dddddd"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#555555"},{"lightness":20}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#aaaaaa"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#f4f4f4"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#f4f4f4"},{"lightness":17},{"weight":1.2}]}];

    // default rewritable map options
    var mapDefaultOptions = {
        zoom                 : 16,
        center               : {lat: 40.731607, lng:-73.997038},
        disableDefaultUI     : false,
        scrollwheel          : false,
        draggable            : true,
        styles               : gmapStyle,
        mapTypeControl       : false,
        navigationControl    : false,
        mapTypeId            : 'roadmap'
    };

    // init maps
    $mapPlaces.each(function() {
        var $map = $(this);
        var mapObj = new google.maps.Map($map.get(0), mapDefaultOptions);
        $map.data('gmap-object', mapObj);
    });

    // equip geocoder
    $mapPlaces.filter('[data-address]').each(function() {
        var $map = $(this),
            mapObj = $map.data('gmap-object'),
            geocoder = new google.maps.Geocoder();

        if (!mapObj || !geocoder) {
            return;
        }

        geocoder.geocode({'address': $map.data('address')}, function (results, status) {
            if (status !== google.maps.GeocoderStatus.OK) {
                console.error('Google Maps are unable to find location: ' + $map.data('address'), status, results);
                return;
            }

            var result = results[0];
            mapObj.setCenter(result.geometry.location);

            var infowindow = new google.maps.InfoWindow({
                content: '<b>' +result.formatted_address + '</b>',
                size   : new google.maps.Size(150, 50)
            });

            var marker = new google.maps.Marker({
                position: result.geometry.location,
                map     : mapObj,
                icon    : 'assets/images/map-pin.png',
                title   : $map.data('address')
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(mapObj, marker);
            });
        });
    });

})(jQuery);
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */


/***********************************************
 * Proper animation delays for mobile menu
 ***********************************************/
(function($) {
    'use strict';

    var $html = $('html'),
        $burger_menu = $('#sp-mobile-nav-container'),
        $burger_trigger = $('#sp-mobile-nav-trigger'),
        animDelay = Modernizr.prefixed('animationDelay');

    $burger_menu.find('.nav_menu > li').each(function() {
        var $this = $(this);
        $this[0].style[animDelay] = 300 + ($this.index() * 150) + 'ms';
    });

    // submenu open trigger
    $burger_menu.find('.menu-item-has-children > a').on('click', function (e) {
        var $this = $(this),
            $current_menu_item = $(this).parent();

        $burger_menu.find('.menu-item-has-children').each(function () {
            if (!$.contains(this, $current_menu_item.get(0))) {
                $(this).find('> a').removeClass('sub-active').next('ul').slideUp(250);
            }
        });

        if ($this.next('ul').is(':visible') === false) {
            $this.addClass('sub-active').next('ul').slideDown(250);
        }

        e.preventDefault();
    });

    // toggle state of the burger menu
    var burger_menu_open = false;
    $burger_trigger.on('click', function (e) {
        e.preventDefault();

        burger_menu_open = !burger_menu_open;
        $html.toggleClass('sp-active-burger-menu', burger_menu_open);

        var header_height = $('#sp-header').outerHeight();
        $burger_menu.css('border-top-width', header_height);

        $burger_menu.find('.sub-active').each(function () {
            $(this).removeClass('sub-active').next('ul').hide();
        });
    });

    // close fullscreen menu on menu item click
    $burger_menu.find('.nav_menu a').on('click', function () {
        if ($(this).parent().hasClass('menu-item-has-children') === false) {
            burger_menu_open && $burger_trigger.trigger('click');
        }
    });

    // fix scrolling issues on mobile when menu is open
    $(document).on('touchmove', function (e) {
        if (burger_menu_open && !$(e.target).closest($burger_menu).length) {
            e.preventDefault();
        }
    });

})(jQuery);


/***********************************************
 * Desktop menu
 ***********************************************/
(function($) {
    'use strict';

    var $win = $(window),
        $header = $('#sp-header');

    // dropdown autoposition
    $win.on('docready load resize', $.debounce(250, function () {
        $header.find('.sub-menu').each(function () {
            var $this = $(this);
            if ($this.offset().left + $this.outerWidth() >= ($win.outerWidth() - 25)) {
                $this.addClass('invert-attach-point');
            }
        });
    }));

    // sticky menu (150 is a scroll offset in pixels)
    PZTJS.scrollRAF(function () {
        if (window.pageYOffset > 150 && !$header.hasClass('header-stuck')) {
            $header.addClass('header-stuck');
        }

        if (window.pageYOffset <= 150 && $header.hasClass('header-stuck')) {
            $header.removeClass('header-stuck');
        }
    });

    // disable jumps for empty-anchor links
    $header.find('.nav_menu a[href="#"]').on('click', function (e) {
        e.preventDefault();
    });

})(jQuery);


/***********************************************
 * Fullscreen search
 ***********************************************/
(function($) {
    'use strict';

    var $toggle = $('#sp-header').find('.sp-search-icon'),
        $searchContainer = $('#sp-search-block-container');

    // focus input when container is visible
    $searchContainer.find('> .search-block-inner').on(PZTJS.transitionEnd, function() {
        $(this).is(':visible') && $(this).find('.search-input').focus();
    });

    // close on click
    $searchContainer.find('.close-search').on('click', function(event) {
        event.preventDefault();
        $searchContainer.removeClass('open');
    });

    // close on esc keyup
    $(document).keyup(function(e) {
        (e.keyCode === 27) && $searchContainer.removeClass('open');
    });

    // open trigger
    $toggle.on('click', function(event) {
        event.preventDefault();
        $searchContainer.addClass('open');
    });

})(jQuery);
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */


/***********************************************
 * Intro init
 ***********************************************/
(function ($) {
    'use strict';

    var $intro = $('.sp-intro'),
        $win = $(window);

    var initImage = function ($intro) {
        if (!$intro.attr('data-background')) { return; }
        $intro.find('> .intro-bg').length || $intro.append('<div class="intro-bg"/>');
        $intro.find('> .intro-bg').css('background-image', 'url(' + $intro.attr('data-background') + ')');
    };

    var initCarousel = function ($intro) {
        $intro.addClass('slick-dots-inside');

        $intro.on('init reInit', function () {
            $(this).find('.swipebox-video').swipebox();
        });

        var slickDefaultOptions = {
            slide        : '.slider-item',
            speed        : 1000,
            dots         : true,
            fade         : true,
            autoplay     : true,
            infinite     : true,
            autoplaySpeed: 7500
        };

        $intro.slick($.extend(slickDefaultOptions, $intro.data('slick')));
    };

    var initVideo = function($intro) {
        var $video = $intro.find('> .video-container'),
            $placeholder = $video.find('> .video-placeholder'),
            $controls = $video.find('> .video-controls');

        $video.on('YTPPlay YTPPause', function(e) {
            if(e.type === 'YTPPlay') { $controls.find('.sp-video-play > i').removeClass('icon-ion-ios-play'); }
            if(e.type === 'YTPPause') { $controls.find('.sp-video-play > i').addClass('icon-ion-ios-play'); }
        });

        $video.on('YTPMuted YTPUnmuted', function(e) {
            if(e.type === 'YTPMuted') { $controls.find('.sp-video-volume > i').removeClass('icon-ion-android-volume-up'); }
            if(e.type === 'YTPUnmuted') { $controls.find('.sp-video-volume > i').addClass('icon-ion-android-volume-up'); }
        });

        $video.YTPlayer({
            videoURL    : $video.data('url'),
            showControls: false,
            containment : 'self',
            loop        : true,
            autoPlay    : true,
            vol         : 25,
            mute        : true,
            showYTLogo  : false,
            startAt     : $video.data('start') || 0,
            stopAt      : $video.data('stop') || 0,
            onReady     : function () {
                $placeholder.fadeOut('fast');
                $controls.fadeIn('fast');
            }
        });

        $controls.find('.sp-video-play').on('click', function (e) {
            e.preventDefault();
            $video.YTPTogglePlay();
        });

        $controls.find('.sp-video-volume').on('click', function (e) {
            e.preventDefault();
            $video.YTPToggleVolume();
        });
    };

    // init all intros on the page
    $intro.each(function () {
        var $this = $(this);
        if ($this.hasClass('sp-intro-carousel')) { initCarousel($this); }
        if ($this.hasClass('sp-intro-video')) { initVideo($this); }
        if ($this.hasClass('sp-intro-image')) { initImage($this); }
    });

    // add scroll effect
    ($win.width() > 680) && PZTJS.scrollRAF(function () {
        $intro.each(function () {
            var $currIntro = $(this),
                introHeight = $currIntro.height(),
                pageYOffset = Math.max(window.pageYOffset, 0);

            if (window.pageYOffset > introHeight) {
                return;
            }

            var translateY = Math.floor(pageYOffset * 0.3) + 'px';
            $currIntro[0].style[Modernizr.prefixed('transform')] = 'translate3d(0, ' + translateY + ', 0)';
            $currIntro.css('opacity', (1 - pageYOffset / introHeight));
        });
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Mailchimp subscription ajax form
 ***********************************************/
(function($) {

    $.ajaxChimp.translations.soprano = {
        'submit': 'Loading, please wait...',
        0: 'We have sent you a confirmation email. Please check your inbox.',
        1: 'The field is empty. Please enter your email.',
        2: 'An email address must contain a single "@" character.',
        3: 'This email seems to be invalid. Please enter a correct one.',
        4: 'This email seems to be invalid. Please enter a correct one.',
        5: 'This email address looks fake or invalid. Please enter a real email address.'
    };

    $('.sp-subscribe-form').each(function () {
        var $form = $(this).closest('form');

        $form.on('submit', function () {
            $form.addClass('mc-loading');
        });

        $form.ajaxChimp({
            language: 'soprano',
            label   : $form.find('> .form-output'),
            callback: function (resp) {
                $form.removeClass('mc-loading');
                $form.toggleClass('mc-valid', (resp.result === 'success'));
                $form.toggleClass('mc-invalid', (resp.result === 'error'));

                if (resp.result === 'success') {
                    $form.find('input[type="email"]').val('');
                }

                setTimeout(function () {
                    $form.removeClass('mc-valid mc-invalid');
                    $form.find('input[type="email"]').focus();
                }, 4500);
            }
        });
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Make clicks on mobile devices smoother
 ***********************************************/
jQuery(document).ready(function () {
    if(typeof FastClick === 'function') {
        FastClick.attach(document.body);
    }
});


/***********************************************
 * Disable hover effects when page is scrolled
 ***********************************************/
(function () {
    var body = document.body,
        timer;

    window.addEventListener('scroll', function () {
        clearTimeout(timer);
        if (!body.classList.contains('disable-hover')) {
            body.classList.add('disable-hover')
        }

        timer = setTimeout(function () {
            body.classList.remove('disable-hover')
        }, 100);
    }, false);
})();
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Scroll to anchor
 ***********************************************/
(function($) {

    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top - 55
                    }, 1500, 'easeInOutExpo', function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        }
                    });
                }
            }
        });

})(jQuery);


/***********************************************
 * Scroll to Top button
 ***********************************************/
(function($) {

    var offset = 500,
        $back_to_top = $('.sp-scroll-top');

    PZTJS.scrollRAF(function() {
        if (window.pageYOffset > offset) {
            $back_to_top.addClass('scroll-top-visible');
        } else {
            $back_to_top.removeClass('scroll-top-visible');
        }
    });

    $back_to_top.on('mouseover mouseout', function() {
        $(this).find('.anno-text').stop().animate({
            width: 'toggle',
            padding: 'toggle'
            // display: 'inline'
        });
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Portfolio shuffle
 ***********************************************/
(function ($) {
    // locate what we want to sort
    var $blocks = $('.sp-portfolio-block');

    // don't run this function if this page does not contain required element
    if ($blocks.length <= 0) {
        return;
    }

    // init shuffle and filters
    $blocks.each(function () {
        var $this = $(this),
            $grid = $this.find('.sp-portfolio-items'),
            $filterBtns = $this.find('.sp-portfolio-sorting a[data-group]');

        // instantiate the plugin
        $grid.pzt_shuffle({
            itemSelector: '[class*="col-"]',
            gutterWidth : 0,
            speed       : 600, // transition/animation speed (milliseconds).
            easing      : 'ease'
        });

        // init filters
        $filterBtns.on('click', function (e) {
            var $this = $(this);

            // hide current label, show current label in title
            $this.parent().siblings().removeClass('active');
            $this.parent().addClass('active');

            // filter elements
            $grid.shuffle('shuffle', $this.data('group'));

            e.preventDefault();
        });
    });
})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Preloader
 ***********************************************/
(function ($) {
    var $window = $(window);

    $window.on('load', function () {
        $window.trigger('pzt.preloader_done');
        setTimeout(function () {
            $('body').addClass('sp-page-loaded');
            $('#sp-preloader').fadeOut('slow');
        }, 250);
    });
})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Progress Bar
 ***********************************************/
(function ($) {

    var $el = $('.sp-progress-bar'),
        delay = 0;

    $el.each(function() {
        var $this = $(this),
            $pv = $this.find('.progress-value'),
            $pb = $this.find('.progress-bar');

        $pv.html('0%');
        $pb.css('width', 0);
    });

    $el.one('appear', function () {
        var $this = $(this),
            $pv = $this.find('.progress-value'),
            $pb = $this.find('.progress-bar');

        setTimeout(function () {
            $pb.animate({
                width: $this.data('value')
            }, {
                duration: 2500,
                easing  : 'easeInOutQuint',
                step    : function (now, fx) {
                    $pv.html(now.toFixed(0) + fx.unit);
                }
            });
        }, delay);
        delay += 300;
    });

    setInterval(function () { delay = 0; }, 1000);

    $(window).one('pzt.preloader_done', function () {
        $el.appear({force_process: true});
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

(function ($) {
    var $elems = $('.sp-animate-numbers h2');

    // set roller numbers first
    $elems.each(function (i) {
        $(this).attr('data-slno', i);
        $(this).addClass('roller-title-number-' + i);
    });

    // run when appear
    $elems.one('appear', function () {
        numberRoller($(this).attr('data-slno'));
    });

    $(window).one('pzt.preloader_done', function() {
        $elems.appear({force_process: true});
    });

    function numberRoller(slno) {
        var min = $('.roller-title-number-' + slno).attr('data-min');
        var max = $('.roller-title-number-' + slno).attr('data-max');
        var timediff = $('.roller-title-number-' + slno).attr('data-delay');
        var increment = $('.roller-title-number-' + slno).attr('data-increment');
        var numdiff = max - min;
        var timeout = (timediff * 1000) / numdiff;
        numberRoll(slno, min, max, increment, timeout);
    }

    function numberRoll(slno, min, max, increment, timeout) {//alert(slno+"="+min+"="+max+"="+increment+"="+timeout);
        if (min <= max) {
            $('.roller-title-number-' + slno).html(min);
            min = parseInt(min, 10) + parseInt(increment, 10);
            setTimeout(function () {
                numberRoll(parseInt(slno, 10), parseInt(min, 10), parseInt(max, 10), parseInt(increment, 10), parseInt(timeout, 10))
            }, timeout);
        } else {
            $('.roller-title-number-' + slno).html(max);
        }
    }

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Clients Slider
 ***********************************************/
(function ($) {

    $('.sp-slick-clients').slick({
        dots          : false,
        infinite      : true,
        adaptiveHeight: true,
        speed         : 750,
        slidesToShow  : 5,
        autoplay      : true,
        arrows        : false,
        responsive    : [{
            breakpoint: 992,
            settings  : {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings  : {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings  : {
                slidesToShow: 1
            }
        }]
    });

})(jQuery);



(function ($) {

    $('.sp-slick-demo').slick({
        dots          : false,
        infinite      : true,
        adaptiveHeight: true,
        speed         : 750,
        slidesToShow  : 3,
        autoplay      : true,
        arrows        : false,
        responsive    : [{
            breakpoint: 992,
            settings  : {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings  : {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings  : {
                slidesToShow: 1
            }
        }]
    });

})(jQuery);


/***********************************************
 * Testimonials Slider
 ***********************************************/
(function ($) {

    $('.sp-slick-testimonials').slick({
        dots          : true,
        infinite      : true,
        speed         : 750,
        slidesToShow  : 1,
        adaptiveHeight: true,
        autoplay      : true,
        arrows        : false,
        autoplaySpeed : 6500
    });

})(jQuery);


/***********************************************
 * "iMac" slider
 ***********************************************/
(function($) {

    $('.sp-slick-imac').slick({
        dots          : true,
        infinite      : true,
        speed         : 450,
        slidesToShow  : 1,
        adaptiveHeight: true,
        autoplay      : true,
        arrows        : false,
        fade          : true,
        easing        : 'linear'
    });

})(jQuery);


/***********************************************
 * Single Post Gallery
 ***********************************************/
(function ($) {

    $('.sp-slick-post-gallery').slick({
        dots          : false,
        infinite      : true,
        speed         : 750,
        slidesToShow  : 1,
        adaptiveHeight: true,
        autoplay      : true,
        autoplaySpeed : 5000,
        nextArrow     : '<div class="slick-next circle"><i class="icon-angle-right"></i></div>',
        prevArrow     : '<div class="slick-prev circle"><i class="icon-angle-left"></i></div>',
    });

})(jQuery);


/***************************************************
 * Add slick-animated class when transition is over
 ***************************************************/
(function ($) {

    var $sliders = $('.slick-initialized');

    $sliders.on('initialAnimate reInit afterChange', function (e, slick, currentSlide) {
        currentSlide = currentSlide || 0;
        slick = slick || $(this).slick('getSlick');

        $(slick.$slides).removeClass('slick-animated');
        $(slick.$slides[currentSlide]).addClass('slick-animated');
    });

    $sliders.trigger('initialAnimate');

})(jQuery);


/***************************************************
 * Stop sliders that is not currently in viewport
 ***************************************************/
(function ($) {

    var $sliders = $('.slick-initialized');

    PZTJS.scrollRAF($.throttle(250, function () {
        $sliders.each(function () {
            var $this = $(this),
                $slick = $this.slick('getSlick');

            // stop slider
            if (!PZTJS.isElementInViewport(this) && !$slick.paused) {
                $this.slick('pause');
            }

            // unpause slider
            if (PZTJS.isElementInViewport(this) && $slick.paused) {
                $this.slick('play');
            }
        });
    }));

})(jQuery);


/***************************************************
 * Integrate WOW.js with slick
 ***************************************************/
(function($) {

    $('.slick-initialized').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        $(slick.$slides[nextSlide]).find('.wow, .re-animate').each(function () {
            var el = $(this),
                newone = el.clone(true);

            el.before(newone);
            el.remove();
        });
    });

})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Sticky sidebar feature
 ***********************************************/
(function ($) {

    $('.sp-sidebar.sticky').stick_in_parent({
        offset_top: $('#sp-header').outerHeight() + 30
    });

})(jQuery);
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */


/***********************************************
 * Integration with text rotator jQuery plugin
 ***********************************************/
(function ($) {
    'use strict';

    $('.sp-text-rotate').each(function () {
        var $this = $(this);

        $this.textrotator({
            animation: $this.data('animation'),
            speed    : $this.data('speed'),
            separator: '|'
        });
    });
})(jQuery);
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

'use strict';

/***********************************************
 * Typed.js integration
 ***********************************************/
jQuery(window).one('pzt.preloader_done', function () {
    var $ = jQuery;

    // process each typed-enabled element
    $('[data-typed-str]').each(function () {
        var $this = $(this),
            texts = $this.attr('data-typed-str').split('|');

        $this.html('').append('<span class="typed-container"></span>');
        $this.find('> .typed-container').typed({
            strings   : texts,
            typeSpeed : 180,
            loop      : ($this.attr('data-typed-repeat') === 'yes'),
            backDelay : 1500,
            showCursor: ($this.attr('data-typed-cursor') === 'yes')
        });
    });
});
/*! ===================================
 *  Author: Nazarkin Roman, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */

/***********************************************
 * Load theme fonts via WebFontLoader
 ***********************************************/
'use strict';
(function () {
    if (typeof SOPRANO_FONTS === 'undefined') {
        return;
    }

    WebFont.load({
        google: {families: SOPRANO_FONTS}
    });
})();
/*! ===================================
 *  Author: Roman Nazarkin, Egor Dankov
 *  -----------------------------------
 *  PuzzleThemes
 *  =================================== */


/***********************************************
 * WOW.js appearance animation engine
 ***********************************************/
(function ($) {
    'use strict';

    var animationDuration = Modernizr.prefixed('animationDuration');

    // quit from function on mobile devices
    // ======================================
    if (PZTJS.isMobile()) {
        $('.wow.sequenced').removeClass('wow sequenced');
        $('.wow').removeClass('wow');
        return;
    }


    // sequenced animations
    // ======================================
    var seqElements = $('.wow.sequenced').each(function() {
        var $this = $(this),
            classname = $this.attr('class'),
            animDuration = $this.data('wow-duration') || '1s',
            childrenSelector = $this.data('wow-children'),
            fxname = /fx-([a-zA-Z]+)/g.exec(classname);

        // remove classnames
        $this.removeClass('wow sequenced fx-' + fxname[1]);

        // select children elements
        var $children = $this.find('> *');
        if (!childrenSelector) {
            if ($this.hasClass('wpb_column')) { // || $this.hasClass('wpb_row')
                $children = $this.find('.wpb_wrapper > *');
            }
        } else {
            $children = $this.find(childrenSelector);
        }

        // hide all non-animated children
        $children.css('visibility', 'hidden');

        // set proper animation speed
        for (var i = 0; i < $children.length; i++) {
            $children.get(i).style[animationDuration] = animDuration;
        }

        // bind animation end event
        $children.one(PZTJS.animationEnd, function() {
            $(this).removeClass('animated ' + fxname[1]);
        });

        // save data for further execution
        $this.data({
            wow_children: $children,
            wow_fx      : fxname[1]
        });
    });

    // start animations when element appears in viewport
    seqElements.one('appear', function () {
        var $this = $(this), rowStart = null;

        // get fx name
        var fxname = $this.data('wow_fx'),
            $children = $this.data('wow_children'),
            el_index = 0, row_id = 0;

        // run animation sequence
        $children.each(function () {
            var $el = $(this), currTopPosition = $el.position().top;

            // check for a new row
            if (currTopPosition !== rowStart) {
                el_index = 0;
                rowStart = currTopPosition;
                row_id++;
            }

            // run animation after some delay
            setTimeout(function() {
                $el.addClass('animated ' + fxname);
                $el.css('visibility', 'visible');
            }, (el_index * 300) + (row_id * 150));

            el_index++;
        });
    });


    // regular wow engine
    // ======================================
    var regWOW = new WOW({
        boxClass       : 'wow',      // animated element css class (default is wow)
        animateClass   : 'animated', // animation css class (default is animated)
        offset         : 0,          // distance to the element when triggering the animation (default is 0)
        mobile         : false,      // trigger animations on mobile devices (default is true)
        live           : true,       // act on asynchronously loaded content (default is true)
        scrollContainer: null,       // optional scroll container selector, otherwise use window
        callback       : function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        }
    });


    // run both engines once preloading done
    // ======================================
    $(window).one('pzt.preloader_done', function() {
        seqElements.selector = false;
        seqElements.appear({force_process: true});

        regWOW.init();
    });

})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFuaW1hdGVkX2NpcmNsZXMuanMiLCJibG9nLmpzIiwiY2xpZW50cy5qcyIsImNvbG9yX3N3YXJtLmpzIiwiZmFxLmpzIiwiZm9vdGVyLmpzIiwiZ29vZ2xlX21hcHMuanMiLCJoZWFkZXIuanMiLCJpbnRyby5qcyIsIm1haWxjaGltcF9zdWJzY3JpYmUuanMiLCJvdGhlcnMuanMiLCJwYWdlX3Njcm9sbGluZy5qcyIsInBvcnRmb2xpby5qcyIsInByZWxvYWRlci5qcyIsInByb2dyZXNzX2JhcnMuanMiLCJyb2xsaW5nX251bWJlcnMuanMiLCJzbGlkZXJzLmpzIiwic3RpY2t5X3NpZGViYXJzLmpzIiwidGV4dHJvdGF0b3IuanMiLCJ0eXBlZC5qcyIsIndmbF9pbml0LmpzIiwid293LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM5R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3BEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDNUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQy9JQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM5R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbk1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNsQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImNvbnRyb2xsZXJzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBBbmltYXRlZCBDaXJjbGVzXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCQpIHtcblxuICAgIHZhciBlbCA9ICQoJy5zcC1jaXJjbGUnKSxcbiAgICAgICAgZGVsYXkgPSAwLFxuICAgICAgICBvcHRpb25zID0ge1xuICAgICAgICAgICAgdmFsdWUgICAgIDogMCxcbiAgICAgICAgICAgIHNpemUgICAgICA6IDEyNSxcbiAgICAgICAgICAgIHRoaWNrbmVzcyA6IDIsXG4gICAgICAgICAgICBmaWxsICAgICAgOiB7Y29sb3I6IFwiIzExMVwifSxcbiAgICAgICAgICAgIGVtcHR5RmlsbCA6IFwiI2RkZFwiLFxuICAgICAgICAgICAgc3RhcnRBbmdsZTogMzAwLFxuICAgICAgICAgICAgYW5pbWF0aW9uIDoge2R1cmF0aW9uOiAyNTAwLCBlYXNpbmc6ICdlYXNlSW5PdXRRdWludCd9XG4gICAgICAgIH07XG5cbiAgICBlbC5vbmUoJ2FwcGVhcicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyICRlbCA9ICQodGhpcyk7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGVsLmNpcmNsZVByb2dyZXNzKCd2YWx1ZScsICRlbC5kYXRhKCd2YWx1ZScpKTtcbiAgICAgICAgfSwgZGVsYXkpO1xuICAgICAgICBkZWxheSArPSAxNTA7XG4gICAgfSk7XG5cbiAgICBlbC5jaXJjbGVQcm9ncmVzcyhvcHRpb25zKS5vbignY2lyY2xlLWFuaW1hdGlvbi1wcm9ncmVzcycsIGZ1bmN0aW9uIChldmVudCwgcHJvZ3Jlc3MsIHN0ZXBWYWx1ZSkge1xuICAgICAgICAkKHRoaXMpLmZpbmQoJ3NwYW4nKS50ZXh0KChzdGVwVmFsdWUgKiAxMDApLnRvRml4ZWQoMSkpO1xuICAgIH0pO1xuXG4gICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkgeyBkZWxheSA9IDA7IH0sIDEwMDApO1xuXG4gICAgJCh3aW5kb3cpLm9uZSgncHp0LnByZWxvYWRlcl9kb25lJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIGVsLmFwcGVhcih7Zm9yY2VfcHJvY2VzczogdHJ1ZX0pO1xuICAgIH0pO1xuXG59KShqUXVlcnkpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogTmF6YXJraW4gUm9tYW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogTWFzb25yeSBCbG9nIExheW91dFxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgdmFyICRncmlkID0gJCgnI3NwLWJsb2ctZ3JpZCcpOyAvLyBsb2NhdGUgd2hhdCB3ZSB3YW50IHRvIHNvcnRcblxuICAgIC8vIGRvbid0IHJ1biB0aGlzIGZ1bmN0aW9uIGlmIHRoaXMgcGFnZSBkb2VzIG5vdCBjb250YWluIHJlcXVpcmVkIGVsZW1lbnRcbiAgICBpZiAoJGdyaWQubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIGluc3RhbnRpYXRlIHRoZSBwbHVnaW5cbiAgICAkZ3JpZC5wenRfc2h1ZmZsZSh7XG4gICAgICAgIGl0ZW1TZWxlY3RvcjogJ1tjbGFzcyo9XCJjb2wtXCJdJyxcbiAgICAgICAgZ3V0dGVyV2lkdGggOiAwLFxuICAgICAgICBzcGVlZCAgICAgICA6IDYwMCwgLy8gdHJhbnNpdGlvbi9hbmltYXRpb24gc3BlZWQgKG1pbGxpc2Vjb25kcykuXG4gICAgICAgIGVhc2luZyAgICAgIDogJ2Vhc2UnXG4gICAgfSk7XG59KShqUXVlcnkpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogTmF6YXJraW4gUm9tYW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogTWFzb25yeSBDbGllbnRzIExheW91dFxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgdmFyICRncmlkID0gJCgnLnNwLWNsaWVudHMtZ3JpZCcpOyAvLyBsb2NhdGUgd2hhdCB3ZSB3YW50IHRvIHNvcnRcblxuICAgIC8vIGRvbid0IHJ1biB0aGlzIGZ1bmN0aW9uIGlmIHRoaXMgcGFnZSBkb2VzIG5vdCBjb250YWluIHJlcXVpcmVkIGVsZW1lbnRcbiAgICBpZiAoJGdyaWQubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIGluc3RhbnRpYXRlIHRoZSBwbHVnaW5cbiAgICAkZ3JpZC5wenRfc2h1ZmZsZSh7XG4gICAgICAgIGl0ZW1TZWxlY3RvcjogJ1tjbGFzcyo9XCJjb2wtXCJdJyxcbiAgICAgICAgZ3V0dGVyV2lkdGggOiAwLFxuICAgICAgICBzcGVlZCAgICAgICA6IDYwMCwgLy8gdHJhbnNpdGlvbi9hbmltYXRpb24gc3BlZWQgKG1pbGxpc2Vjb25kcykuXG4gICAgICAgIGVhc2luZyAgICAgIDogJ2Vhc2UnXG4gICAgfSk7XG59KShqUXVlcnkpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogUm9tYW4gTmF6YXJraW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogQ29sb3JTd2FybSBtb2R1bGUgQVBJXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCQpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICAkLmZuLlBaVF9Db2xvclN3YXJtID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgY2FudmFzID0gJCh0aGlzKVswXTtcbiAgICAgICAgdmFyIGN0eDtcbiAgICAgICAgdmFyIG51bUNpcmNsZXMgPSAxMDA7XG5cbiAgICAgICAgdmFyIHJlc2l6ZSA9IHdpbmRvdy5yZXNpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjYW52YXMuaGVpZ2h0ID0gJChjYW52YXMpLnBhcmVudCgpLm91dGVySGVpZ2h0KCk7XG4gICAgICAgICAgICBjYW52YXMud2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcbiAgICAgICAgfTtcblxuICAgICAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGN0eCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICAgICAgICAgICAgcmVzaXplKCk7XG5cbiAgICAgICAgICAgIHZhciBjaXJjbGVzID0gW10sXG4gICAgICAgICAgICAgICAgY29sb3JzID0gcmFuZG9tQ29sb3Ioe2x1bWlub3NpdHk6ICdsaWdodCcsIGNvdW50OiBudW1DaXJjbGVzfSk7XG5cbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbnVtQ2lyY2xlczsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIHggPSBNYXRoLnJhbmRvbSgpICogY2FudmFzLndpZHRoO1xuICAgICAgICAgICAgICAgIHZhciB5ID0gTWF0aC5yYW5kb20oKSAqIGNhbnZhcy5oZWlnaHQ7XG4gICAgICAgICAgICAgICAgdmFyIGMgPSBuZXcgQ2lyY2xlKHgsIHksIGNvbG9yc1tpXSk7XG4gICAgICAgICAgICAgICAgYy5kcmF3KCk7XG4gICAgICAgICAgICAgICAgY2lyY2xlcy5wdXNoKGMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgcmVxdWVzdEFuaW1GcmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgICAgICAgICAgICAgICAgd2luZG93Lm9SZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgICAgICAgICAgICAgICAgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChhLCAxRTMgLyA2MCk7XG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9KCk7XG5cbiAgICAgICAgICAgIHZhciBsb29wID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJlcXVlc3RBbmltRnJhbWUobG9vcCk7XG4gICAgICAgICAgICAgICAgY3R4LmNsZWFyUmVjdCgwLCAwLCBjYW52YXMud2lkdGgsIGNhbnZhcy5oZWlnaHQpO1xuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2lyY2xlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBjaXJjbGVzW2ldLmZyYW1lKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgbG9vcCgpO1xuICAgICAgICB9KTtcblxuICAgICAgICB2YXIgQ2lyY2xlID0gZnVuY3Rpb24gKHgsIHksIGMpIHtcbiAgICAgICAgICAgIHRoaXMucG9zID0gW3gsIHldO1xuICAgICAgICAgICAgdGhpcy5yID0gKDEuNSAqIE1hdGgucmFuZG9tKCkpICsgMTtcbiAgICAgICAgICAgIHRoaXMuYyA9IGM7XG4gICAgICAgICAgICB0aGlzLnYgPSBbXG4gICAgICAgICAgICAgICAgKE1hdGgucmFuZG9tKCkgLSAwLjUpICogMC4zLFxuICAgICAgICAgICAgICAgIChNYXRoLnJhbmRvbSgpIC0gMC41KSAqIDAuM1xuICAgICAgICAgICAgXTtcbiAgICAgICAgfTtcblxuICAgICAgICBDaXJjbGUucHJvdG90eXBlLmdldEJvdW5kID0gZnVuY3Rpb24gKGkpIHtcbiAgICAgICAgICAgIHJldHVybiBpID8gY2FudmFzLmhlaWdodCA6IGNhbnZhcy53aWR0aDtcbiAgICAgICAgfTtcblxuICAgICAgICB2YXIgaTtcbiAgICAgICAgQ2lyY2xlLnByb3RvdHlwZS5mcmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCAyOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wb3NbaV0gPiB0aGlzLmdldEJvdW5kKGkpIC0gMTApIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52W2ldICo9IC0xO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmICh0aGlzLnBvc1tpXSA8IDEwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudltpXSAqPSAtMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5wb3NbaV0gKz0gdGhpcy52W2ldICogMTA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuZHJhdygpO1xuICAgICAgICB9O1xuXG4gICAgICAgIENpcmNsZS5wcm90b3R5cGUuZHJhdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGN0eC5maWxsU3R5bGUgPSB0aGlzLmM7XG4gICAgICAgICAgICBjdHguYmVnaW5QYXRoKCk7XG4gICAgICAgICAgICBjdHguYXJjKHRoaXMucG9zWzBdLCB0aGlzLnBvc1sxXSwgdGhpcy5yLCAwLCAyICogTWF0aC5QSSwgZmFsc2UpO1xuICAgICAgICAgICAgY3R4LmZpbGwoKTtcbiAgICAgICAgfTtcbiAgICB9O1xufSkoalF1ZXJ5KTtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIEVxdWlwIG1vZHVsZVxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgJCgnLnNwLWNvbG9yLXN3YXJtJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciAkY2FudmFzID0gJCgnPGNhbnZhcyBjbGFzcz1cInNwLWNvbG9yLXN3YXJtLXN2Z1wiIC8+Jyk7XG4gICAgICAgICQodGhpcykucHJlcGVuZCgkY2FudmFzKTtcbiAgICAgICAgJGNhbnZhcy5QWlRfQ29sb3JTd2FybSgpO1xuICAgIH0pO1xufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IE5hemFya2luIFJvbWFuLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIEZBUSBjYXJkIHRvZ2dsZXNcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbiAoJCkge1xuXG4gICAgJCgnLnNwLWZhcS1saXN0IC5zcC1mYXEtY2FyZCcpLmZpbmQoJz4gLmNhcmQtaGVhZGVyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkY2FyZCA9ICQodGhpcykuY2xvc2VzdCgnLnNwLWZhcS1jYXJkJyksXG4gICAgICAgICAgICAkbGlzdCA9ICRjYXJkLmNsb3Nlc3QoJy5zcC1mYXEtbGlzdCcpO1xuXG4gICAgICAgICRsaXN0LmZpbmQoJy5zcC1mYXEtY2FyZCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYoJCh0aGlzKS5pcygkY2FyZCkpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgICAkKHRoaXMpLmZpbmQoJz4gLmNhcmQtY29udGVudHMnKS5jb2xsYXBzZSgnaGlkZScpO1xuICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnY2FyZC1vcGVuJyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRjYXJkLmZpbmQoJz4gLmNhcmQtY29udGVudHMnKS5jb2xsYXBzZSgnc2hvdycpO1xuICAgICAgICAkY2FyZC5hZGRDbGFzcygnY2FyZC1vcGVuJyk7XG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBSb21hbiBOYXphcmtpbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogRm9vdGVyIHNjcm9sbGluZyBhbmltYXRpb25cbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbigkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyICR3aW5kb3cgPSAkKHdpbmRvdyksXG4gICAgICAgICRmb290ZXIgPSAkKCcjc3AtZm9vdGVyJyksXG4gICAgICAgICRzaXppbmdfaGVscGVyID0gJCgnI3NwLWZvb3Rlci1zaXppbmctaGVscGVyJyk7XG5cbiAgICBpZiAoISRmb290ZXIuaGFzQ2xhc3MoJ3NwLWZvb3Rlci1maXhlZCcpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBmb290ZXIgc2l6aW5nIGhlbHBlciBoZWlnaHQgY2FsY3VsYXRpb25cbiAgICB2YXIgbGFzdF9mb290ZXJfaGVpZ2h0ID0gLTE7XG4gICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAobGFzdF9mb290ZXJfaGVpZ2h0ID09PSAkZm9vdGVyLm91dGVySGVpZ2h0KCkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGxhc3RfZm9vdGVyX2hlaWdodCA9ICRmb290ZXIub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgJHNpemluZ19oZWxwZXIuY3NzKCdoZWlnaHQnLCAkZm9vdGVyLm91dGVySGVpZ2h0KCkpO1xuXG4gICAgICAgIGlmICgkZm9vdGVyLm91dGVySGVpZ2h0KCkgPj0gKCR3aW5kb3cub3V0ZXJIZWlnaHQoKSAvIDEuNSkpIHtcbiAgICAgICAgICAgICRmb290ZXIuY3NzKCdwb3NpdGlvbicsICdzdGF0aWMnKTtcbiAgICAgICAgICAgICRmb290ZXIuZmluZCgnPiBkaXYnKS5jc3MoJ29wYWNpdHknLCAxKTtcbiAgICAgICAgICAgICRzaXppbmdfaGVscGVyLmhpZGUoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICRmb290ZXIuY3NzKCdwb3NpdGlvbicsICdmaXhlZCcpO1xuICAgICAgICAgICAgJHNpemluZ19oZWxwZXIuc2hvdygpO1xuICAgICAgICB9XG4gICAgfSwgNzUwKTtcblxuICAgIC8vIHNjcm9sbGluZyBhbmltYXRpb25cbiAgICBQWlRKUy5zY3JvbGxSQUYoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgaGVscGVyX29mZnNldCA9ICRzaXppbmdfaGVscGVyLm9mZnNldCgpLnRvcCxcbiAgICAgICAgICAgIHdTY3JvbGxCb3R0b20gPSAkd2luZG93LnNjcm9sbFRvcCgpICsgJHdpbmRvdy5vdXRlckhlaWdodCgpO1xuXG4gICAgICAgIGlmICh3U2Nyb2xsQm90dG9tIDw9IGhlbHBlcl9vZmZzZXQgfHwgJGZvb3Rlci5jc3MoJ3Bvc2l0aW9uJykgPT09ICdzdGF0aWMnKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAkZm9vdGVyLmZpbmQoJz4gZGl2JykuY3NzKCdvcGFjaXR5JywgKHdTY3JvbGxCb3R0b20gLSBoZWxwZXJfb2Zmc2V0KSAvICRmb290ZXIub3V0ZXJIZWlnaHQoKSk7XG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBHb29nbGUgbWFwcyBpbnRlZ3JhdGlvblxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICAvLyBzZWxlY3QgbWFwIHBsYWNlbWVudHNcbiAgICB2YXIgJG1hcFBsYWNlcyA9ICQoJy5zcC1tYXAtcGxhY2UnKTtcblxuICAgIC8vIG1hcCBjb2xvdXIgdGhlbWVcbiAgICB2YXIgZ21hcFN0eWxlID0gW3tcImZlYXR1cmVUeXBlXCI6XCJ3YXRlclwiLFwiZWxlbWVudFR5cGVcIjpcImdlb21ldHJ5XCIsXCJzdHlsZXJzXCI6W3tcImNvbG9yXCI6XCIjZGRkZGRkXCJ9LHtcImxpZ2h0bmVzc1wiOjE3fV19LHtcImZlYXR1cmVUeXBlXCI6XCJsYW5kc2NhcGVcIixcImVsZW1lbnRUeXBlXCI6XCJnZW9tZXRyeVwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiI2YyZjJmMlwifSx7XCJsaWdodG5lc3NcIjoyMH1dfSx7XCJmZWF0dXJlVHlwZVwiOlwicm9hZC5oaWdod2F5XCIsXCJlbGVtZW50VHlwZVwiOlwiZ2VvbWV0cnkuZmlsbFwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiI2ZmZmZmZlwifSx7XCJsaWdodG5lc3NcIjoxN31dfSx7XCJmZWF0dXJlVHlwZVwiOlwicm9hZC5oaWdod2F5XCIsXCJlbGVtZW50VHlwZVwiOlwiZ2VvbWV0cnkuc3Ryb2tlXCIsXCJzdHlsZXJzXCI6W3tcImNvbG9yXCI6XCIjZmZmZmZmXCJ9LHtcImxpZ2h0bmVzc1wiOjI5fSx7XCJ3ZWlnaHRcIjowLjJ9XX0se1wiZmVhdHVyZVR5cGVcIjpcInJvYWQuYXJ0ZXJpYWxcIixcImVsZW1lbnRUeXBlXCI6XCJnZW9tZXRyeVwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiI2ZmZmZmZlwifSx7XCJsaWdodG5lc3NcIjoxOH1dfSx7XCJmZWF0dXJlVHlwZVwiOlwicm9hZC5sb2NhbFwiLFwiZWxlbWVudFR5cGVcIjpcImdlb21ldHJ5XCIsXCJzdHlsZXJzXCI6W3tcImNvbG9yXCI6XCIjZmZmZmZmXCJ9LHtcImxpZ2h0bmVzc1wiOjE2fV19LHtcImZlYXR1cmVUeXBlXCI6XCJwb2lcIixcImVsZW1lbnRUeXBlXCI6XCJnZW9tZXRyeVwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiI2Y1ZjVmNVwifSx7XCJsaWdodG5lc3NcIjoyMX1dfSx7XCJmZWF0dXJlVHlwZVwiOlwicG9pLnBhcmtcIixcImVsZW1lbnRUeXBlXCI6XCJnZW9tZXRyeVwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiZGRkZGRkXCJ9LHtcImxpZ2h0bmVzc1wiOjIxfV19LHtcImVsZW1lbnRUeXBlXCI6XCJsYWJlbHMudGV4dC5zdHJva2VcIixcInN0eWxlcnNcIjpbe1widmlzaWJpbGl0eVwiOlwib2ZmXCJ9LHtcImNvbG9yXCI6XCIjZmZmZmZmXCJ9LHtcImxpZ2h0bmVzc1wiOjE2fV19LHtcImVsZW1lbnRUeXBlXCI6XCJsYWJlbHMudGV4dC5maWxsXCIsXCJzdHlsZXJzXCI6W3tcInNhdHVyYXRpb25cIjozNn0se1wiY29sb3JcIjpcIiM1NTU1NTVcIn0se1wibGlnaHRuZXNzXCI6MjB9XX0se1wiZWxlbWVudFR5cGVcIjpcImxhYmVscy5pY29uXCIsXCJzdHlsZXJzXCI6W3tcInZpc2liaWxpdHlcIjpcIm9mZlwifV19LHtcImZlYXR1cmVUeXBlXCI6XCJ0cmFuc2l0XCIsXCJlbGVtZW50VHlwZVwiOlwiZ2VvbWV0cnlcIixcInN0eWxlcnNcIjpbe1wiY29sb3JcIjpcIiNhYWFhYWFcIn0se1wibGlnaHRuZXNzXCI6MTl9XX0se1wiZmVhdHVyZVR5cGVcIjpcImFkbWluaXN0cmF0aXZlXCIsXCJlbGVtZW50VHlwZVwiOlwiZ2VvbWV0cnkuZmlsbFwiLFwic3R5bGVyc1wiOlt7XCJjb2xvclwiOlwiI2Y0ZjRmNFwifSx7XCJsaWdodG5lc3NcIjoyMH1dfSx7XCJmZWF0dXJlVHlwZVwiOlwiYWRtaW5pc3RyYXRpdmVcIixcImVsZW1lbnRUeXBlXCI6XCJnZW9tZXRyeS5zdHJva2VcIixcInN0eWxlcnNcIjpbe1wiY29sb3JcIjpcIiNmNGY0ZjRcIn0se1wibGlnaHRuZXNzXCI6MTd9LHtcIndlaWdodFwiOjEuMn1dfV07XG5cbiAgICAvLyBkZWZhdWx0IHJld3JpdGFibGUgbWFwIG9wdGlvbnNcbiAgICB2YXIgbWFwRGVmYXVsdE9wdGlvbnMgPSB7XG4gICAgICAgIHpvb20gICAgICAgICAgICAgICAgIDogMTYsXG4gICAgICAgIGNlbnRlciAgICAgICAgICAgICAgIDoge2xhdDogNDAuNzMxNjA3LCBsbmc6LTczLjk5NzAzOH0sXG4gICAgICAgIGRpc2FibGVEZWZhdWx0VUkgICAgIDogZmFsc2UsXG4gICAgICAgIHNjcm9sbHdoZWVsICAgICAgICAgIDogZmFsc2UsXG4gICAgICAgIGRyYWdnYWJsZSAgICAgICAgICAgIDogdHJ1ZSxcbiAgICAgICAgc3R5bGVzICAgICAgICAgICAgICAgOiBnbWFwU3R5bGUsXG4gICAgICAgIG1hcFR5cGVDb250cm9sICAgICAgIDogZmFsc2UsXG4gICAgICAgIG5hdmlnYXRpb25Db250cm9sICAgIDogZmFsc2UsXG4gICAgICAgIG1hcFR5cGVJZCAgICAgICAgICAgIDogJ3JvYWRtYXAnXG4gICAgfTtcblxuICAgIC8vIGluaXQgbWFwc1xuICAgICRtYXBQbGFjZXMuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICRtYXAgPSAkKHRoaXMpO1xuICAgICAgICB2YXIgbWFwT2JqID0gbmV3IGdvb2dsZS5tYXBzLk1hcCgkbWFwLmdldCgwKSwgbWFwRGVmYXVsdE9wdGlvbnMpO1xuICAgICAgICAkbWFwLmRhdGEoJ2dtYXAtb2JqZWN0JywgbWFwT2JqKTtcbiAgICB9KTtcblxuICAgIC8vIGVxdWlwIGdlb2NvZGVyXG4gICAgJG1hcFBsYWNlcy5maWx0ZXIoJ1tkYXRhLWFkZHJlc3NdJykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICRtYXAgPSAkKHRoaXMpLFxuICAgICAgICAgICAgbWFwT2JqID0gJG1hcC5kYXRhKCdnbWFwLW9iamVjdCcpLFxuICAgICAgICAgICAgZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXIoKTtcblxuICAgICAgICBpZiAoIW1hcE9iaiB8fCAhZ2VvY29kZXIpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGdlb2NvZGVyLmdlb2NvZGUoeydhZGRyZXNzJzogJG1hcC5kYXRhKCdhZGRyZXNzJyl9LCBmdW5jdGlvbiAocmVzdWx0cywgc3RhdHVzKSB7XG4gICAgICAgICAgICBpZiAoc3RhdHVzICE9PSBnb29nbGUubWFwcy5HZW9jb2RlclN0YXR1cy5PSykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0dvb2dsZSBNYXBzIGFyZSB1bmFibGUgdG8gZmluZCBsb2NhdGlvbjogJyArICRtYXAuZGF0YSgnYWRkcmVzcycpLCBzdGF0dXMsIHJlc3VsdHMpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IHJlc3VsdHNbMF07XG4gICAgICAgICAgICBtYXBPYmouc2V0Q2VudGVyKHJlc3VsdC5nZW9tZXRyeS5sb2NhdGlvbik7XG5cbiAgICAgICAgICAgIHZhciBpbmZvd2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICc8Yj4nICtyZXN1bHQuZm9ybWF0dGVkX2FkZHJlc3MgKyAnPC9iPicsXG4gICAgICAgICAgICAgICAgc2l6ZSAgIDogbmV3IGdvb2dsZS5tYXBzLlNpemUoMTUwLCA1MClcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlc3VsdC5nZW9tZXRyeS5sb2NhdGlvbixcbiAgICAgICAgICAgICAgICBtYXAgICAgIDogbWFwT2JqLFxuICAgICAgICAgICAgICAgIGljb24gICAgOiAnYXNzZXRzL2ltYWdlcy9tYXAtcGluLnBuZycsXG4gICAgICAgICAgICAgICAgdGl0bGUgICA6ICRtYXAuZGF0YSgnYWRkcmVzcycpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFya2VyLCAnY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaW5mb3dpbmRvdy5vcGVuKG1hcE9iaiwgbWFya2VyKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IFJvbWFuIE5hemFya2luLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIFByb3BlciBhbmltYXRpb24gZGVsYXlzIGZvciBtb2JpbGUgbWVudVxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uKCQpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICB2YXIgJGh0bWwgPSAkKCdodG1sJyksXG4gICAgICAgICRidXJnZXJfbWVudSA9ICQoJyNzcC1tb2JpbGUtbmF2LWNvbnRhaW5lcicpLFxuICAgICAgICAkYnVyZ2VyX3RyaWdnZXIgPSAkKCcjc3AtbW9iaWxlLW5hdi10cmlnZ2VyJyksXG4gICAgICAgIGFuaW1EZWxheSA9IE1vZGVybml6ci5wcmVmaXhlZCgnYW5pbWF0aW9uRGVsYXknKTtcblxuICAgICRidXJnZXJfbWVudS5maW5kKCcubmF2X21lbnUgPiBsaScpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgICR0aGlzWzBdLnN0eWxlW2FuaW1EZWxheV0gPSAzMDAgKyAoJHRoaXMuaW5kZXgoKSAqIDE1MCkgKyAnbXMnO1xuICAgIH0pO1xuXG4gICAgLy8gc3VibWVudSBvcGVuIHRyaWdnZXJcbiAgICAkYnVyZ2VyX21lbnUuZmluZCgnLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4gPiBhJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICRjdXJyZW50X21lbnVfaXRlbSA9ICQodGhpcykucGFyZW50KCk7XG5cbiAgICAgICAgJGJ1cmdlcl9tZW51LmZpbmQoJy5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoISQuY29udGFpbnModGhpcywgJGN1cnJlbnRfbWVudV9pdGVtLmdldCgwKSkpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmZpbmQoJz4gYScpLnJlbW92ZUNsYXNzKCdzdWItYWN0aXZlJykubmV4dCgndWwnKS5zbGlkZVVwKDI1MCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICgkdGhpcy5uZXh0KCd1bCcpLmlzKCc6dmlzaWJsZScpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgJHRoaXMuYWRkQ2xhc3MoJ3N1Yi1hY3RpdmUnKS5uZXh0KCd1bCcpLnNsaWRlRG93bigyNTApO1xuICAgICAgICB9XG5cbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG4gICAgLy8gdG9nZ2xlIHN0YXRlIG9mIHRoZSBidXJnZXIgbWVudVxuICAgIHZhciBidXJnZXJfbWVudV9vcGVuID0gZmFsc2U7XG4gICAgJGJ1cmdlcl90cmlnZ2VyLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICBidXJnZXJfbWVudV9vcGVuID0gIWJ1cmdlcl9tZW51X29wZW47XG4gICAgICAgICRodG1sLnRvZ2dsZUNsYXNzKCdzcC1hY3RpdmUtYnVyZ2VyLW1lbnUnLCBidXJnZXJfbWVudV9vcGVuKTtcblxuICAgICAgICB2YXIgaGVhZGVyX2hlaWdodCA9ICQoJyNzcC1oZWFkZXInKS5vdXRlckhlaWdodCgpO1xuICAgICAgICAkYnVyZ2VyX21lbnUuY3NzKCdib3JkZXItdG9wLXdpZHRoJywgaGVhZGVyX2hlaWdodCk7XG5cbiAgICAgICAgJGJ1cmdlcl9tZW51LmZpbmQoJy5zdWItYWN0aXZlJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdzdWItYWN0aXZlJykubmV4dCgndWwnKS5oaWRlKCk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gY2xvc2UgZnVsbHNjcmVlbiBtZW51IG9uIG1lbnUgaXRlbSBjbGlja1xuICAgICRidXJnZXJfbWVudS5maW5kKCcubmF2X21lbnUgYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCQodGhpcykucGFyZW50KCkuaGFzQ2xhc3MoJ21lbnUtaXRlbS1oYXMtY2hpbGRyZW4nKSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGJ1cmdlcl9tZW51X29wZW4gJiYgJGJ1cmdlcl90cmlnZ2VyLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIGZpeCBzY3JvbGxpbmcgaXNzdWVzIG9uIG1vYmlsZSB3aGVuIG1lbnUgaXMgb3BlblxuICAgICQoZG9jdW1lbnQpLm9uKCd0b3VjaG1vdmUnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICBpZiAoYnVyZ2VyX21lbnVfb3BlbiAmJiAhJChlLnRhcmdldCkuY2xvc2VzdCgkYnVyZ2VyX21lbnUpLmxlbmd0aCkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBEZXNrdG9wIG1lbnVcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbigkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyICR3aW4gPSAkKHdpbmRvdyksXG4gICAgICAgICRoZWFkZXIgPSAkKCcjc3AtaGVhZGVyJyk7XG5cbiAgICAvLyBkcm9wZG93biBhdXRvcG9zaXRpb25cbiAgICAkd2luLm9uKCdkb2NyZWFkeSBsb2FkIHJlc2l6ZScsICQuZGVib3VuY2UoMjUwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICRoZWFkZXIuZmluZCgnLnN1Yi1tZW51JykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuICAgICAgICAgICAgaWYgKCR0aGlzLm9mZnNldCgpLmxlZnQgKyAkdGhpcy5vdXRlcldpZHRoKCkgPj0gKCR3aW4ub3V0ZXJXaWR0aCgpIC0gMjUpKSB7XG4gICAgICAgICAgICAgICAgJHRoaXMuYWRkQ2xhc3MoJ2ludmVydC1hdHRhY2gtcG9pbnQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSkpO1xuXG4gICAgLy8gc3RpY2t5IG1lbnUgKDE1MCBpcyBhIHNjcm9sbCBvZmZzZXQgaW4gcGl4ZWxzKVxuICAgIFBaVEpTLnNjcm9sbFJBRihmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPiAxNTAgJiYgISRoZWFkZXIuaGFzQ2xhc3MoJ2hlYWRlci1zdHVjaycpKSB7XG4gICAgICAgICAgICAkaGVhZGVyLmFkZENsYXNzKCdoZWFkZXItc3R1Y2snKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPD0gMTUwICYmICRoZWFkZXIuaGFzQ2xhc3MoJ2hlYWRlci1zdHVjaycpKSB7XG4gICAgICAgICAgICAkaGVhZGVyLnJlbW92ZUNsYXNzKCdoZWFkZXItc3R1Y2snKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy8gZGlzYWJsZSBqdW1wcyBmb3IgZW1wdHktYW5jaG9yIGxpbmtzXG4gICAgJGhlYWRlci5maW5kKCcubmF2X21lbnUgYVtocmVmPVwiI1wiXScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIEZ1bGxzY3JlZW4gc2VhcmNoXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24oJCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciAkdG9nZ2xlID0gJCgnI3NwLWhlYWRlcicpLmZpbmQoJy5zcC1zZWFyY2gtaWNvbicpLFxuICAgICAgICAkc2VhcmNoQ29udGFpbmVyID0gJCgnI3NwLXNlYXJjaC1ibG9jay1jb250YWluZXInKTtcblxuICAgIC8vIGZvY3VzIGlucHV0IHdoZW4gY29udGFpbmVyIGlzIHZpc2libGVcbiAgICAkc2VhcmNoQ29udGFpbmVyLmZpbmQoJz4gLnNlYXJjaC1ibG9jay1pbm5lcicpLm9uKFBaVEpTLnRyYW5zaXRpb25FbmQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpLmlzKCc6dmlzaWJsZScpICYmICQodGhpcykuZmluZCgnLnNlYXJjaC1pbnB1dCcpLmZvY3VzKCk7XG4gICAgfSk7XG5cbiAgICAvLyBjbG9zZSBvbiBjbGlja1xuICAgICRzZWFyY2hDb250YWluZXIuZmluZCgnLmNsb3NlLXNlYXJjaCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICRzZWFyY2hDb250YWluZXIucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcbiAgICB9KTtcblxuICAgIC8vIGNsb3NlIG9uIGVzYyBrZXl1cFxuICAgICQoZG9jdW1lbnQpLmtleXVwKGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgKGUua2V5Q29kZSA9PT0gMjcpICYmICRzZWFyY2hDb250YWluZXIucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcbiAgICB9KTtcblxuICAgIC8vIG9wZW4gdHJpZ2dlclxuICAgICR0b2dnbGUub24oJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgJHNlYXJjaENvbnRhaW5lci5hZGRDbGFzcygnb3BlbicpO1xuICAgIH0pO1xuXG59KShqUXVlcnkpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogUm9tYW4gTmF6YXJraW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogSW50cm8gaW5pdFxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyICRpbnRybyA9ICQoJy5zcC1pbnRybycpLFxuICAgICAgICAkd2luID0gJCh3aW5kb3cpO1xuXG4gICAgdmFyIGluaXRJbWFnZSA9IGZ1bmN0aW9uICgkaW50cm8pIHtcbiAgICAgICAgaWYgKCEkaW50cm8uYXR0cignZGF0YS1iYWNrZ3JvdW5kJykpIHsgcmV0dXJuOyB9XG4gICAgICAgICRpbnRyby5maW5kKCc+IC5pbnRyby1iZycpLmxlbmd0aCB8fCAkaW50cm8uYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaW50cm8tYmdcIi8+Jyk7XG4gICAgICAgICRpbnRyby5maW5kKCc+IC5pbnRyby1iZycpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsICd1cmwoJyArICRpbnRyby5hdHRyKCdkYXRhLWJhY2tncm91bmQnKSArICcpJyk7XG4gICAgfTtcblxuICAgIHZhciBpbml0Q2Fyb3VzZWwgPSBmdW5jdGlvbiAoJGludHJvKSB7XG4gICAgICAgICRpbnRyby5hZGRDbGFzcygnc2xpY2stZG90cy1pbnNpZGUnKTtcblxuICAgICAgICAkaW50cm8ub24oJ2luaXQgcmVJbml0JywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCh0aGlzKS5maW5kKCcuc3dpcGVib3gtdmlkZW8nKS5zd2lwZWJveCgpO1xuICAgICAgICB9KTtcblxuICAgICAgICB2YXIgc2xpY2tEZWZhdWx0T3B0aW9ucyA9IHtcbiAgICAgICAgICAgIHNsaWRlICAgICAgICA6ICcuc2xpZGVyLWl0ZW0nLFxuICAgICAgICAgICAgc3BlZWQgICAgICAgIDogMTAwMCxcbiAgICAgICAgICAgIGRvdHMgICAgICAgICA6IHRydWUsXG4gICAgICAgICAgICBmYWRlICAgICAgICAgOiB0cnVlLFxuICAgICAgICAgICAgYXV0b3BsYXkgICAgIDogdHJ1ZSxcbiAgICAgICAgICAgIGluZmluaXRlICAgICA6IHRydWUsXG4gICAgICAgICAgICBhdXRvcGxheVNwZWVkOiA3NTAwXG4gICAgICAgIH07XG5cbiAgICAgICAgJGludHJvLnNsaWNrKCQuZXh0ZW5kKHNsaWNrRGVmYXVsdE9wdGlvbnMsICRpbnRyby5kYXRhKCdzbGljaycpKSk7XG4gICAgfTtcblxuICAgIHZhciBpbml0VmlkZW8gPSBmdW5jdGlvbigkaW50cm8pIHtcbiAgICAgICAgdmFyICR2aWRlbyA9ICRpbnRyby5maW5kKCc+IC52aWRlby1jb250YWluZXInKSxcbiAgICAgICAgICAgICRwbGFjZWhvbGRlciA9ICR2aWRlby5maW5kKCc+IC52aWRlby1wbGFjZWhvbGRlcicpLFxuICAgICAgICAgICAgJGNvbnRyb2xzID0gJHZpZGVvLmZpbmQoJz4gLnZpZGVvLWNvbnRyb2xzJyk7XG5cbiAgICAgICAgJHZpZGVvLm9uKCdZVFBQbGF5IFlUUFBhdXNlJywgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgaWYoZS50eXBlID09PSAnWVRQUGxheScpIHsgJGNvbnRyb2xzLmZpbmQoJy5zcC12aWRlby1wbGF5ID4gaScpLnJlbW92ZUNsYXNzKCdpY29uLWlvbi1pb3MtcGxheScpOyB9XG4gICAgICAgICAgICBpZihlLnR5cGUgPT09ICdZVFBQYXVzZScpIHsgJGNvbnRyb2xzLmZpbmQoJy5zcC12aWRlby1wbGF5ID4gaScpLmFkZENsYXNzKCdpY29uLWlvbi1pb3MtcGxheScpOyB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICR2aWRlby5vbignWVRQTXV0ZWQgWVRQVW5tdXRlZCcsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIGlmKGUudHlwZSA9PT0gJ1lUUE11dGVkJykgeyAkY29udHJvbHMuZmluZCgnLnNwLXZpZGVvLXZvbHVtZSA+IGknKS5yZW1vdmVDbGFzcygnaWNvbi1pb24tYW5kcm9pZC12b2x1bWUtdXAnKTsgfVxuICAgICAgICAgICAgaWYoZS50eXBlID09PSAnWVRQVW5tdXRlZCcpIHsgJGNvbnRyb2xzLmZpbmQoJy5zcC12aWRlby12b2x1bWUgPiBpJykuYWRkQ2xhc3MoJ2ljb24taW9uLWFuZHJvaWQtdm9sdW1lLXVwJyk7IH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgJHZpZGVvLllUUGxheWVyKHtcbiAgICAgICAgICAgIHZpZGVvVVJMICAgIDogJHZpZGVvLmRhdGEoJ3VybCcpLFxuICAgICAgICAgICAgc2hvd0NvbnRyb2xzOiBmYWxzZSxcbiAgICAgICAgICAgIGNvbnRhaW5tZW50IDogJ3NlbGYnLFxuICAgICAgICAgICAgbG9vcCAgICAgICAgOiB0cnVlLFxuICAgICAgICAgICAgYXV0b1BsYXkgICAgOiB0cnVlLFxuICAgICAgICAgICAgdm9sICAgICAgICAgOiAyNSxcbiAgICAgICAgICAgIG11dGUgICAgICAgIDogdHJ1ZSxcbiAgICAgICAgICAgIHNob3dZVExvZ28gIDogZmFsc2UsXG4gICAgICAgICAgICBzdGFydEF0ICAgICA6ICR2aWRlby5kYXRhKCdzdGFydCcpIHx8IDAsXG4gICAgICAgICAgICBzdG9wQXQgICAgICA6ICR2aWRlby5kYXRhKCdzdG9wJykgfHwgMCxcbiAgICAgICAgICAgIG9uUmVhZHkgICAgIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICRwbGFjZWhvbGRlci5mYWRlT3V0KCdmYXN0Jyk7XG4gICAgICAgICAgICAgICAgJGNvbnRyb2xzLmZhZGVJbignZmFzdCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkY29udHJvbHMuZmluZCgnLnNwLXZpZGVvLXBsYXknKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJHZpZGVvLllUUFRvZ2dsZVBsYXkoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJGNvbnRyb2xzLmZpbmQoJy5zcC12aWRlby12b2x1bWUnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJHZpZGVvLllUUFRvZ2dsZVZvbHVtZSgpO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgLy8gaW5pdCBhbGwgaW50cm9zIG9uIHRoZSBwYWdlXG4gICAgJGludHJvLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuICAgICAgICBpZiAoJHRoaXMuaGFzQ2xhc3MoJ3NwLWludHJvLWNhcm91c2VsJykpIHsgaW5pdENhcm91c2VsKCR0aGlzKTsgfVxuICAgICAgICBpZiAoJHRoaXMuaGFzQ2xhc3MoJ3NwLWludHJvLXZpZGVvJykpIHsgaW5pdFZpZGVvKCR0aGlzKTsgfVxuICAgICAgICBpZiAoJHRoaXMuaGFzQ2xhc3MoJ3NwLWludHJvLWltYWdlJykpIHsgaW5pdEltYWdlKCR0aGlzKTsgfVxuICAgIH0pO1xuXG4gICAgLy8gYWRkIHNjcm9sbCBlZmZlY3RcbiAgICAoJHdpbi53aWR0aCgpID4gNjgwKSAmJiBQWlRKUy5zY3JvbGxSQUYoZnVuY3Rpb24gKCkge1xuICAgICAgICAkaW50cm8uZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJGN1cnJJbnRybyA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgaW50cm9IZWlnaHQgPSAkY3VyckludHJvLmhlaWdodCgpLFxuICAgICAgICAgICAgICAgIHBhZ2VZT2Zmc2V0ID0gTWF0aC5tYXgod2luZG93LnBhZ2VZT2Zmc2V0LCAwKTtcblxuICAgICAgICAgICAgaWYgKHdpbmRvdy5wYWdlWU9mZnNldCA+IGludHJvSGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgdHJhbnNsYXRlWSA9IE1hdGguZmxvb3IocGFnZVlPZmZzZXQgKiAwLjMpICsgJ3B4JztcbiAgICAgICAgICAgICRjdXJySW50cm9bMF0uc3R5bGVbTW9kZXJuaXpyLnByZWZpeGVkKCd0cmFuc2Zvcm0nKV0gPSAndHJhbnNsYXRlM2QoMCwgJyArIHRyYW5zbGF0ZVkgKyAnLCAwKSc7XG4gICAgICAgICAgICAkY3VyckludHJvLmNzcygnb3BhY2l0eScsICgxIC0gcGFnZVlPZmZzZXQgLyBpbnRyb0hlaWdodCkpO1xuICAgICAgICB9KTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IE5hemFya2luIFJvbWFuLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIE1haWxjaGltcCBzdWJzY3JpcHRpb24gYWpheCBmb3JtXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24oJCkge1xuXG4gICAgJC5hamF4Q2hpbXAudHJhbnNsYXRpb25zLnNvcHJhbm8gPSB7XG4gICAgICAgICdzdWJtaXQnOiAnTG9hZGluZywgcGxlYXNlIHdhaXQuLi4nLFxuICAgICAgICAwOiAnV2UgaGF2ZSBzZW50IHlvdSBhIGNvbmZpcm1hdGlvbiBlbWFpbC4gUGxlYXNlIGNoZWNrIHlvdXIgaW5ib3guJyxcbiAgICAgICAgMTogJ1RoZSBmaWVsZCBpcyBlbXB0eS4gUGxlYXNlIGVudGVyIHlvdXIgZW1haWwuJyxcbiAgICAgICAgMjogJ0FuIGVtYWlsIGFkZHJlc3MgbXVzdCBjb250YWluIGEgc2luZ2xlIFwiQFwiIGNoYXJhY3Rlci4nLFxuICAgICAgICAzOiAnVGhpcyBlbWFpbCBzZWVtcyB0byBiZSBpbnZhbGlkLiBQbGVhc2UgZW50ZXIgYSBjb3JyZWN0IG9uZS4nLFxuICAgICAgICA0OiAnVGhpcyBlbWFpbCBzZWVtcyB0byBiZSBpbnZhbGlkLiBQbGVhc2UgZW50ZXIgYSBjb3JyZWN0IG9uZS4nLFxuICAgICAgICA1OiAnVGhpcyBlbWFpbCBhZGRyZXNzIGxvb2tzIGZha2Ugb3IgaW52YWxpZC4gUGxlYXNlIGVudGVyIGEgcmVhbCBlbWFpbCBhZGRyZXNzLidcbiAgICB9O1xuXG4gICAgJCgnLnNwLXN1YnNjcmliZS1mb3JtJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciAkZm9ybSA9ICQodGhpcykuY2xvc2VzdCgnZm9ybScpO1xuXG4gICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkZm9ybS5hZGRDbGFzcygnbWMtbG9hZGluZycpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkZm9ybS5hamF4Q2hpbXAoe1xuICAgICAgICAgICAgbGFuZ3VhZ2U6ICdzb3ByYW5vJyxcbiAgICAgICAgICAgIGxhYmVsICAgOiAkZm9ybS5maW5kKCc+IC5mb3JtLW91dHB1dCcpLFxuICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uIChyZXNwKSB7XG4gICAgICAgICAgICAgICAgJGZvcm0ucmVtb3ZlQ2xhc3MoJ21jLWxvYWRpbmcnKTtcbiAgICAgICAgICAgICAgICAkZm9ybS50b2dnbGVDbGFzcygnbWMtdmFsaWQnLCAocmVzcC5yZXN1bHQgPT09ICdzdWNjZXNzJykpO1xuICAgICAgICAgICAgICAgICRmb3JtLnRvZ2dsZUNsYXNzKCdtYy1pbnZhbGlkJywgKHJlc3AucmVzdWx0ID09PSAnZXJyb3InKSk7XG5cbiAgICAgICAgICAgICAgICBpZiAocmVzcC5yZXN1bHQgPT09ICdzdWNjZXNzJykge1xuICAgICAgICAgICAgICAgICAgICAkZm9ybS5maW5kKCdpbnB1dFt0eXBlPVwiZW1haWxcIl0nKS52YWwoJycpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAkZm9ybS5yZW1vdmVDbGFzcygnbWMtdmFsaWQgbWMtaW52YWxpZCcpO1xuICAgICAgICAgICAgICAgICAgICAkZm9ybS5maW5kKCdpbnB1dFt0eXBlPVwiZW1haWxcIl0nKS5mb2N1cygpO1xuICAgICAgICAgICAgICAgIH0sIDQ1MDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IE5hemFya2luIFJvbWFuLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIE1ha2UgY2xpY2tzIG9uIG1vYmlsZSBkZXZpY2VzIHNtb290aGVyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICBpZih0eXBlb2YgRmFzdENsaWNrID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIEZhc3RDbGljay5hdHRhY2goZG9jdW1lbnQuYm9keSk7XG4gICAgfVxufSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBEaXNhYmxlIGhvdmVyIGVmZmVjdHMgd2hlbiBwYWdlIGlzIHNjcm9sbGVkXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCkge1xuICAgIHZhciBib2R5ID0gZG9jdW1lbnQuYm9keSxcbiAgICAgICAgdGltZXI7XG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgZnVuY3Rpb24gKCkge1xuICAgICAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgICAgICBpZiAoIWJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlLWhvdmVyJykpIHtcbiAgICAgICAgICAgIGJvZHkuY2xhc3NMaXN0LmFkZCgnZGlzYWJsZS1ob3ZlcicpXG4gICAgICAgIH1cblxuICAgICAgICB0aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdkaXNhYmxlLWhvdmVyJylcbiAgICAgICAgfSwgMTAwKTtcbiAgICB9LCBmYWxzZSk7XG59KSgpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogTmF6YXJraW4gUm9tYW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogU2Nyb2xsIHRvIGFuY2hvclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uKCQpIHtcblxuICAgIC8vIFNlbGVjdCBhbGwgbGlua3Mgd2l0aCBoYXNoZXNcbiAgICAkKCdhW2hyZWYqPVwiI1wiXScpXG4gICAgICAgIC8vIFJlbW92ZSBsaW5rcyB0aGF0IGRvbid0IGFjdHVhbGx5IGxpbmsgdG8gYW55dGhpbmdcbiAgICAgICAgLm5vdCgnW2hyZWY9XCIjXCJdJylcbiAgICAgICAgLm5vdCgnW2hyZWY9XCIjMFwiXScpXG4gICAgICAgIC5jbGljayhmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgLy8gT24tcGFnZSBsaW5rc1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgIGxvY2F0aW9uLnBhdGhuYW1lLnJlcGxhY2UoL15cXC8vLCAnJykgPT0gdGhpcy5wYXRobmFtZS5yZXBsYWNlKC9eXFwvLywgJycpXG4gICAgICAgICAgICAgICAgJiZcbiAgICAgICAgICAgICAgICBsb2NhdGlvbi5ob3N0bmFtZSA9PSB0aGlzLmhvc3RuYW1lXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICAvLyBGaWd1cmUgb3V0IGVsZW1lbnQgdG8gc2Nyb2xsIHRvXG4gICAgICAgICAgICAgICAgdmFyIHRhcmdldCA9ICQodGhpcy5oYXNoKTtcbiAgICAgICAgICAgICAgICB0YXJnZXQgPSB0YXJnZXQubGVuZ3RoID8gdGFyZ2V0IDogJCgnW25hbWU9JyArIHRoaXMuaGFzaC5zbGljZSgxKSArICddJyk7XG4gICAgICAgICAgICAgICAgLy8gRG9lcyBhIHNjcm9sbCB0YXJnZXQgZXhpc3Q/XG4gICAgICAgICAgICAgICAgaWYgKHRhcmdldC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gT25seSBwcmV2ZW50IGRlZmF1bHQgaWYgYW5pbWF0aW9uIGlzIGFjdHVhbGx5IGdvbm5hIGhhcHBlblxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IHRhcmdldC5vZmZzZXQoKS50b3AgLSA1NVxuICAgICAgICAgICAgICAgICAgICB9LCAxNTAwLCAnZWFzZUluT3V0RXhwbycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ2FsbGJhY2sgYWZ0ZXIgYW5pbWF0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBNdXN0IGNoYW5nZSBmb2N1cyFcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkdGFyZ2V0ID0gJCh0YXJnZXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHRhcmdldC5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCR0YXJnZXQuaXMoXCI6Zm9jdXNcIikpIHsgLy8gQ2hlY2tpbmcgaWYgdGhlIHRhcmdldCB3YXMgZm9jdXNlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRhcmdldC5hdHRyKCd0YWJpbmRleCcsJy0xJyk7IC8vIEFkZGluZyB0YWJpbmRleCBmb3IgZWxlbWVudHMgbm90IGZvY3VzYWJsZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0YXJnZXQuZm9jdXMoKTsgLy8gU2V0IGZvY3VzIGFnYWluXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBTY3JvbGwgdG8gVG9wIGJ1dHRvblxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uKCQpIHtcblxuICAgIHZhciBvZmZzZXQgPSA1MDAsXG4gICAgICAgICRiYWNrX3RvX3RvcCA9ICQoJy5zcC1zY3JvbGwtdG9wJyk7XG5cbiAgICBQWlRKUy5zY3JvbGxSQUYoZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPiBvZmZzZXQpIHtcbiAgICAgICAgICAgICRiYWNrX3RvX3RvcC5hZGRDbGFzcygnc2Nyb2xsLXRvcC12aXNpYmxlJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkYmFja190b190b3AucmVtb3ZlQ2xhc3MoJ3Njcm9sbC10b3AtdmlzaWJsZScpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAkYmFja190b190b3Aub24oJ21vdXNlb3ZlciBtb3VzZW91dCcsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpLmZpbmQoJy5hbm5vLXRleHQnKS5zdG9wKCkuYW5pbWF0ZSh7XG4gICAgICAgICAgICB3aWR0aDogJ3RvZ2dsZScsXG4gICAgICAgICAgICBwYWRkaW5nOiAndG9nZ2xlJ1xuICAgICAgICAgICAgLy8gZGlzcGxheTogJ2lubGluZSdcbiAgICAgICAgfSk7XG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBQb3J0Zm9saW8gc2h1ZmZsZVxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgLy8gbG9jYXRlIHdoYXQgd2Ugd2FudCB0byBzb3J0XG4gICAgdmFyICRibG9ja3MgPSAkKCcuc3AtcG9ydGZvbGlvLWJsb2NrJyk7XG5cbiAgICAvLyBkb24ndCBydW4gdGhpcyBmdW5jdGlvbiBpZiB0aGlzIHBhZ2UgZG9lcyBub3QgY29udGFpbiByZXF1aXJlZCBlbGVtZW50XG4gICAgaWYgKCRibG9ja3MubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIGluaXQgc2h1ZmZsZSBhbmQgZmlsdGVyc1xuICAgICRibG9ja3MuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICAkZ3JpZCA9ICR0aGlzLmZpbmQoJy5zcC1wb3J0Zm9saW8taXRlbXMnKSxcbiAgICAgICAgICAgICRmaWx0ZXJCdG5zID0gJHRoaXMuZmluZCgnLnNwLXBvcnRmb2xpby1zb3J0aW5nIGFbZGF0YS1ncm91cF0nKTtcblxuICAgICAgICAvLyBpbnN0YW50aWF0ZSB0aGUgcGx1Z2luXG4gICAgICAgICRncmlkLnB6dF9zaHVmZmxlKHtcbiAgICAgICAgICAgIGl0ZW1TZWxlY3RvcjogJ1tjbGFzcyo9XCJjb2wtXCJdJyxcbiAgICAgICAgICAgIGd1dHRlcldpZHRoIDogMCxcbiAgICAgICAgICAgIHNwZWVkICAgICAgIDogNjAwLCAvLyB0cmFuc2l0aW9uL2FuaW1hdGlvbiBzcGVlZCAobWlsbGlzZWNvbmRzKS5cbiAgICAgICAgICAgIGVhc2luZyAgICAgIDogJ2Vhc2UnXG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIGluaXQgZmlsdGVyc1xuICAgICAgICAkZmlsdGVyQnRucy5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICAgICAgICAgICAgLy8gaGlkZSBjdXJyZW50IGxhYmVsLCBzaG93IGN1cnJlbnQgbGFiZWwgaW4gdGl0bGVcbiAgICAgICAgICAgICR0aGlzLnBhcmVudCgpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgJHRoaXMucGFyZW50KCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gICAgICAgICAgICAvLyBmaWx0ZXIgZWxlbWVudHNcbiAgICAgICAgICAgICRncmlkLnNodWZmbGUoJ3NodWZmbGUnLCAkdGhpcy5kYXRhKCdncm91cCcpKTtcblxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBQcmVsb2FkZXJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbiAoJCkge1xuICAgIHZhciAkd2luZG93ID0gJCh3aW5kb3cpO1xuXG4gICAgJHdpbmRvdy5vbignbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJHdpbmRvdy50cmlnZ2VyKCdwenQucHJlbG9hZGVyX2RvbmUnKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3MoJ3NwLXBhZ2UtbG9hZGVkJyk7XG4gICAgICAgICAgICAkKCcjc3AtcHJlbG9hZGVyJykuZmFkZU91dCgnc2xvdycpO1xuICAgICAgICB9LCAyNTApO1xuICAgIH0pO1xufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IE5hemFya2luIFJvbWFuLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIFByb2dyZXNzIEJhclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICB2YXIgJGVsID0gJCgnLnNwLXByb2dyZXNzLWJhcicpLFxuICAgICAgICBkZWxheSA9IDA7XG5cbiAgICAkZWwuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICRwdiA9ICR0aGlzLmZpbmQoJy5wcm9ncmVzcy12YWx1ZScpLFxuICAgICAgICAgICAgJHBiID0gJHRoaXMuZmluZCgnLnByb2dyZXNzLWJhcicpO1xuXG4gICAgICAgICRwdi5odG1sKCcwJScpO1xuICAgICAgICAkcGIuY3NzKCd3aWR0aCcsIDApO1xuICAgIH0pO1xuXG4gICAgJGVsLm9uZSgnYXBwZWFyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICAgICAgJHB2ID0gJHRoaXMuZmluZCgnLnByb2dyZXNzLXZhbHVlJyksXG4gICAgICAgICAgICAkcGIgPSAkdGhpcy5maW5kKCcucHJvZ3Jlc3MtYmFyJyk7XG5cbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkcGIuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgd2lkdGg6ICR0aGlzLmRhdGEoJ3ZhbHVlJylcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogMjUwMCxcbiAgICAgICAgICAgICAgICBlYXNpbmcgIDogJ2Vhc2VJbk91dFF1aW50JyxcbiAgICAgICAgICAgICAgICBzdGVwICAgIDogZnVuY3Rpb24gKG5vdywgZngpIHtcbiAgICAgICAgICAgICAgICAgICAgJHB2Lmh0bWwobm93LnRvRml4ZWQoMCkgKyBmeC51bml0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSwgZGVsYXkpO1xuICAgICAgICBkZWxheSArPSAzMDA7XG4gICAgfSk7XG5cbiAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7IGRlbGF5ID0gMDsgfSwgMTAwMCk7XG5cbiAgICAkKHdpbmRvdykub25lKCdwenQucHJlbG9hZGVyX2RvbmUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICRlbC5hcHBlYXIoe2ZvcmNlX3Byb2Nlc3M6IHRydWV9KTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IE5hemFya2luIFJvbWFuLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24gKCQpIHtcbiAgICB2YXIgJGVsZW1zID0gJCgnLnNwLWFuaW1hdGUtbnVtYmVycyBoMicpO1xuXG4gICAgLy8gc2V0IHJvbGxlciBudW1iZXJzIGZpcnN0XG4gICAgJGVsZW1zLmVhY2goZnVuY3Rpb24gKGkpIHtcbiAgICAgICAgJCh0aGlzKS5hdHRyKCdkYXRhLXNsbm8nLCBpKTtcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygncm9sbGVyLXRpdGxlLW51bWJlci0nICsgaSk7XG4gICAgfSk7XG5cbiAgICAvLyBydW4gd2hlbiBhcHBlYXJcbiAgICAkZWxlbXMub25lKCdhcHBlYXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIG51bWJlclJvbGxlcigkKHRoaXMpLmF0dHIoJ2RhdGEtc2xubycpKTtcbiAgICB9KTtcblxuICAgICQod2luZG93KS5vbmUoJ3B6dC5wcmVsb2FkZXJfZG9uZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkZWxlbXMuYXBwZWFyKHtmb3JjZV9wcm9jZXNzOiB0cnVlfSk7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBudW1iZXJSb2xsZXIoc2xubykge1xuICAgICAgICB2YXIgbWluID0gJCgnLnJvbGxlci10aXRsZS1udW1iZXItJyArIHNsbm8pLmF0dHIoJ2RhdGEtbWluJyk7XG4gICAgICAgIHZhciBtYXggPSAkKCcucm9sbGVyLXRpdGxlLW51bWJlci0nICsgc2xubykuYXR0cignZGF0YS1tYXgnKTtcbiAgICAgICAgdmFyIHRpbWVkaWZmID0gJCgnLnJvbGxlci10aXRsZS1udW1iZXItJyArIHNsbm8pLmF0dHIoJ2RhdGEtZGVsYXknKTtcbiAgICAgICAgdmFyIGluY3JlbWVudCA9ICQoJy5yb2xsZXItdGl0bGUtbnVtYmVyLScgKyBzbG5vKS5hdHRyKCdkYXRhLWluY3JlbWVudCcpO1xuICAgICAgICB2YXIgbnVtZGlmZiA9IG1heCAtIG1pbjtcbiAgICAgICAgdmFyIHRpbWVvdXQgPSAodGltZWRpZmYgKiAxMDAwKSAvIG51bWRpZmY7XG4gICAgICAgIG51bWJlclJvbGwoc2xubywgbWluLCBtYXgsIGluY3JlbWVudCwgdGltZW91dCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbnVtYmVyUm9sbChzbG5vLCBtaW4sIG1heCwgaW5jcmVtZW50LCB0aW1lb3V0KSB7Ly9hbGVydChzbG5vK1wiPVwiK21pbitcIj1cIittYXgrXCI9XCIraW5jcmVtZW50K1wiPVwiK3RpbWVvdXQpO1xuICAgICAgICBpZiAobWluIDw9IG1heCkge1xuICAgICAgICAgICAgJCgnLnJvbGxlci10aXRsZS1udW1iZXItJyArIHNsbm8pLmh0bWwobWluKTtcbiAgICAgICAgICAgIG1pbiA9IHBhcnNlSW50KG1pbiwgMTApICsgcGFyc2VJbnQoaW5jcmVtZW50LCAxMCk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBudW1iZXJSb2xsKHBhcnNlSW50KHNsbm8sIDEwKSwgcGFyc2VJbnQobWluLCAxMCksIHBhcnNlSW50KG1heCwgMTApLCBwYXJzZUludChpbmNyZW1lbnQsIDEwKSwgcGFyc2VJbnQodGltZW91dCwgMTApKVxuICAgICAgICAgICAgfSwgdGltZW91dCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKCcucm9sbGVyLXRpdGxlLW51bWJlci0nICsgc2xubykuaHRtbChtYXgpO1xuICAgICAgICB9XG4gICAgfVxuXG59KShqUXVlcnkpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogTmF6YXJraW4gUm9tYW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogQ2xpZW50cyBTbGlkZXJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbiAoJCkge1xuXG4gICAgJCgnLnNwLXNsaWNrLWNsaWVudHMnKS5zbGljayh7XG4gICAgICAgIGRvdHMgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgaW5maW5pdGUgICAgICA6IHRydWUsXG4gICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxuICAgICAgICBzcGVlZCAgICAgICAgIDogNzUwLFxuICAgICAgICBzbGlkZXNUb1Nob3cgIDogNSxcbiAgICAgICAgYXV0b3BsYXkgICAgICA6IHRydWUsXG4gICAgICAgIGFycm93cyAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgcmVzcG9uc2l2ZSAgICA6IFt7XG4gICAgICAgICAgICBicmVha3BvaW50OiA5OTIsXG4gICAgICAgICAgICBzZXR0aW5ncyAgOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAzXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDc2OCxcbiAgICAgICAgICAgIHNldHRpbmdzICA6IHtcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAgYnJlYWtwb2ludDogNDgwLFxuICAgICAgICAgICAgc2V0dGluZ3MgIDoge1xuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMVxuICAgICAgICAgICAgfVxuICAgICAgICB9XVxuICAgIH0pO1xuXG59KShqUXVlcnkpO1xuXG5cblxuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICAkKCcuc3Atc2xpY2stZGVtbycpLnNsaWNrKHtcbiAgICAgICAgZG90cyAgICAgICAgICA6IGZhbHNlLFxuICAgICAgICBpbmZpbml0ZSAgICAgIDogdHJ1ZSxcbiAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IHRydWUsXG4gICAgICAgIHNwZWVkICAgICAgICAgOiA3NTAsXG4gICAgICAgIHNsaWRlc1RvU2hvdyAgOiAzLFxuICAgICAgICBhdXRvcGxheSAgICAgIDogdHJ1ZSxcbiAgICAgICAgYXJyb3dzICAgICAgICA6IGZhbHNlLFxuICAgICAgICByZXNwb25zaXZlICAgIDogW3tcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcbiAgICAgICAgICAgIHNldHRpbmdzICA6IHtcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDNcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICAgICAgc2V0dGluZ3MgIDoge1xuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMlxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBicmVha3BvaW50OiA0ODAsXG4gICAgICAgICAgICBzZXR0aW5ncyAgOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxXG4gICAgICAgICAgICB9XG4gICAgICAgIH1dXG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBUZXN0aW1vbmlhbHMgU2xpZGVyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCQpIHtcblxuICAgICQoJy5zcC1zbGljay10ZXN0aW1vbmlhbHMnKS5zbGljayh7XG4gICAgICAgIGRvdHMgICAgICAgICAgOiB0cnVlLFxuICAgICAgICBpbmZpbml0ZSAgICAgIDogdHJ1ZSxcbiAgICAgICAgc3BlZWQgICAgICAgICA6IDc1MCxcbiAgICAgICAgc2xpZGVzVG9TaG93ICA6IDEsXG4gICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxuICAgICAgICBhdXRvcGxheSAgICAgIDogdHJ1ZSxcbiAgICAgICAgYXJyb3dzICAgICAgICA6IGZhbHNlLFxuICAgICAgICBhdXRvcGxheVNwZWVkIDogNjUwMFxuICAgIH0pO1xuXG59KShqUXVlcnkpO1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogXCJpTWFjXCIgc2xpZGVyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24oJCkge1xuXG4gICAgJCgnLnNwLXNsaWNrLWltYWMnKS5zbGljayh7XG4gICAgICAgIGRvdHMgICAgICAgICAgOiB0cnVlLFxuICAgICAgICBpbmZpbml0ZSAgICAgIDogdHJ1ZSxcbiAgICAgICAgc3BlZWQgICAgICAgICA6IDQ1MCxcbiAgICAgICAgc2xpZGVzVG9TaG93ICA6IDEsXG4gICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxuICAgICAgICBhdXRvcGxheSAgICAgIDogdHJ1ZSxcbiAgICAgICAgYXJyb3dzICAgICAgICA6IGZhbHNlLFxuICAgICAgICBmYWRlICAgICAgICAgIDogdHJ1ZSxcbiAgICAgICAgZWFzaW5nICAgICAgICA6ICdsaW5lYXInXG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBTaW5nbGUgUG9zdCBHYWxsZXJ5XG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCQpIHtcblxuICAgICQoJy5zcC1zbGljay1wb3N0LWdhbGxlcnknKS5zbGljayh7XG4gICAgICAgIGRvdHMgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgaW5maW5pdGUgICAgICA6IHRydWUsXG4gICAgICAgIHNwZWVkICAgICAgICAgOiA3NTAsXG4gICAgICAgIHNsaWRlc1RvU2hvdyAgOiAxLFxuICAgICAgICBhZGFwdGl2ZUhlaWdodDogdHJ1ZSxcbiAgICAgICAgYXV0b3BsYXkgICAgICA6IHRydWUsXG4gICAgICAgIGF1dG9wbGF5U3BlZWQgOiA1MDAwLFxuICAgICAgICBuZXh0QXJyb3cgICAgIDogJzxkaXYgY2xhc3M9XCJzbGljay1uZXh0IGNpcmNsZVwiPjxpIGNsYXNzPVwiaWNvbi1hbmdsZS1yaWdodFwiPjwvaT48L2Rpdj4nLFxuICAgICAgICBwcmV2QXJyb3cgICAgIDogJzxkaXYgY2xhc3M9XCJzbGljay1wcmV2IGNpcmNsZVwiPjxpIGNsYXNzPVwiaWNvbi1hbmdsZS1sZWZ0XCI+PC9pPjwvZGl2PicsXG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogQWRkIHNsaWNrLWFuaW1hdGVkIGNsYXNzIHdoZW4gdHJhbnNpdGlvbiBpcyBvdmVyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICB2YXIgJHNsaWRlcnMgPSAkKCcuc2xpY2staW5pdGlhbGl6ZWQnKTtcblxuICAgICRzbGlkZXJzLm9uKCdpbml0aWFsQW5pbWF0ZSByZUluaXQgYWZ0ZXJDaGFuZ2UnLCBmdW5jdGlvbiAoZSwgc2xpY2ssIGN1cnJlbnRTbGlkZSkge1xuICAgICAgICBjdXJyZW50U2xpZGUgPSBjdXJyZW50U2xpZGUgfHwgMDtcbiAgICAgICAgc2xpY2sgPSBzbGljayB8fCAkKHRoaXMpLnNsaWNrKCdnZXRTbGljaycpO1xuXG4gICAgICAgICQoc2xpY2suJHNsaWRlcykucmVtb3ZlQ2xhc3MoJ3NsaWNrLWFuaW1hdGVkJyk7XG4gICAgICAgICQoc2xpY2suJHNsaWRlc1tjdXJyZW50U2xpZGVdKS5hZGRDbGFzcygnc2xpY2stYW5pbWF0ZWQnKTtcbiAgICB9KTtcblxuICAgICRzbGlkZXJzLnRyaWdnZXIoJ2luaXRpYWxBbmltYXRlJyk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogU3RvcCBzbGlkZXJzIHRoYXQgaXMgbm90IGN1cnJlbnRseSBpbiB2aWV3cG9ydFxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbiAoJCkge1xuXG4gICAgdmFyICRzbGlkZXJzID0gJCgnLnNsaWNrLWluaXRpYWxpemVkJyk7XG5cbiAgICBQWlRKUy5zY3JvbGxSQUYoJC50aHJvdHRsZSgyNTAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJHNsaWRlcnMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICRzbGljayA9ICR0aGlzLnNsaWNrKCdnZXRTbGljaycpO1xuXG4gICAgICAgICAgICAvLyBzdG9wIHNsaWRlclxuICAgICAgICAgICAgaWYgKCFQWlRKUy5pc0VsZW1lbnRJblZpZXdwb3J0KHRoaXMpICYmICEkc2xpY2sucGF1c2VkKSB7XG4gICAgICAgICAgICAgICAgJHRoaXMuc2xpY2soJ3BhdXNlJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHVucGF1c2Ugc2xpZGVyXG4gICAgICAgICAgICBpZiAoUFpUSlMuaXNFbGVtZW50SW5WaWV3cG9ydCh0aGlzKSAmJiAkc2xpY2sucGF1c2VkKSB7XG4gICAgICAgICAgICAgICAgJHRoaXMuc2xpY2soJ3BsYXknKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSkpO1xuXG59KShqUXVlcnkpO1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIEludGVncmF0ZSBXT1cuanMgd2l0aCBzbGlja1xuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbigkKSB7XG5cbiAgICAkKCcuc2xpY2staW5pdGlhbGl6ZWQnKS5vbignYmVmb3JlQ2hhbmdlJywgZnVuY3Rpb24oZSwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKSB7XG4gICAgICAgICQoc2xpY2suJHNsaWRlc1tuZXh0U2xpZGVdKS5maW5kKCcud293LCAucmUtYW5pbWF0ZScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGVsID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICBuZXdvbmUgPSBlbC5jbG9uZSh0cnVlKTtcblxuICAgICAgICAgICAgZWwuYmVmb3JlKG5ld29uZSk7XG4gICAgICAgICAgICBlbC5yZW1vdmUoKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBTdGlja3kgc2lkZWJhciBmZWF0dXJlXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4oZnVuY3Rpb24gKCQpIHtcblxuICAgICQoJy5zcC1zaWRlYmFyLnN0aWNreScpLnN0aWNrX2luX3BhcmVudCh7XG4gICAgICAgIG9mZnNldF90b3A6ICQoJyNzcC1oZWFkZXInKS5vdXRlckhlaWdodCgpICsgMzBcbiAgICB9KTtcblxufSkoalF1ZXJ5KTsiLCIvKiEgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAqICBBdXRob3I6IFJvbWFuIE5hemFya2luLCBFZ29yIERhbmtvdlxuICogIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiAgUHV6emxlVGhlbWVzXG4gKiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIEludGVncmF0aW9uIHdpdGggdGV4dCByb3RhdG9yIGpRdWVyeSBwbHVnaW5cbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbihmdW5jdGlvbiAoJCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgICQoJy5zcC10ZXh0LXJvdGF0ZScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXG4gICAgICAgICR0aGlzLnRleHRyb3RhdG9yKHtcbiAgICAgICAgICAgIGFuaW1hdGlvbjogJHRoaXMuZGF0YSgnYW5pbWF0aW9uJyksXG4gICAgICAgICAgICBzcGVlZCAgICA6ICR0aGlzLmRhdGEoJ3NwZWVkJyksXG4gICAgICAgICAgICBzZXBhcmF0b3I6ICd8J1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7IiwiLyohID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgQXV0aG9yOiBOYXphcmtpbiBSb21hbiwgRWdvciBEYW5rb3ZcbiAqICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogIFB1enpsZVRoZW1lc1xuICogID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gKiBUeXBlZC5qcyBpbnRlZ3JhdGlvblxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xualF1ZXJ5KHdpbmRvdykub25lKCdwenQucHJlbG9hZGVyX2RvbmUnLCBmdW5jdGlvbiAoKSB7XG4gICAgdmFyICQgPSBqUXVlcnk7XG5cbiAgICAvLyBwcm9jZXNzIGVhY2ggdHlwZWQtZW5hYmxlZCBlbGVtZW50XG4gICAgJCgnW2RhdGEtdHlwZWQtc3RyXScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICAgICAgdGV4dHMgPSAkdGhpcy5hdHRyKCdkYXRhLXR5cGVkLXN0cicpLnNwbGl0KCd8Jyk7XG5cbiAgICAgICAgJHRoaXMuaHRtbCgnJykuYXBwZW5kKCc8c3BhbiBjbGFzcz1cInR5cGVkLWNvbnRhaW5lclwiPjwvc3Bhbj4nKTtcbiAgICAgICAgJHRoaXMuZmluZCgnPiAudHlwZWQtY29udGFpbmVyJykudHlwZWQoe1xuICAgICAgICAgICAgc3RyaW5ncyAgIDogdGV4dHMsXG4gICAgICAgICAgICB0eXBlU3BlZWQgOiA2NSxcbiAgICAgICAgICAgIGxvb3AgICAgICA6ICgkdGhpcy5hdHRyKCdkYXRhLXR5cGVkLXJlcGVhdCcpID09PSAneWVzJyksXG4gICAgICAgICAgICBiYWNrRGVsYXkgOiAxNTAwLFxuICAgICAgICAgICAgc2hvd0N1cnNvcjogKCR0aGlzLmF0dHIoJ2RhdGEtdHlwZWQtY3Vyc29yJykgPT09ICd5ZXMnKVxuICAgICAgICB9KTtcbiAgICB9KTtcbn0pOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogTmF6YXJraW4gUm9tYW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAqIExvYWQgdGhlbWUgZm9udHMgdmlhIFdlYkZvbnRMb2FkZXJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbid1c2Ugc3RyaWN0JztcbihmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHR5cGVvZiBTT1BSQU5PX0ZPTlRTID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgV2ViRm9udC5sb2FkKHtcbiAgICAgICAgZ29vZ2xlOiB7ZmFtaWxpZXM6IFNPUFJBTk9fRk9OVFN9XG4gICAgfSk7XG59KSgpOyIsIi8qISA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICogIEF1dGhvcjogUm9tYW4gTmF6YXJraW4sIEVnb3IgRGFua292XG4gKiAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqICBQdXp6bGVUaGVtZXNcbiAqICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICogV09XLmpzIGFwcGVhcmFuY2UgYW5pbWF0aW9uIGVuZ2luZVxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIGFuaW1hdGlvbkR1cmF0aW9uID0gTW9kZXJuaXpyLnByZWZpeGVkKCdhbmltYXRpb25EdXJhdGlvbicpO1xuXG4gICAgLy8gcXVpdCBmcm9tIGZ1bmN0aW9uIG9uIG1vYmlsZSBkZXZpY2VzXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICBpZiAoUFpUSlMuaXNNb2JpbGUoKSkge1xuICAgICAgICAkKCcud293LnNlcXVlbmNlZCcpLnJlbW92ZUNsYXNzKCd3b3cgc2VxdWVuY2VkJyk7XG4gICAgICAgICQoJy53b3cnKS5yZW1vdmVDbGFzcygnd293Jyk7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cblxuICAgIC8vIHNlcXVlbmNlZCBhbmltYXRpb25zXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICB2YXIgc2VxRWxlbWVudHMgPSAkKCcud293LnNlcXVlbmNlZCcpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICBjbGFzc25hbWUgPSAkdGhpcy5hdHRyKCdjbGFzcycpLFxuICAgICAgICAgICAgYW5pbUR1cmF0aW9uID0gJHRoaXMuZGF0YSgnd293LWR1cmF0aW9uJykgfHwgJzFzJyxcbiAgICAgICAgICAgIGNoaWxkcmVuU2VsZWN0b3IgPSAkdGhpcy5kYXRhKCd3b3ctY2hpbGRyZW4nKSxcbiAgICAgICAgICAgIGZ4bmFtZSA9IC9meC0oW2EtekEtWl0rKS9nLmV4ZWMoY2xhc3NuYW1lKTtcblxuICAgICAgICAvLyByZW1vdmUgY2xhc3NuYW1lc1xuICAgICAgICAkdGhpcy5yZW1vdmVDbGFzcygnd293IHNlcXVlbmNlZCBmeC0nICsgZnhuYW1lWzFdKTtcblxuICAgICAgICAvLyBzZWxlY3QgY2hpbGRyZW4gZWxlbWVudHNcbiAgICAgICAgdmFyICRjaGlsZHJlbiA9ICR0aGlzLmZpbmQoJz4gKicpO1xuICAgICAgICBpZiAoIWNoaWxkcmVuU2VsZWN0b3IpIHtcbiAgICAgICAgICAgIGlmICgkdGhpcy5oYXNDbGFzcygnd3BiX2NvbHVtbicpKSB7IC8vIHx8ICR0aGlzLmhhc0NsYXNzKCd3cGJfcm93JylcbiAgICAgICAgICAgICAgICAkY2hpbGRyZW4gPSAkdGhpcy5maW5kKCcud3BiX3dyYXBwZXIgPiAqJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkY2hpbGRyZW4gPSAkdGhpcy5maW5kKGNoaWxkcmVuU2VsZWN0b3IpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gaGlkZSBhbGwgbm9uLWFuaW1hdGVkIGNoaWxkcmVuXG4gICAgICAgICRjaGlsZHJlbi5jc3MoJ3Zpc2liaWxpdHknLCAnaGlkZGVuJyk7XG5cbiAgICAgICAgLy8gc2V0IHByb3BlciBhbmltYXRpb24gc3BlZWRcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAkY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICRjaGlsZHJlbi5nZXQoaSkuc3R5bGVbYW5pbWF0aW9uRHVyYXRpb25dID0gYW5pbUR1cmF0aW9uO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gYmluZCBhbmltYXRpb24gZW5kIGV2ZW50XG4gICAgICAgICRjaGlsZHJlbi5vbmUoUFpUSlMuYW5pbWF0aW9uRW5kLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2FuaW1hdGVkICcgKyBmeG5hbWVbMV0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBzYXZlIGRhdGEgZm9yIGZ1cnRoZXIgZXhlY3V0aW9uXG4gICAgICAgICR0aGlzLmRhdGEoe1xuICAgICAgICAgICAgd293X2NoaWxkcmVuOiAkY2hpbGRyZW4sXG4gICAgICAgICAgICB3b3dfZnggICAgICA6IGZ4bmFtZVsxXVxuICAgICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHN0YXJ0IGFuaW1hdGlvbnMgd2hlbiBlbGVtZW50IGFwcGVhcnMgaW4gdmlld3BvcnRcbiAgICBzZXFFbGVtZW50cy5vbmUoJ2FwcGVhcicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSwgcm93U3RhcnQgPSBudWxsO1xuXG4gICAgICAgIC8vIGdldCBmeCBuYW1lXG4gICAgICAgIHZhciBmeG5hbWUgPSAkdGhpcy5kYXRhKCd3b3dfZngnKSxcbiAgICAgICAgICAgICRjaGlsZHJlbiA9ICR0aGlzLmRhdGEoJ3dvd19jaGlsZHJlbicpLFxuICAgICAgICAgICAgZWxfaW5kZXggPSAwLCByb3dfaWQgPSAwO1xuXG4gICAgICAgIC8vIHJ1biBhbmltYXRpb24gc2VxdWVuY2VcbiAgICAgICAgJGNoaWxkcmVuLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcyksIGN1cnJUb3BQb3NpdGlvbiA9ICRlbC5wb3NpdGlvbigpLnRvcDtcblxuICAgICAgICAgICAgLy8gY2hlY2sgZm9yIGEgbmV3IHJvd1xuICAgICAgICAgICAgaWYgKGN1cnJUb3BQb3NpdGlvbiAhPT0gcm93U3RhcnQpIHtcbiAgICAgICAgICAgICAgICBlbF9pbmRleCA9IDA7XG4gICAgICAgICAgICAgICAgcm93U3RhcnQgPSBjdXJyVG9wUG9zaXRpb247XG4gICAgICAgICAgICAgICAgcm93X2lkKys7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHJ1biBhbmltYXRpb24gYWZ0ZXIgc29tZSBkZWxheVxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkZWwuYWRkQ2xhc3MoJ2FuaW1hdGVkICcgKyBmeG5hbWUpO1xuICAgICAgICAgICAgICAgICRlbC5jc3MoJ3Zpc2liaWxpdHknLCAndmlzaWJsZScpO1xuICAgICAgICAgICAgfSwgKGVsX2luZGV4ICogMzAwKSArIChyb3dfaWQgKiAxNTApKTtcblxuICAgICAgICAgICAgZWxfaW5kZXgrKztcbiAgICAgICAgfSk7XG4gICAgfSk7XG5cblxuICAgIC8vIHJlZ3VsYXIgd293IGVuZ2luZVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgdmFyIHJlZ1dPVyA9IG5ldyBXT1coe1xuICAgICAgICBib3hDbGFzcyAgICAgICA6ICd3b3cnLCAgICAgIC8vIGFuaW1hdGVkIGVsZW1lbnQgY3NzIGNsYXNzIChkZWZhdWx0IGlzIHdvdylcbiAgICAgICAgYW5pbWF0ZUNsYXNzICAgOiAnYW5pbWF0ZWQnLCAvLyBhbmltYXRpb24gY3NzIGNsYXNzIChkZWZhdWx0IGlzIGFuaW1hdGVkKVxuICAgICAgICBvZmZzZXQgICAgICAgICA6IDAsICAgICAgICAgIC8vIGRpc3RhbmNlIHRvIHRoZSBlbGVtZW50IHdoZW4gdHJpZ2dlcmluZyB0aGUgYW5pbWF0aW9uIChkZWZhdWx0IGlzIDApXG4gICAgICAgIG1vYmlsZSAgICAgICAgIDogZmFsc2UsICAgICAgLy8gdHJpZ2dlciBhbmltYXRpb25zIG9uIG1vYmlsZSBkZXZpY2VzIChkZWZhdWx0IGlzIHRydWUpXG4gICAgICAgIGxpdmUgICAgICAgICAgIDogdHJ1ZSwgICAgICAgLy8gYWN0IG9uIGFzeW5jaHJvbm91c2x5IGxvYWRlZCBjb250ZW50IChkZWZhdWx0IGlzIHRydWUpXG4gICAgICAgIHNjcm9sbENvbnRhaW5lcjogbnVsbCwgICAgICAgLy8gb3B0aW9uYWwgc2Nyb2xsIGNvbnRhaW5lciBzZWxlY3Rvciwgb3RoZXJ3aXNlIHVzZSB3aW5kb3dcbiAgICAgICAgY2FsbGJhY2sgICAgICAgOiBmdW5jdGlvbiAoYm94KSB7XG4gICAgICAgICAgICAvLyB0aGUgY2FsbGJhY2sgaXMgZmlyZWQgZXZlcnkgdGltZSBhbiBhbmltYXRpb24gaXMgc3RhcnRlZFxuICAgICAgICAgICAgLy8gdGhlIGFyZ3VtZW50IHRoYXQgaXMgcGFzc2VkIGluIGlzIHRoZSBET00gbm9kZSBiZWluZyBhbmltYXRlZFxuICAgICAgICB9XG4gICAgfSk7XG5cblxuICAgIC8vIHJ1biBib3RoIGVuZ2luZXMgb25jZSBwcmVsb2FkaW5nIGRvbmVcbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgICQod2luZG93KS5vbmUoJ3B6dC5wcmVsb2FkZXJfZG9uZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICBzZXFFbGVtZW50cy5zZWxlY3RvciA9IGZhbHNlO1xuICAgICAgICBzZXFFbGVtZW50cy5hcHBlYXIoe2ZvcmNlX3Byb2Nlc3M6IHRydWV9KTtcblxuICAgICAgICByZWdXT1cuaW5pdCgpO1xuICAgIH0pO1xuXG59KShqUXVlcnkpOyJdfQ==
