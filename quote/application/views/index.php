<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="QUOTE - Request a quote for every type of companies">
    <meta name="author" content="Ansonika">
    <title>Oengines - Request a quote for every type of companies</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url();?>img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url();?>img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url();?>img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url();?>img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url();?>layerslider/css/layerslider.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/bootstrap.min.css?v=1" rel="stylesheet">
    <link href="<?php echo base_url();?>css/style.css?v=1" rel="stylesheet">
    <link href="<?php echo base_url();?>css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/magnific-popup.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/skins/square/yellow.css" rel="stylesheet">
    <link href="<?php echo base_url();?>js/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <!-- <link href="<?php echo base_url();?>css/custom.css?v=2" rel="stylesheet"> -->

</head>

<body <?php echo isset($id) ? 'onload="myFunction()"' : '';?> >
    
    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div><!-- /Loader_form -->

    <header>
        <div id="logo_home">
            <h1><a href="index.html" title="Quote">Quote - Request a quote for every type of companies</a></h1>
        </div>
        
        <a id="menu-button-mobile" class="cmn-toggle-switch cmn-toggle-switch__htx" href="javascript:void(0);"><span>Menu mobile</span></a>
        <nav class="main_nav">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab">Request a quote</a></li>
                <li><a href="#tab_2" data-toggle="tab">About</a></li>
                <li><a href="#tab_3" data-toggle="tab">Contact</a></li>
            </ul>
        </nav>
    </header><!-- /header -->

    <div id="layerslider" class="fullsize" style="width:1200px;height:100vh;">
    <!-- First slide -->
        <div class="ls-slide"  data-ls="duration: 8000; transition2d: 5;bgsize:cover;bgposition:50% 50%;kenburnszoom:in;">
                <img src="<?php echo base_url();?>img/slides/slide_1.jpg" class="ls-bg" alt="Slide background">
                <p style="font-size:72px; color:#fff;top:46%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:800;offsetxout:-100;durationout:1000;"><strong>QUOTATIONS</strong></p>
                  <p style="font-size:52px; color:#fff;top:55%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:1500;offsetxout:-100;durationout:1000;">for every type of companies</p>
        </div>
     
        <!-- Second slide -->
        <div class="ls-slide"  data-ls="duration: 8000; transition2d: 5;bgsize:cover;bgposition:50% 50%;kenburnszoom:in;">
                <img src="<?php echo base_url();?>img/slides/slide_2.jpg" class="ls-bg" alt="Slide background">
                 <p style="font-size:72px; color:#fff;top:46%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:800;offsetxout:-100;durationout:1000;"><strong>HOOK</strong></p>
                  <p style="font-size:52px; color:#fff;top:55%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:1500;offsetxout:-100;durationout:1000;">new customers and contacts</p>
        </div>
        
        <!-- Third slide -->
        <div class="ls-slide"  data-ls="duration: 8000; transition2d: 5;bgsize:cover;bgposition:50% 50%;kenburnszoom:in;">
                <img src="<?php echo base_url();?>img/slides/slide_3.jpg" class="ls-bg" alt="Slide background">
                   <p style="font-size:72px; color:#fff;top:46%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:800;offsetxout:-100;durationout:1000;"><strong>GET RELATIONSHIP</strong></p>
                  <p style="font-size:52px; color:#fff;top:55%; left:0px;line-height:1;text-shadow: 2px 2px 10px rgba(0, 0, 0, 0.60); padding-left:50px; letter-spacing:-1px;" class="ls-l sliderleft" data-ls="offsetxin:-100;durationin:2000;delayin:1500;offsetxout:-100;durationout:1000;">share youe thinks</p>
        </div>
        
    </div><!-- /layerslider -->


    <div class="layer"></div><!-- /mask -->

    <div id="main_container">
       
        <div id="header_in">
            <a href="#0" class="close_in"><i class="pe-7s-close-circle"></i></a>
        </div>

        <div class="wrapper_in">
            <div class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane fade" id="tab_1">                                               
                        <div class="subheader" id="quote"></div>
                        <div class="row">
                            <aside class="col-xl-3 col-lg-4">
                                <h2>Request a Quote and Compare prices!</h2>
                                <p class="lead">An mei sadipscing dissentiet, eos ea partem viderer facilisi.</p>
                                <ul class="list_ok">
                                    <li>Delicata persecuti ei nec, et his minim omnium, aperiam placerat ea vis.</li>
                                    <li>Suavitate vituperatoribus pro ad, cum in quis propriae abhorreant.</li>
                                    <li>Aperiri deterruisset ei mea, sed cu laudem intellegat, eu mutat iuvaret voluptatum mei.</li>
                                </ul>
                            </aside><!-- /aside -->

                            <div class="col-xl-9 col-lg-8">
                                <div id="wizard_container">
                                    <div id="top-wizard">
                                        <strong>Progress</strong>
                                        <div id="progressbar"></div>
                                    </div><!-- /top-wizard -->

                                    <form action="<?php echo base_url('home/index') ;?>" method="POST" enctype="multipart/form-data">
                                        <input id="website" name="website" type="text" value=""><!-- Leave for security protection, read docs for details -->
                                        <div id="middle-wizard">

                                            <div class="step">
                                                <h3 class="main_question"><strong>1/4</strong>What your poject need?</h3>

                                                <div class="row add_bottom_30 ">
                                                   
                                                   <div class="col-sm-6">
                                                <?php foreach ($needs as $key1 => $value1) {
                                                    if($key1 % 2 == 0){
                                                ?>   

                                                        <div class="form-group checkbox_questions total_need" id="<?php echo $value1['id'];?>">
                                                            <label>
                                                                <input name="needs[]" type="checkbox" value="<?php echo $value1['need'];?>" class="icheck required"><?php echo $value1['need'];?>
                                                            </label>
                                                        </div>                                                  
                                                    
                                                <?php } } ?> 
                                                    </div>

                                                    <div class="col-sm-6">
                                                <?php foreach ($needs as $key2 => $value2) { 
                                                    if($key2 % 2 != 0){
                                                ?>  

                                                        <div class="form-group checkbox_questions total_need" id="<?php echo $value2['id'];?>">
                                                            <label>
                                                                <input name="needs[]" type="checkbox" value="<?php echo $value2['need'];?>" class="icheck required"><?php echo $value2['need'];?>
                                                            </label>
                                                        </div> 

                                                <?php } } ?> 
                                                    </div>

                                                </div><!-- /row-->
                                                <div class="form-group textarea_info">
                                                    <label>Additional info</label>
                                                    <textarea name="additional_text" class="form-control" style="height:150px;" placeholder="How many pages, other details, etc..."></textarea>
                                                </div>
                                            </div>

                                            <div class="step">
                                                <h3 class="main_question"><strong>2/4</strong>What is your budget?</h3>
                                                    <?php 

                                                        foreach ($needs as $key2 => $value2) {
                                                        $budget_arr = explode("=", $value2['budget_limit']);
                                                        $budget_arr = explode("-", $budget_arr[0]);
                                                        $first = reset($budget_arr);
                                                        $last = end($budget_arr);

                                                        foreach ($budget_arr as $key8 => $value8) {
                                                        $prew = $budget_arr[$key8-1];
                                                    ?>
                                                    <?php if($first == $value8){ ?>
                                                    <div class="form-group radio_questions bug_<?php echo $value2['id'];?>">
                                                        <label> 
                                                                My budget is under $<?php echo $first ;?>
                                                            <input name="budgets" type="radio" value="My budget is under $<?php echo $first ;?>" class="icheck required">
                                                        </label>
                                                    </div>
                                                    <?php } ?>

                                                    <?php if($first != $value8 && $last != $value8){ ?>
                                                    <div class="form-group radio_questions bug_<?php echo $value2['id'];?>">
                                                        <label> 
                                                                My budget is between $<?php echo $prew ;?> and $<?php echo $value8 ;?>
                                                            <input name="budgets" type="radio" value="My budget is between $<?php echo $prew ;?> and $<?php echo $value8 ;?>" class="icheck required">
                                                        </label>
                                                    </div>  
                                                    <?php } ?>
                                                        
                                                    <?php if($last == $value8){ 
                                                        $demo = [1,2];
                                                        foreach ($demo as $k => $v) {
                                                    ?>
                                                    <div class="form-group radio_questions bug_<?php echo $value2['id'];?>">
                                                        <label> 
                                                                <?php if($v==1){ ?>
                                                                    My budget is between $<?php echo $prew ;?> and $<?php echo $last ;?>
                                                                <?php }else{ ?>
                                                                    My budget is over $<?php echo $last ;?>
                                                                <?php } ?>    
                                                            <input name="budgets" type="radio" value="<?php echo ($v==1) ? 'My budget is between $'.$prew.' and $'.$last : 'My budget is over $'.$last ;?>" class="icheck required">
                                                        </label>
                                                    </div>
                                                    <?php } } ?>

                                                    <?php } }?>
                                            </div>

                                            <div class="step">
                                                <h3 class="main_question"><strong>3/4</strong>Please answer the following questions:</h3>

                                                <div class="row">
                                                    <div class="col-lg-10">
                                                    <?php 
                                                        foreach ($qna as $key5 => $value5) { 
                                                        $plus_key = $key5 + 1;
                                                    ?>

                                                        <div class="form-group select qna_<?php echo $value5['need_id'];?>">
                                                            <label><?php echo $value5['question'];?></label>
                                                            <input type="hidden" name="<?php echo 'q'.$plus_key ?>" value="<?php echo $value5['question'];?>">
                                                            <div class="styled-select">
                                                                <select class="required" name="<?php echo 'question'.$plus_key ?>">
                                                                    <option value="" selected>Select</option>
                                                                <?php 
                                                                    $ans = explode(", ", $value5['answer']);
                                                                    foreach ($ans as $key6 => $value6) {
                                                                ?>        
                                                                    <option value="<?php echo $value6 ?>">
                                                                    <?php echo $value6 ?>
                                                                    </option>
                                                                <?php } ?>    
                                                                </select>
                                                            </div>
                                                        </div>

                                                    <?php } ?>            
                                                    </div>
                                                </div><!-- /row-->
                                            </div><!-- /step 3-->

                                            <div class="submit step">

                                                <h3 class="main_question"><strong>4/4</strong>Please fill with your details</h3>

                                                <div class="row">
                                                   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text" name="firstname" class="required form-control" placeholder="First name">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="email" name="email" class="required form-control" placeholder="Your Email">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="styled-select">
                                                                <select class="required" name="country">
                                                                    <option value="" selected>Select your country</option>
                                                                    <option value="Europe">Europe</option>
                                                                    <option value="Asia">Asia</option>
                                                                    <option value="North America">North America</option>
                                                                    <option value="South America">South America</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div><!-- /col-sm-6 -->

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text" name="lastname" class="required form-control" placeholder="Last name">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="mobileno" class="required form-control" placeholder="Your Telephone">
                                                        </div>
                                                        
                                                    </div><!-- /col-sm-6 -->
                                                </div><!-- /row -->
                                                
                                                <div class="form-group checkbox_questions">
                                                    <input name="terms" type="checkbox" class="icheck required" value="yes">
                                                    <label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?
                                                    </label>
                                                </div>

                                            </div><!-- /step 4-->

                                        </div><!-- /middle-wizard -->
                                        <div id="bottom-wizard">
                                            <button type="button" name="backward" class="backward">Backward </button>
                                            <button type="button" name="forward" class="forward">Forward</button>
                                            <button type="submit" name="process" class="submit">Submit</button>
                                        </div><!-- /bottom-wizard -->
                                    </form>
                                </div><!-- /Wizard container -->

                            </div><!-- /col -->
                        </div><!-- /row -->
                        </div><!-- /TAB 1:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
                    
                    <div class="tab-pane fade" id="tab_2">
                       <div class="subheader" id="about"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Welcome to Quote</h2>
                                <p class="lead">An mei sadipscing dissentiet, eos ea partem viderer facilisi. Brute nostrud democritum in vis, nam ei erat zril mediocrem. No postea diceret vix. Mei eu scripta dolorum voluptatibus, id omnes repudiare pri.</p>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box_feat" id="icon_1">
                                            <span></span>
                                            <h3>Responsive site design</h3>
                                            <p>Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box_feat" id="icon_2">
                                            <span></span>
                                            <h3>Web site check</h3>
                                            <p>Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel.</p>
                                        </div>
                                    </div>
                                    </div><!-- /row -->
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="box_feat" id="icon_3">
                                            <h3>Email campaigns</h3>
                                            <p>Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box_feat" id="icon_4">
                                            <h3>Seo optimization</h3>
                                            <p>Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel.</p>
                                        </div>
                                    </div>
                                </div><!-- /row -->
                                  <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="about_info">
                                                <i class="pe-7s-news-paper"></i>
                                                <h4>A brief about Quote<span>Suas summo id sed, erat erant oporteat cu pri.</span></h4>
                                                <p>Cum iusto nonumes dignissim ad, movet vocent ceteros nec ut. Eu putent utroque ius, ei usu purto doctus, ludus nostrud consectetuer ex pri. Maiorum petentium similique duo id. Sea ex nostro offendit, ius sumo electram theophrastus et. Nam eu dolore aliquid laoreet, ei eos tacimates assueverit inciderint. His deserunt recteque consequat in. Vis mucius virtute consequat ad, suavitate interesset an mei, oporteat temporibus at sea.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="about_info">
                                                <i class="pe-7s-light"></i>
                                                <h4>Mission<span>Suas summo id sed, erat erant oporteat cu pri.</span></h4>
                                                <p>Cum iusto nonumes dignissim ad, movet vocent ceteros nec ut. Eu putent utroque ius, ei usu purto doctus, ludus nostrud consectetuer ex pri. Maiorum petentium similique duo id. Sea ex nostro offendit, ius sumo electram theophrastus et. Nam eu dolore aliquid laoreet, ei eos tacimates assueverit inciderint. His deserunt recteque consequat in. Vis mucius virtute consequat ad, suavitate interesset an mei, oporteat temporibus at sea.</p>
                                            </div>
                                        </div>
                                    </div><!-- /row -->  
                            </div><!-- /col -->
                            
                          
                        </div><!-- /row -->
                    </div><!-- /TAB 2:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
                    
                    <div class="tab-pane fade" id="tab_3">
                        
                        <div id="map_contact">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d929.7287150035503!2d72.85818662918817!3d21.235224499117937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f153fc63919%3A0x825cef8b1bee1265!2sOEngines+Studio!5e0!3m2!1sen!2sin!4v1533195022928" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div><!-- /map -->
                        
                        <div id="contact_info">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="box_contact">
                                        <i class="pe-7s-map-marker"></i>
                                        <h4>Address</h4>
                                        <p>Duo magna vocibus electram ad. Sit an amet aeque legimus, paulo mnesarchum et mea, et pri quodsi singulis.</p>
                                        <p>11 Fifth Ave - New York, 45 001238 - USA</p>
                                        <a href="https://www.google.com/maps/dir//11+5th+Ave,+New+York,+NY+10003,+Stati+Uniti/@40.7322935,-73.9981148,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x89c25990b3af8bb9:0x854ae1d3553155!2m2!1d-73.9959261!2d40.7322935!3e0" class="btn_1" target="_blank">Get directions</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box_contact">
                                        <i class="pe-7s-mail-open-file"></i>
                                        <h4>Email and website</h4>
                                        <p>Duo magna vocibus electram ad. Sit an amet aeque legimus, paulo mnesarchum et mea, et pri quodsi singulis.</p>
                                        <p>
                                            <strong>Email:</strong> <a href="#0">support@domain.com</a><br>
                                            <strong>Website:</strong> <a href="#0">www.quote.com</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box_contact">
                                        <i class="pe-7s-call"></i>
                                        <h4>Telephone</h4>
                                        <p>Duo magna vocibus electram ad. Sit an amet aeque legimus, paulo mnesarchum et mea, et pri quodsi singulis.</p>
                                        <p>
                                            <strong>Tel:</strong> <a href="#0">+44 543 53433</a><br>
                                            <strong>Fax:</strong> <a href="#0">+44 543 5322</a>
                                        </p>
                                    </div>
                                </div>
                            </div><!-- / row-->
                            <hr>
                            <div id="social">
                                <ul>
                                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                                    <li><a href="#"><i class="icon-google"></i></a></li>
                                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                </ul>
                            </div><!-- /social -->
                        </div>
                    </div><!-- /TAB 3:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->

                </div><!-- /tab content -->
            </div><!-- /container-fluid -->
        </div><!-- /wrapper_in -->
    </div><!-- /main_container -->

    <div id="additional_links">
        <ul>
            <li>© 2018 Quote</li>
            <!-- <li><a href="https://goo.gl/iSDJf3" class="animated_link">Purchase this template</a></li>
            <li><a href="index.html" class="animated_link">Demo Video Bg</a></li>
            <li><a href="index_3.html" class="animated_link">With UPLOAD</a></li>
            <li><a href="index_4.html" class="animated_link">With Branch</a></li>
            <li><a href="index_5.html" class="animated_link">Full Page View</a></li>
            <li><a href="shortcodes.html" class="animated_link">Shortcodes</a></li> -->
        </ul>
    </div>

    <!-- SCRIPTS -->
    <!-- Jquery-->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <!-- Layer slider -->
    <script src="<?php echo base_url();?>layerslider/js/greensock.js"></script>
    <script src="<?php echo base_url();?>layerslider/js/layerslider.transitions.js"></script>
    <script src="<?php echo base_url();?>layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
    <script src="<?php echo base_url();?>js/slider_func.js"></script>
    <!-- Common script -->
    <script src="<?php echo base_url();?>js/common_scripts_min.js"></script>
    <!-- Theme script -->
    <script src="<?php echo base_url();?>js/functions.js"></script>
    <script>
        var ids = [];
        $('.iCheck-helper').click(function() {
            if($( ".icheckbox_square-yellow" ).hasClass( "checked" )){
                var sid = $(this).closest('div.form-group.checkbox_questions').attr('id');
                var id = parseInt(sid);
        
                console.log("IDS-:::::::",ids);
                console.log("SID:::::::",sid);
                
                var i = ids.indexOf(id);
                if(i > -1){
                    for (var i = 0; i < ids.length; i++) {
                        if(ids[i] == id){
                            ids.splice(i, 1); 
                        } 
                    }
                }
                else{
                    ids.push(id);
                }

                console.log("IDS:::::::",ids);

                $.ajax({
                    url: "http://development.oengines.com/quote/home/all_needs",
                    type: "POST",
                    data: {ids : ids}
                }).done(function(response) { 

                response = JSON.parse(response);
                console.log("RES::::::",response);
                    var i;
                    for (i = 0; i < response.data.all_need_id.length; i++) {
                        $(".bug_"+response.data.all_need_id[i].id).css("display", "none");
                        $(".qna_"+response.data.all_need_id[i].id).css("display", "none");
                    }
                    $(".bug_"+response.data.uniq_id).css("display", "block");
                    var j;
                    for (j = 0; j < ids.length; j++) {
                        $(".qna_"+ids[j]).css("display", "block");
                    }   
                });
                
   
            }
        });
    </script>
    <!-- sweetalert -->
    <script src="<?php echo base_url();?>js/sweetalert/sweetalert.min.js"></script>
    <script>
        function myFunction() {
          
            swal('success!', 'your message successfully sent', 'success');
                  
        }
    </script>
    <!-- Google map -->
    <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC-JeyaCifXWDrHeMVZZq4B3ZhB6SBHVPI"></script> -->
    <!-- <script src="<?php echo base_url();?>js/mapmarker.jquery.js"></script> -->
    <!-- <script src="<?php echo base_url();?>js/mapmarker_func.jquery.js"></script> -->
</body>

</html>