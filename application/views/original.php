<!DOCTYPE html>
<html lang="en">
   
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="_nK">
      <title><?php echo $title;?></title>
      <link rel="icon" type="image/png" href="images/favicon.png">
      <!-- START: Styles --><!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,700%7cMarcellus+SC" rel="stylesheet">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- FontAwesome -->
      <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
      <!-- IonIcons -->
      <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/navigation.css">
      <!-- Flickity -->
      <link rel="stylesheet" href="bower_components/flickity/dist/flickity.min.css">
      <!-- Photoswipe -->
      <link rel="stylesheet" type="text/css" href="bower_components/photoswipe/dist/photoswipe.css">
      <link rel="stylesheet" type="text/css" href="bower_components/photoswipe/dist/default-skin/default-skin.css">
      <!-- DateTimePicker -->
      <link rel="stylesheet" type="text/css" href="bower_components/datetimepicker/build/jquery.datetimepicker.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/navigation.css">
      <!-- Prism -->
      <link rel="stylesheet" type="text/css" href="bower_components/prism/themes/prism-tomorrow.css">
      <!-- Summernote -->
      <link rel="stylesheet" type="text/css" href="bower_components/summernote/dist/summernote.css">
      
      <link rel="stylesheet" href="css/oengines.min.css">
      <!-- Custom Styles -->
      <link rel="stylesheet" href="css/custom.css?v=23">
      <!-- END: Styles --><!-- jQuery --><script src="bower_components/jquery/dist/jquery.min.js">
        
      </script>
   </head>
   <!--
      Additional Classes:
          .nk-page-boxed
      -->
   <body>
      <!-- START: Page Preloader -->
      <!-- header content load -->
      <div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content hidden-lg-up">
         <div class="nano">
            <div class="nano-content">
               <a href="index-2.html" class="nk-nav-logo"><img src="images/logo.png" alt="" width="90"></a>
               <div class="nk-navbar-mobile-content">
                  <ul class="nk-nav">
                     <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- END: Navbar Mobile -->
      <div class="nk-main">
         <div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
            <div class="bg-image">
               <div style="background-image: url('images/image-3.jpg')"></div>
            </div>
            <div class="nk-header-table">
               <div class="nk-header-table-cell">
                  <div class="container">
                     <div class="col-md-8 offset-md-2">
                        <p class="lead">welcome</p>
                        </div>
                     <div class="nk-header-text">

                        <h1 class="myt display-3">OENGINES STUDIO</h1>
                        <div class="nk-gap-2"></div>
                        
                        <div class="nk-gap-4"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="mnt-80">
            <div id="rev_slider_50_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="photography-carousel48" style="padding:0px">
               <div id="rev_slider_50_1" class="rev_slider fullscreenbanner" style="display:none" data-version="5.0.7">
                  <ul>
                     <?php foreach ($banners as $key => $value) {?>
                        
                        <li data-index="rs-185" data-transition="slideoverhorizontal" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1500" data-thumb="<?php echo $value;?>" data-rotate="0" data-saveperformance="off">
                           <!-- MAIN IMAGE --><img src="<?php echo $value;?>" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        </li>

                     <?php }?>
                  </ul>
                  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important"></div>
               </div>
            </div>
         </div>
         <!-- END: Rev Slider --><!-- START: Features -->
         <div class="container">
            <div class="nk-gap-6"></div>
            <div class="nk-gap-2"></div>
            <div class="row vertical-gap lg-gap">
               <div class="col-md-4">
                  <div class="nk-ibox">
                     <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ios-game-controller-b"></span></div>
                     <div class="nk-ibox-cont">
                        <h2 class="nk-ibox-title">Incredible AI</h2>
                        There are amazing AI features available for our all games that gives to users smart and speedy reponse.
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="nk-ibox">
                     <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-fireball"></span></div>
                     <div class="nk-ibox-cont">
                        <h2 class="nk-ibox-title">Graphics</h2>
                        We are gives most powerful environment to our games and gives amazing User experience effects.
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="nk-ibox">
                     <div class="nk-ibox-icon nk-ibox-icon-circle"><span class="ion-ribbon-a"></span></div>
                     <div class="nk-ibox-cont">
                        <h2 class="nk-ibox-title">3 Awards</h2>
                        We are fill proud from google games that gives us smart rating and 5 out of 5 for our amazing graphics features. 
                     </div>
                  </div>
               </div>
            </div>
            <div class="nk-gap-2"></div>
            <div class="nk-gap-6"></div>
         </div>
         <!-- END: Features --><!-- START: About -->
         <div class="nk-box bg-dark-1">
            <div class="container text-center">
               <div class="nk-gap-6"></div>
               <div class="nk-gap-2"></div>
               <h2 class="nk-title h1">About The OE Game</h2>
               <div class="nk-gap-3"></div>
               <p class="lead">OEngines gives a game development , Applications and web developments from surat. We are gives games for a smartphones ,ipad,ipod and tablets using both Apple’s iOS and for Android operating systems. Since its founding in 2017, OEngines has brought many games to the market Ludo Knight, Othello king, Backgomman Pro, GinRummy Pro, 28 cards game and One Touch Drawing Master.</p>
               <div class="nk-gap-2"></div>
               <div class="row no-gutters">
                  <div class="col-md-4">
                     <div class="nk-box-2 nk-box-line">
                        <!-- START: Counter -->
                        <div class="nk-counter-3">
                           <div class="nk-count">65</div>
                           <h3 class="nk-counter-title h4">Unique Classes</h3>
                           <div class="nk-gap-1"></div>
                        </div>
                        <!-- END: Counter -->
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="nk-box-2 nk-box-line">
                        <!-- START: Counter -->
                        <div class="nk-counter-3">
                           <div class="nk-count">145</div>
                           <h3 class="nk-counter-title h4">Epic Bosses</h3>
                           <div class="nk-gap-1"></div>
                        </div>
                        <!-- END: Counter -->
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="nk-box-2 nk-box-line">
                        <!-- START: Counter -->
                        <div class="nk-counter-3">
                           <div class="nk-count">35</div>
                           <h3 class="nk-counter-title h4">Castles</h3>
                           <div class="nk-gap-1"></div>
                        </div>
                        <!-- END: Counter -->
                     </div>
                  </div>
               </div>
               <div class="nk-gap-2"></div>
               <div class="nk-gap-6"></div>
            </div>
         </div>
         <!-- END: About --><!-- START: Video -->
         <div class="container">
            <div class="nk-gap-6"></div>
            <div class="nk-gap-2"></div>
            <div class="row">
              <!--  <div class="col-md-8 offset-md-2">
                  <div class="nk-plain-video" data-video="https://youtu.be/kNKV0n3QG1U" data-video-thumb="images/image-3.jpg"></div>
               </div> -->
            </div>
            <div class="nk-gap-2"></div>
            <div class="nk-gap-6"></div>
         </div>
         
         <div class="nk-gap-6"></div>
         <div class="nk-gap-2"></div>
         <div class="nk-carousel-2" data-autoplay="12000" data-dots="true">
            <div class="nk-carousel-inner">
               <?php foreach ($employee as $key => $value) { ?>
                  <div>
                     <div>
                        <blockquote class="nk-testimonial-2">
                           <div class="nk-testimonial-photo" style="background-image: url('<?php echo $value['photo'];?>')"></div>
                           <div class="nk-testimonial-body"><em><?php echo $value['thought'];?></em></div>
                           <div class="nk-testimonial-name h4"><?php echo $value['name'];?></div>
                           <div class="nk-testimonial-source"><?php echo $value['work'];?></div>
                        </blockquote>
                     </div>
                  </div>
               <?php }?>
               
            </div>
         </div>
         <div class="nk-gap-2"></div>
         <div class="nk-gap-6"></div>
         <!-- END: Testimonials --><!-- START: Subscribe -->
         <div class="nk-box bg-dark-1">
            <div class="nk-gap-6"></div>
            <div class="nk-gap-2"></div>
            <div class="container">
               <div class="row">
                  <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                     <h2 class="nk-title text-center h1">Subscribe to our Newsletter</h2>
                     <div class="nk-gap-3"></div>
                     <!-- START: MailChimp Signup Form -->
                        <div class="input-group"><input type="email" value="" name="EMAIL" id="email" class="required email form-control" placeholder="Email *"> <button class="nk-btn nk-btn-lg link-effect-4" onclick="Subscribe();">Subscribe</button></div>
                        <div class="nk-form-response-success"></div>
                        <div class="nk-form-response-error"></div>
                        <small>We'll never share your email with anyone else.</small><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px" aria-hidden="true"><input type="text" name="b_d433160c0c43dcf8ecd52402f_7eafafe8f0" tabindex="-1" value=""></div>
                  </div>
               </div>
            </div>
            <div class="nk-gap-2"></div>
            <div class="nk-gap-6"></div>
            <div class="nk-gap-4"></div>
         </div>
         <!-- footer content load -->
      </div>
      <!--
         START: Share Buttons
             .nk-share-buttons-left
         -->
     <div class="nk-share-buttons nk-share-buttons-left hidden-sm-down">
         <ul>
            <li><span class="nk-share-icon" title="Share page on Facebook" data-share="facebook"><a href="https://www.facebook.com/oe.studio091/"><span class="icon fa fa-facebook"></span></a></span> <span class="nk-share-name">Facebook</span></li>
<!--            <li><span class="nk-share-icon" title="Share page on Twitter" data-share="twitter"><span class="icon fa fa-twitter"></span></span> <span class="nk-share-name">Twitter</span></li>-->
            <li><span class="nk-share-icon" title="Share page on Google+" data-share="google-plus"><a href="https://plus.google.com/u/2/116446153260416186360"> <span class="icon fa fa-google-plus"></span></a></span> <span class="nk-share-name">Google Plus</span></li>

         </ul>
      </div>
      <!--
         START: Side Buttons
             .nk-side-buttons-visible
         -->
      <div class="nk-side-buttons nk-side-buttons-visible">
         <ul>
            <li class="nk-scroll-top"><span class="nk-btn nk-btn-lg nk-btn-icon"><span class="icon ion-ios-arrow-up"></span></span></li>
         </ul>
      </div>
      <!-- END: Side Buttons --><!--
         START: Search
         
         Additional Classes:
             .nk-search-light
         -->
      <div class="nk-search">
         <div class="container">
            <form action="#">
               <fieldset class="form-group nk-search-field"><input type="text" class="form-control" id="searchInput" placeholder="Search..." name="s"><label for="searchInput"><i class="ion-ios-search"></i></label></fieldset>
            </form>
         </div>
      </div>
      <div class="nk-sign-form">
         <div class="nk-gap-5"></div>
         <div class="container">
            <div class="row">
               <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3">
                  <div class="nk-sign-form-container">
                     <div class="nk-sign-form-toggle h3"><a href="#" class="nk-sign-form-login-toggle active">Log In</a> <a href="#" class="nk-sign-form-register-toggle">Register</a></div>
                     <div class="nk-gap-2"></div>
                     <!-- START: Login Form -->
                     <form class="nk-sign-form-login active" action="#">
                        <input class="form-control" type="text" placeholder="Username or Email">
                        <div class="nk-gap-2"></div>
                        <input class="form-control" type="password" placeholder="Password">
                        <div class="nk-gap-2"></div>
                        <div class="form-check pull-left"><label class="form-check-label"><input type="checkbox" class="form-check-input"> Remember Me</label></div>
                        <button class="nk-btn nk-btn-color-white link-effect-4 pull-right">Log In</button>
                        <div class="clearfix"></div>
                        <div class="nk-gap-1"></div>
                        <a class="nk-sign-form-lost-toggle pull-right" href="#">Lost Password?</a>
                     </form>
                     <!-- END: Login Form --><!-- START: Lost Password Form -->
                     <form class="nk-sign-form-lost" action="#">
                        <input class="form-control" type="text" placeholder="Username or Email">
                        <div class="nk-gap-2"></div>
                        <button class="nk-btn nk-btn-color-white link-effect-4 pull-right">Get New Password</button>
                     </form>
                     <!-- END: Lost Password Form --><!-- START: Register Form -->
                     <form class="nk-sign-form-register" action="#">
                        <input class="form-control" type="text" placeholder="Username">
                        <div class="nk-gap-2"></div>
                        <input class="form-control" type="email" placeholder="Email">
                        <div class="nk-gap-2"></div>
                        <div class="pull-left">A password will be emailed to you.</div>
                        <button class="nk-btn nk-btn-color-white link-effect-4 pull-right">Register</button>
                     </form>
                     <!-- END: Register Form -->
                  </div>
               </div>
            </div>
         </div>
         <div class="nk-gap-5"></div>
      </div>
      <!-- END: Sign Form --><!-- START: Scripts --><!-- GSAP -->
      <script src="bower_components/gsap/src/minified/TweenMax.min.js"></script>
      <script src="bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script><!-- Bootstrap -->
      <script src="bower_components/tether/dist/js/tether.min.js"></script>
      <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script><!-- Sticky Kit -->
      <script src="bower_components/sticky-kit/dist/sticky-kit.min.js"></script><!-- Jarallax -->
      <script src="bower_components/jarallax/dist/jarallax.min.js"></script>
      <script src="bower_components/jarallax/dist/jarallax-video.min.js"></script><!-- imagesLoaded -->
      <script src="bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script><!-- Flickity -->
      <script src="bower_components/flickity/dist/flickity.pkgd.min.js"></script><!-- Isotope -->
      <script src="bower_components/isotope/dist/isotope.pkgd.min.js"></script><!-- Photoswipe -->
      <script src="bower_components/photoswipe/dist/photoswipe.min.js"></script>
      <script src="bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script><!-- Typed.js -->
      <script src="bower_components/typed.js/dist/typed.min.js"></script><!-- Jquery Form -->
      <script src="bower_components/jquery-form/dist/jquery.form.min.js"></script><!-- Jquery Validation -->
      <script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script><!-- Jquery Countdown + Moment -->
      <script src="bower_components/jquery.countdown/dist/jquery.countdown.min.js"></script>
      <script src="bower_components/moment/min/moment.min.js"></script>
      <script src="bower_components/moment-timezone/builds/moment-timezone-with-data.js"></script><!-- Hammer.js -->
      <script src="bower_components/hammer.js/hammer.min.js"></script><!-- NanoSroller -->
      <script src="bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script><!-- SoundManager2 -->
      <script src="bower_components/SoundManager2/script/soundmanager2-nodebug-jsmin.js"></script><!-- DateTimePicker -->
      <script src="bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js"></script><!-- Revolution Slider -->
      <script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
      <script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
      <script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
      <script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
      <script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script><!-- Keymaster -->
      <script src="bower_components/keymaster/keymaster.js"></script><!-- Summernote -->
      <script src="bower_components/summernote/dist/summernote.min.js"></script><!-- Prism -->
      <script src="bower_components/prism/prism.js"></script>
      <script src="js/oengines.min.js?v=23"></script>
      <script src="js/oengines-init.js?v=23"></script>
      <!-- END: Scripts -->
      <script type="text/javascript">
        var tpj=jQuery;
         var revapi50;
         tpj(document).ready(function() {
             if(tpj("#rev_slider_50_1").revolution == undefined){
                 revslider_showDoubleJqueryError("#rev_slider_50_1");
             }else{
                 revapi50 = tpj("#rev_slider_50_1").show().revolution({
                     sliderType:"carousel",
                     jsFileLocation:"plugins/revolution/js/",
                     sliderLayout:"auto",
                     dottedOverlay:"none",
                     delay:9000,
                     navigation: {
                         keyboardNavigation:"off",
                         keyboard_direction: "horizontal",
                         onHoverStop:"off",
                     },
                     carousel: {
                         maxRotation: 8,
                         vary_rotation: "off",
                         minScale: 20,
                         vary_scale: "off",
                         horizontal_align: "center",
                         vertical_align: "center",
                         fadeout: "off",
                         vary_fade: "off",
                         maxVisibleItems: 3,
                         infinity: "on",
                         space: -90,
                         stretch: "off"
                     },
                     responsiveLevels:[1240,1024,778,480],
                     gridwidth:[800,600,400,320],
                     gridheight:[600,400,320,280],
                     lazyType:"none",
                     shadow:0,
                     spinner:"off",
                     stopLoop:"on",
                     stopAfterLoops:0,
                     stopAtSlide:0,
                     shuffle:"off",
                     autoHeight:"off",
                     fullScreenAlignForce:"off",
                     fullScreenOffsetContainer: "",
                     fullScreenOffset: "",
                     disableProgressBar:"on",
                     hideThumbsOnMobile:"off",
                     hideSliderAtLimit:0,
                     hideCaptionAtLimit:0,
                     hideAllCaptionAtLilmit:0,
                     debugMode:false,
                     fallbacks: {
                         simplifyAll:"off",
                         nextSlideOnWindowFocus:"off",
                         disableFocusListener:false,
                     }
                 });
             }
         });

         function Subscribe(){
            
            $.ajax({
             url: "<?php echo site_url('home/subscribeNewsLetter');?>",
             type: 'POST',
             data: {email: $('#email').val()},
             success: function(data){
               console.log("sent :::::::::: ", data);
               return false;
             },
             error: function(err){
               console.log(err);
             }
           })
         }
      </script>
   </body>
   
</html>