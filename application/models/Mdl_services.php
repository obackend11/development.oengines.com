<?php


class Mdl_services extends CI_Model
{
	
	function get_games($array=''){

		$this->db->select('*');
		$data = $this->db->get('config');
        $value = stdToArray($data->result())[0];
        
		if($array){
			$data = array();
			foreach ($array as $key => $value) {
					
					$this->db->where('id', $value);
					$this->db->where('active_state', 1);
			$game = $this->db->get('www_games');
			$game = $game->result_array()[0];
			array_push($data, $game);
			}	
			return $data;
		}
				$this->db->limit($value['games_page_game']);
				$this->db->where('active_state', 1);
				$this->db->where('is_deleted', 0);
				$this->db->order_by('sort', 'asc');
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
	}

	function get_ses_games($array=''){
		if($array){
			$data = array();
			foreach ($array as $key => $value) {
					
					$this->db->where('id', $value);
					$this->db->where('active_state', 1);
			$game = $this->db->get('www_games');
			$game = $game->result_array()[0];
			array_push($data, $game);
			}	
			return $data;
		}else{
			$data = '';
			return $data;
		}
	}

	function get_single_game($id){
				$this->db->where(array('active_state' => 1,'id' => $id));
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
	}

	function rel_game($id){
				$this->db->where('id', $id);
		$data = $this->db->get('www_games');
		$data = $data->result_array();
		return $data;
	}

	function month($month){
		if($month == 1){
			$month = 'January';
		}elseif($month == 2){
			$month = 'February';
		}elseif($month == 3){
			$month = 'March';
		}elseif($month == 4){
			$month = 'April';
		}elseif($month == 5){
			$month = 'May';
		}elseif($month == 6){
			$month = 'June';
		}elseif($month == 7){
			$month = 'July';
		}elseif($month == 8){
			$month = 'August';
		}elseif($month == 9){
			$month = 'September';
		}elseif($month == 10){
			$month = 'October';
		}elseif($month == 11){
			$month = 'November';
		}elseif($month == 12){
			$month = 'December';
		}
	return $month;
	}

}