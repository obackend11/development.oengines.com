<!DOCTYPE html>
<html lang="en">
   
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="_nK">
      <title><?php echo $title;?></title>
      <link rel="icon" type="image/ico" href="<?php echo site_url();?>images/favicon.ico">
      <!-- START: Styles --><!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,700%7cMarcellus+SC" rel="stylesheet">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- FontAwesome -->
      <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
      <!-- IonIcons -->
      <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/navigation.css">
      <!-- Flickity -->
      <link rel="stylesheet" href="bower_components/flickity/dist/flickity.min.css">
      <!-- Photoswipe -->
      <link rel="stylesheet" type="text/css" href="bower_components/photoswipe/dist/photoswipe.css">
      <link rel="stylesheet" type="text/css" href="bower_components/photoswipe/dist/default-skin/default-skin.css">
      <!-- DateTimePicker -->
      <link rel="stylesheet" type="text/css" href="bower_components/datetimepicker/build/jquery.datetimepicker.min.css">
      <!-- Revolution Slider -->
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/settings.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/layers.css">
      <link rel="stylesheet" type="text/css" href="plugins/revolution/css/navigation.css">
      <!-- Prism -->
      <link rel="stylesheet" type="text/css" href="bower_components/prism/themes/prism-tomorrow.css">
      <!-- Summernote -->
      <link rel="stylesheet" type="text/css" href="bower_components/summernote/dist/summernote.css">
      <link rel="stylesheet" href="css/oengines.min.css">
      <!-- Custom Styles -->
      <link rel="stylesheet" href="css/custom.css?v=23">
      <!-- END: Styles --><!-- jQuery --><script src="bower_components/jquery/dist/jquery.min.js"></script>


   </head>
   <!--
      Additional Classes:
          .nk-page-boxed
      -->
   <body>
      <!-- header content load -->
      <div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content hidden-lg-up">
         <div class="nano">
            <div class="nano-content">
               <a href="<?php echo base_url();?>" class="nk-nav-logo"><img src="images/logo.png" alt="" width="90"></a>
               <div class="nk-navbar-mobile-content">
                  <ul class="nk-nav">
                     <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- END: Navbar Mobile -->
      <div class="nk-main">
         <!-- START: Header Title --><!--
            Additional Classes:
                .nk-header-title-sm
                .nk-header-title-md
                .nk-header-title-lg
                .nk-header-title-xl
                .nk-header-title-full
                .nk-header-title-parallax
                .nk-header-title-parallax-opacity
                .nk-header-title-boxed
            -->
         <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
            <div class="bg-image">
               <div style="background-image: url('images/image-3.jpg')"></div>
            </div>
            <div class="nk-header-table">
               <div class="nk-header-table-cell">
                  <div class="container">
                     <h1 class="myt"><?php echo $inner_title;?></h1>
                  </div>
               </div>
            </div>
         </div>
         <!-- END: Header Title -->
         <div class="nk-gap-4"></div>
         <div class="container">
            <div class="row">
               <div class="col-md-8 offset-md-2">
                  <h2>24X7 customer support</h2>
                  <p>We are always excited to hear from customers. Reach us at the following address with Product name and problem so that we can address the issue and can provide best product. We are providing 24X7 customer support at oengines.studio@gmail.com.</p>
               </div>
            </div>
         </div>
         <div class="nk-gap-4"></div>
         <!-- footer content load -->

         <footer class="nk-footer nk-footer-parallax nk-footer-parallax-opacity">
            <img class="nk-footer-top-corner" src="<?php echo site_url();?>images/footer-corner.png" alt="">
            <div class="container">
               <div class="nk-gap-2"></div>
                <div class="nk-footer-logos">
                  <a href="<?php echo base_url();?>" target="_blank"><img class="nk-img" src="<?php echo site_url();?>images/logo.png" alt="" width="120"></a> 
                  
                  
                  
                  
                </div>
               <div class="nk-gap"></div>
               <p>&copy; 2016 OE Studio Inc. Developed in association with companion. OEngines Studio designing and developing games. Create Innvative themes and make effect graphics with 2D & 3D design. All Rights Reserved.</p>
               <p>OE &reg; is a make amazing designs and develop singleplayer or multiplayers services for clients.</p>
              
               <div class="nk-gap-4"></div>
            </div>
         </footer>
      </div>
      <!--
         START: Share Buttons
             .nk-share-buttons-left
         -->
     <div class="nk-share-buttons nk-share-buttons-left hidden-sm-down">
         <ul>
            <li><span class="nk-share-icon" title="Share page on Facebook" data-share="facebook"><a href="https://www.facebook.com/oe.studio091/"><span class="icon fa fa-facebook"></span></a></span> <span class="nk-share-name">Facebook</span></li>
<!--            <li><span class="nk-share-icon" title="Share page on Twitter" data-share="twitter"><span class="icon fa fa-twitter"></span></span> <span class="nk-share-name">Twitter</span></li>-->
            <li><span class="nk-share-icon" title="Share page on Google+" data-share="google-plus"><a href="https://plus.google.com/u/2/116446153260416186360"> <span class="icon fa fa-google-plus"></span></a></span> <span class="nk-share-name">Google Plus</span></li>

         </ul>
      </div>
      <!--
         START: Side Buttons
             .nk-side-buttons-visible
         -->
      <div class="nk-side-buttons nk-side-buttons-visible">
         <ul>
            <li class="nk-scroll-top"><span class="nk-btn nk-btn-lg nk-btn-icon"><span class="icon ion-ios-arrow-up"></span></span></li>
         </ul>
      </div>
      
      
      <!-- END: Sign Form --><!-- START: Scripts --><!-- GSAP --><script src="bower_components/gsap/src/minified/TweenMax.min.js"></script><script src="bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script><!-- Bootstrap --><script src="bower_components/tether/dist/js/tether.min.js"></script><script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script><!-- Sticky Kit --><script src="bower_components/sticky-kit/dist/sticky-kit.min.js"></script><!-- Jarallax --><script src="bower_components/jarallax/dist/jarallax.min.js"></script><script src="bower_components/jarallax/dist/jarallax-video.min.js"></script><!-- imagesLoaded --><script src="bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script><!-- Flickity --><script src="bower_components/flickity/dist/flickity.pkgd.min.js"></script><!-- Isotope --><script src="bower_components/isotope/dist/isotope.pkgd.min.js"></script><!-- Photoswipe --><script src="bower_components/photoswipe/dist/photoswipe.min.js"></script><script src="bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script><!-- Typed.js --><script src="bower_components/typed.js/dist/typed.min.js"></script><!-- Jquery Form --><script src="bower_components/jquery-form/dist/jquery.form.min.js"></script><!-- Jquery Validation --><script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script><!-- Jquery Countdown + Moment --><script src="bower_components/jquery.countdown/dist/jquery.countdown.min.js"></script><script src="bower_components/moment/min/moment.min.js"></script><script src="bower_components/moment-timezone/builds/moment-timezone-with-data.js"></script><!-- Hammer.js --><script src="bower_components/hammer.js/hammer.min.js"></script><!-- NanoSroller --><script src="bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script><!-- SoundManager2 --><script src="bower_components/SoundManager2/script/soundmanager2-nodebug-jsmin.js"></script><!-- DateTimePicker --><script src="bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js"></script><!-- Revolution Slider --><script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script><script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script><script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script><script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script><script type="text/javascript" src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script><!-- Keymaster --><script src="bower_components/keymaster/keymaster.js"></script><!-- Summernote --><script src="bower_components/summernote/dist/summernote.min.js"></script><!-- Prism --><script src="bower_components/prism/prism.js"></script>
      <script src="js/oengines.min.js"></script><script src="js/oengines-init.js"></script><!-- END: Scripts -->
   </body>
   
</html>