<?php

/**
* 
*/
class Mdl_home extends CI_Model
{
	
	function savedata($arr){

		$_pdf = [];
		foreach ($_POST['needs'] as $key => $value) {
			$arr = explode(' ',trim($value));
			array_push($_pdf, $arr[0]);
		}
        $time = date('d-m-y H:i:s');
        $time = explode(":", $time);
        $time = implode("-", $time);
        $time = explode(" ", $time);
        $time = implode("_", $time);
        
		$pdf_name = implode("_",$_pdf).'_'.$time.'.pdf';
        // pr($pdf_name);die;
// ------------------// PDF //--------------------//

		 $fname = $_POST['firstname'];
		 $lname = $_POST['lastname'];
		 $email = $_POST['email'];
		 $mobileno = $_POST['mobileno'];
		 $country = $_POST['country'];
		 $budgets = $_POST['budgets'];
		 $a_text = $_POST['additional_text'];
		 $q1 = $_POST['q1'];
		 $a1 = $_POST['question1'];
		 $q2 = $_POST['q2'];
		 $a2 = $_POST['question2'];
		 $q3 = $_POST['q3'];
		 $a3 = $_POST['question3'];


         $pdf = new FPDF('P','mm','A4');
         $pdf->AddPage();

         $pdf->SetFont('Arial','B',68);
	     $pdf->SetTextColor(229,229,229);
	     $pdf->RotatedText(40,240,'OENGINES STUDIO',50);
	

		 $pdf->SetFillColor(242,242,242);
		 // $pdf->Rect(0, 0, 210, 297, 'F');		  
         $pdf->SetTextColor(140,0,0);
         $pdf->SetFont("Arial","B",24);
         $pdf->cell(0,20,"ESTIMATION","B",1,"C");

         $pdf->cell(0,7, "",0,1,"L");

        
         $pdf->SetFont("Arial","B",12);
         $pdf->SetTextColor(0,0,0);

         $pdf->cell(0,10,$fname." ".$lname,0,1,"R");
         $pdf->cell(0,10,$email,0,1,"R");
         $pdf->cell(0,10,$country,0,1,"R");
         $pdf->cell(0,10,"MO : ".$mobileno,0,1,"R");
         

         $pdf->cell(0,15,"",0,1,"C");

		 $pdf->cell(22,15,"Budgets :",0,0,"");
		 $pdf->SetFont("Arial","",12);
         $pdf->cell(0,15,$budgets,0,1,"");

         $pdf->SetFont("Arial","B",12);
         $pdf->cell(19,13,"Needs :",0,0,"");
         $arr = [];
         foreach ($_POST['needs'] as $key => $value) { 	
         	  array_push($arr, $value);
         }
         $need = implode(" , ", $arr);
         $pdf->SetFont("Arial","",12);
         $pdf->MultiCell(0,13,$need,0,1,"");

         $pdf->cell(0,2,"",0,1,"C");
         $pdf->SetFont("Arial","B",12);

         $pdf->cell(36,10,"Additional Text :",0,0,"");
         $pdf->SetFont("Arial","",12);
         $pdf->MultiCell(0,10,$a_text,0,1,"");   

         $pdf->cell(0,10,"",0,1,"C");
         $pdf->SetFont("Arial","B",12);

         $pdf->MultiCell(0,13,"Q1. {$q1}","T",1);
         $pdf->SetFont("Arial","",12);
         $pdf->SetTextColor(0,0,0);
         $pdf->MultiCell(0,12,"A1. {$a1}",0,1);

         
         $pdf->SetFont("Arial","B",12);
         $pdf->MultiCell(0,13,"Q2. {$q2}","T",1);
         $pdf->SetFont("Arial","",12);
         $pdf->SetTextColor(0,0,0);
         $pdf->MultiCell(0,12,"A2. {$a2}",0,1);

         
         $pdf->SetFont("Arial","B",12);
         $pdf->MultiCell(0,13,"Q3. {$q3}","T",1);
         $pdf->SetFont("Arial","",12);
         $pdf->SetTextColor(0,0,0);
         $pdf->MultiCell(0,12,"A3. {$a3}","B",1);

         $filename='uploads/pdf/'.$pdf_name;
         $pdf->output($filename,'F');
         // $pdf->output();
         // die;
// ------------------// END //--------------------//

// ------------------// MAIL //--------------------//

        $mail_to = array(TO,$_POST['email']);
        foreach ($mail_to as $key => $value) {
        	
        	$this->sendMail($filename,$value);

        }	
        
        // pr($mail);die;
// ------------------// END //--------------------//
        


		$data = array(
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'email' => $_POST['email'],
            'mobileno' => $_POST['mobileno'],
            'country' => $_POST['country']
        );

        $qa = array(
        	array('q1' => $_POST['q1'],'a1' => $_POST['question1']),
        	array('q2' => $_POST['q2'],'a2' => $_POST['question2']),
        	array('q3' => $_POST['q3'],'a3' => $_POST['question3'])
        );
       
        $quotes = array(
        	'budgets' => $_POST['budgets'],
            'needs' => $_POST['needs'],
            'additional_text' => $_POST['additional_text'],
            'qa' => $qa
        );
        
		$data['quotes'] = json_encode($quotes);
		$data['pdf'] = base_url().'uploads/pdf/'.$pdf_name;
		$data['id'] =  mt_rand(1, time());
		// pr($data);die();
		$this->db->insert('quotes',$data);
		$id = $this->db->affected_rows();
        return $id;
	}

	function sendMail($file_name,$value)
	{
		$message = "Quotation as encompass elements such as the services and estimated budget that are quoted, basic contact information, special trade agreements and discounts, and possible taxes and surcharges.";
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_user' => SMTP_USER, // change it to yours
		'smtp_pass' => SMTP_PASS, // change it to yours
		'mailtype' => 'html',
		'charset' => 'iso-8859-1',
		'wordwrap' => TRUE
		);

			
		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");
		    $this->email->from(FROM); // change it to yours
		    $this->email->to($value);// change it to yours
		    $this->email->subject('Project Quotations from OEQUOTES');
		    $this->email->message($message);
		    $this->email->attach($file_name);

		   
		    if($this->email->send())
		     {
		      	return true;
		     }
		    else
		     {
		      	show_error($this->email->print_debugger());
		     }    

	}

    function mdl_all_needs($ids){

        $this->db->select('id');
                 $this->db->where('is_deleted', 0);
        $query = $this->db->get('quote_poject_need');
        $data['all_need_id'] = $query->result_array();
     
        $check_limit = array();
        foreach ($ids as $key => $value) {
            
                $this->db->select('id, budget_limit');
                $this->db->where('id', $value);
        $query = $this->db->get('quote_poject_need');
        $count = explode("=", $query->result_array()[0]['budget_limit']);
        array_push($check_limit,$count[1]);

        }

        $unic = max($check_limit);
        foreach ($check_limit as $k => $v) {
            if($unic == $v){
                $data['uniq_id'] = $ids[$k];
            }
        }
        return $data;
    }

    function mdl_qna_req($arr){

        $data = array();
        for ($i=0; $i < count($arr); $i++) { 
            
                 $this->db->select('*');
                 $this->db->where('need_id', $arr[$i]);
                 $this->db->where('is_deleted', 0);
        $query = $this->db->get('quote_qna');
        array_push($data, $query->result_array());
        }
        return $data;

    }

    function all_need(){

                 $this->db->select('*');
                 $this->db->where('is_deleted', 0);
        $query = $this->db->get('quote_poject_need');
        return $query->result_array();
    }

    function all_budget(){

                 $this->db->select('*');
                 $this->db->where('is_deleted', 0);
        $query = $this->db->get('quote_budget');
        return $query->result_array();

    }

    function all_qna(){

                 $this->db->select('*');
                 $this->db->where('is_deleted', 0);
        $query = $this->db->get('quote_qna');
        return $query->result_array();

    }
}