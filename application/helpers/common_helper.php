<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pr'))
{
	function pr($array){
		echo "<pre>";
		print_r($array);
		echo "</pre>";

	}
}
if ( ! function_exists('stdToArray'))
{
	function stdToArray($obj){
	  $reaged = (array)$obj;
	  foreach($reaged as $key => &$field){
	    if(is_object($field))$field = stdToArray($field);
	  }
	  return $reaged;
	}
}

if( ! function_exists('sendResponse')){
	function sendResponse($data = array()){
		echo json_encode(array('error' => (count($data) > 0) ? 0 : 1, 'data' => (count($data) > 0) ? $data : array(), 'msg' => ''));
	}
}
