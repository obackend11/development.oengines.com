<!-- start footer.html-->
<footer id="sp-footer" class="sp-footer-fixed">
   <div class="sp-main-footer">
       <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
               <div class="title-block"><h4>Oengines Info</h4></div>
               <div class="text-block">© 2016 OE Studio Inc. Developed in association with companion. OEngines Studio designing and developing games. Create Innovative themes and make effective graphics with 2D & 3D design.</div>
               <div class="soclink-block">
                <div class="col-md-4">
                <ul>
                  <li><a style="color: #fff;font-size: 16px;" href="<?php echo base_url('home/faq') ;?>">- F.A.Q</a></li><br>
                  <li><a style="color: #fff;font-size: 16px;" href="<?php echo base_url('home/privacy') ;?>">- PRIVACY</a></li><br><br>
                
                 <li><a href="https://www.facebook.com/oe.studio091/" target="_blank"><i class="icon-facebook"></i></a></li>
                 <li><a href="https://play.google.com/store/apps/dev?id=7618782448745223948&hl=en" target="_blank"><i class="icon-ion-android-playstore"></i></a></li>
                 <li>
                    <a href="https://play.google.com/store/apps/dev?id=7618782448745223948&hl=en">
                      <img src="<?php echo base_url().'assets/images/app-store-apple-symbol.png'?>" width="25px;">
                    </a>
                 </li>
                  </ul>
                </div>  
               </div>
            </div>
            
         </div>
       </div>
   </div>
   <div class="sp-end-footer">
      <div class="container" style="display: block;">
         <div class="end-footer-block" align="center">
            © 2018 by <a href="#">Oengines Studio</a>. All rights reserved.
         </div>
      </div>
   </div>
</footer>
<div id="sp-footer-sizing-helper"></div>
<!-- end footer.html-->

<!-- JS assets -->
<!-- start page-footer-assets -->

<!-- end page-footer-assets -->
<script>
    var SOPRANO_FONTS = [
        'Montserrat:400,400i,500,500i,600,600i,700,700i',
        'Source Sans Pro:300,300i,400,400i,600,600i,700,700i',
        'Shadows Into Light:400'
    ];
</script>
<script src="<?php echo base_url();?>assets/scripts/modernizr.js"></script>
<script src="<?php echo base_url();?>assets/scripts/jquery.js"></script>
<script src="<?php echo base_url();?>assets/scripts/assets.js"></script>
<script src="<?php echo base_url();?>assets/scripts/bootstrap.js"></script>
<script type="text/javascript">
  
  $(document).ready(function(){
    
        $(".slick-dots").hide();

});


jQuery(document).ready(function($){
 
    var bArray = [];

    var sArray = [4,6,8,10];

    for (var i = 0; i < $('.bubbles').width(); i++) {
        bArray.push(i);
    }
   
    function randomValue(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }
 
    setInterval(function(){
         
        var size = randomValue(sArray);
        
        $('.bubbles').append('<div class="individual-bubble" style="left: ' + randomValue(bArray) + 'px; width: ' + size + 'px; height:' + size + 'px;"></div>');
         
        $('.individual-bubble').animate({
            'bottom': '100%',
            'opacity' : '-=0.7'
        }, 3000, function(){
            $(this).remove()
        }
        );
 
    }, 350);
 
});
</script>
<script src="<?php echo base_url();?>assets/scripts/controllers.js?v=3"></script>
</body>

</html>