<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=1'?>">


<!-- start page-content -->
<br><br><br>
<section class="sp-section text-center pb0" id="portfolio">
    <div class="sp-portfolio-block inner">
        <div class="container">
            <ul class="sp-portfolio-sorting">
                <li class="active"><a href="#" data-group="all">All</a></li>
                <?php foreach ($tags as $key => $value) { ?>
                    <li><a href="#" data-group="<?php echo $key;?>"><?php echo $value;?></a></li>
                <?php }?>
            </ul>
        </div>
        <div class="container">
            <div class="row sp-portfolio-items">
                
                <?php 
                if(isset($e)){
                    foreach ($e as $key => $value) { ?>
                        
                        <div class="col-sm-6 col-xs-12" data-groups="[&quot;e&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($corp)){
                    foreach ($corp as $key => $value) { ?>
                        
                        <div class="col-sm-6 col-xs-12" data-groups="[&quot;corp&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($android)){

                    foreach ($android as $key => $value) { ?>
                    
                    <div class="col-sm-6 col-xs-12" data-groups="[&quot;android&quot;]">
                        <div class="sp-portfolio-item">
                            <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                <div class="portfolio-hover">
                                    <h5 class="title"><?php echo $value['title'];?></h5>
                                    <span class="desc"><?php echo $value['content'];?></span>
                                    <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>


                <?php } 

                }?>

                <?php 
                if(isset($ios)){
                    foreach ($ios as $key => $value) { ?>
                        
                        <div class="col-sm-6 col-xs-12" data-groups="[&quot;ios&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($hybrid)){
                    foreach ($hybrid as $key => $value) { ?>
                        
                        <div class="col-sm-6 col-xs-12" data-groups="[&quot;hybrid&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($unity)){
                    foreach ($unity as $key => $value) { ?>
                        
                        <div class="col-sm-6 col-xs-12" data-groups="[&quot;unity&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($icon)){
                    foreach ($icon as $key => $value) { ?>
                        
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-xs-12" data-groups="[&quot;icon&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: transparent;">
                                    <!-- <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                    </div> -->
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>

                <?php 
                if(isset($photoshop)){
                    foreach ($photoshop as $key => $value) { ?>
                        
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-xs-12" data-groups="[&quot;photo&quot;]">
                            <div class="sp-portfolio-item">
                                <a href="<?php echo $value['link'];?>" target="_blank" style="background-image: url(<?php echo $value['img'];?>); background-size: contain;background-color: #170a04;">
                                    <div class="portfolio-hover">
                                        <h5 class="title"><?php echo $value['title'];?></h5>
                                        <span class="desc"><?php echo $value['content'];?></span>
                                        <span class="link"><i class="icon-ion-ios-arrow-thin-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }
                }?>
            </div>
        </div>
    </div>
</section>
<!-- end page-content -->
</div>
