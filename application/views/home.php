<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/home.css?v=2'?>">
<style type="text/css">
  .android{
    background-image: url(assets/images/android.png);
  }
  .ios{
    background-image: url(assets/images/ios.png);
  }
  .back-sing{
    background-image: url(../../../assets/images/background_repeat.png); 
  }
  .back{
    background-image: url(../../assets/images/background_repeat.png); 
  }
  
    .container41{
        padding: 2% 10% !important;
    }
    .coming_pad{
        padding: 40px 10px !important;
    }
  @media screen and (max-width: 768px) {
    .coming_cont{
        margin-top: -75px !important;
    }
    .coming{
        font-size: 24px !important;
        margin-bottom: -20px !important;
    }
    .mobile_font{
        font-size: 30px !important;
    }
    .mobile_img{
        height: 170px !important;
    }
    .container41{
        padding: 2% 0% !important;
    }
    .sp-section_mobile {
        padding-top: 10px !important;
    }
    .typed-container{
        font-size: 30px !important;
    }
    .mobile_services{
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
    #sp-header-inner{
        padding: 0px 0% !important;
    }
    .coming_pad{
        padding: 20px 10px !important;
    }
    .coming_vid{
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
    .mar_bot{
        margin-bottom: -20px !important;
    }
    .find-more2{
        padding: 2px !important;
    }
    .find-more-a{
        font-size: 16px !important;
        padding: 30px !important;
    }
    .sp-section.little{
        padding-top: 50px !important;
    }
    .mobile_oe_pro_con{
        padding: 0px 0% !important;
    }
    .sp-title-block{
        padding: 30px 0px !important;
    }
    .mobile_font2{
        font-size: 24px !important;
    }
    .mobile_br{
        display: none !important;
    }
    .mobile_services_con{
        padding-top: 10px !important;
    }
    .mobile_content{
        height: 60px !important;
        overflow: hidden !important;
    }
    {
        padding-top: 10px !important;
   pd10  }
  }
</style>
<!---- srtart content ------->

<section class="sp-intro sp-intro-image fullscreen" data-background="http://more.oengines.com/uploads/www/slider/wwwslider1.jpg">
    <div class="intro-body">
        <br><br><br><br><br>
        <h2 class="intro-title intro-title-1"><span data-typed-str="Oengines Studio|Game Developments|Web Developments" data-typed-repeat="yes" data-typed-cursor="yes"></span></h2>

        <p style="font-size:20px;" class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"><?php echo 
        $configdata['slider_content'];?></p>
        <br> <br>
        <div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
            <div class="btn">
                <a target="_blank" href="https://play.google.com/store/search?q=OEngines&c=apps">
                    <button class="btn btn-sm btn-primary device" style="background-image: url('<?php echo base_url();?>/assets/images/android.png');width:200px;background-size:auto;height:64px;border-radius:8px"></button>
                </a>
            </div>
            <div class="btn">
                <a target="_blank" href="https://play.google.com/store/search?q=OEngines&c=apps">
                    <button class="btn btn-sm btn-primary device" style="background-image: url('<?php echo base_url();?>/assets/images/ios.png');width:200px;background-size:auto;height:64px;border-radius:8px"></button>
                </a>
            </div>
        </div> 
        <br><br><br>
        <div class="no-mobile">
        <?php if(isset($all_img)){ ?>
        <div class="container">
           <section class="customer-logos slider">
            <?php foreach ($all_img as $key6 => $value6) { ?>

              <div class="slide"><img class="border" src="<?php echo $value6['icon_img'] ?>"></div>

            <?php } ?>  
           </section>
        </div>
        <?php } ?>      
        </div>
    </div>
</section>
 
<section class="sp-section sp-section_mobile mar_bot text-center little bg-color-light pd10" id="sp-about">
    <div class="container">
        <div class="sp-title-block line">
            <h1 class="mobile_font" style="font-weight: 700;">Welcome  To  Oengines  Studio</h1>
        </div>

        <div class="row">
            <div class="col-md-10 offset-md-1 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                <p>Oengines is a Game Development studio with speciliazation that provides a web developments, game developments, ionic and cordova developments, android and ios developments, web and graphics design (UX|UI), Unity 3D and nodejs developments with key fetures of our experienced persons.Oengines Gives passionate response time accordance AWS hosting or full stack developments.</p>
                <br>
                <div class="btn btn-primary find-more2"><a class="find-more-a" href="<?php echo base_url('Home/about') ?>">FIND MORE</a></div>
            </div>
        </div>
    </div>
</section>


<section class="sp-section" style="padding-bottom:0px;padding-top:30px;">
    <div class="container coming_cont">
        <div class="row">
            <div class="col-md-12">
                <center><h1 class="coming" style="color:#C3AC6D;font-weight:700;margin-bottom:50px;">Coming Soon Games</h1></center>
            </div>
            <div class="col-md-6 col-xs-12 coming_pad">
                <div class="sp-portfolio-list">
                    <ul>
                        <li><h1><?php echo $videodata['name'];?></h1></li>
                    </ul>
                </div>
                <br>
                <p><?php echo $videodata['description'];?></p>
                
            </div>
            <div class="col-md-6 col-xs-12 wow fadeInRight animated coming_vid" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInRight;">
                <iframe src="https://www.youtube.com/embed/<?php echo $videodata['video'];?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" style="border: 4px solid #c3ac6d;border-radius:4px;width:100%;height:100%;"></iframe>
            </div>
        </div>
    </div>
</section>


<?php if($games['data']){ ?>
<section class="sp-section text-center little">
    <div class="container-fluid mobile_oe_pro_con" style="background:#f5f5f5">
        <div class="sp-title-block">
            <center><h1 class="mobile_font2" style="font-weight: 700;margin-bottom:10px;"><span style="color:#C3AC6D;"> OENGINES </span> PRODUCTS</h1></center>
            <center><div style="border:2px solid #C3AC6D;width:40%;"></div></center>
        </div>
    </div> 
    <div class="fetchdata">

        <div class="container41 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s" style="animation-delay: 2s;background-color:#f5f5f5;">
            <div class="col-md-12">
                <a href="<?php echo base_url().'home/services/games/'.$games['data'][0]['id'] ;?>">    
                    <center><h1 style="font-weight: 700;margin-bottom:10px;"><span style="color:#C3AC6D;font-size:40px;"> <?php echo $games['data'][0]['title'] ;?> </span></h1></center>  
                    <center><h6 class="mobile_content" style="font-weight: 500;margin-bottom:10px;"><span style="color:#000;"> <?php echo $games['data'][0]['content'] ;?> </span></h6></center> 
                    <br> 
                    <img class="border2 mobile_img" src="<?php echo $games['data'][0]['img'] ;?>" class="img-responsive" style="background-size:cover;width:100%;border-radius:10px;">
             
                    <div class="no-mobile">
                        <div class="container" style="width: 75%;">
                           <section class="customer-screen slider" style="background-color:#f5f5f5;padding: 30px 10px;margin-top:-150px;border-radius: 10px;">
                            <?php  
                                 $img7 = explode(",", $games['data'][0]['index_img']);
                                 foreach ($img7 as $key7 => $value7) {
                            ?>

                              <div class="slide"><img class="border2" src="<?php echo 'http://more.oengines.com/uploads/screen/'.$value7 ;?>"></div>

                            <?php } ?>  
                           </section>
                        </div>     
                    </div>
                </a>

                <div class="col-md-12">
                    <?php if($games['data'][0]['link_android'] != ''){ ?>
                    <div class="btn">
                        <div class="sp-blog-read"><a target="_blank" href="<?php echo $games['data'][0]['link_android'] ;?>"><button class="btn btn-sm btn-primary device" style="background-image: url('<?php echo base_url();?>/assets/images/android.png');width: 200px;background-size: auto;height: 64px;"></button></a></div>
                    </div>
                    <?php } ?>
                    <?php if($games['data'][0]['link_ios'] != ''){ ?>  
                    <div class="btn">
                        <div class="sp-blog-read"><a target="_blank" href="<?php echo $games['data'][0]['link_ios'] ?>"><button class="btn btn-sm btn-primary device" style="background-image: url('<?php echo base_url();?>/assets/images/ios.png');width: 200px;background-size: auto;height: 64px;"></button></a></div>
                    </div>
                    <?php } ?> 
                </div>

            </div>
        </div>
    </div>  
    <br><br class="mobile_br"><br class="mobile_br">

    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                <div class="btn btn-primary find-more2 show_more"><a class="find-more-a2" href="javascript:;">LOAD MORE GAMES</a></div>
            </div>
        </div>
    </div>
    <br><br><br class="mobile_br">
    
</section>
<?php } ?>

<section class="sp-section text-center bg-color-light mobile_services_con" id="sp-services" style="margin-top:-80px;background-color:#f5f5f5">
    <div class="container">
        <div class="sp-title-block line">
            <h3>Our Services</h3>
            <span>great experience</span>
        </div>
 
        <div class="sp-services-container row wow sequenced" data-wow-duration="2s">
            <div class="col-md-4 col-sm-6 col-xs-12 mobile_services" data-wow-duration="2s">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-speedometer-outline"></i></div>
                    <div class="title"><h4>Professional Code</h4></div>
                    <p style="font-size:16px !important;">Gets Perfect Maintain consistent variations while using numerous variables with optimized data.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mobile_services" data-wow-duration="2s">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-lightbulb-outline"></i></div>
                    <div class="title"><h4>Clean & Modern Design</h4></div>
                    <p style="font-size:16px !important;">Great Design is about more then good-looking and experienced futures products. Power to shape how we things.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mobile_services" data-wow-duration="2s">
                <div class="sp-services-block">
                    <div class="icon"><i class="icon-ion-ios-heart-outline"></i></div>
                    <div class="title"><h4>We Love Our Clients</h4></div>
                    <p style="font-size:16px !important;">General windows effects not are drawing man. Common indeed garden you his ladies out preference imprudence.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sp-section sp-section_mobile" id="sp-magic" style="background-color:#fff">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-5 col-md-6">
                <div class="sp-title-block"><h3>We have magic</h3></div>
                <p>Wholesome this oh alas adequately that and a queerly more the until far wolverine some rhythmically ludicrously above closed and sardonic felicitously activated where invaluable far zebra reverent much</p>
            </div>
            <div class="col-xl-6 col-lg-7 col-md-6 text-right sp-marg50 wow sequenced fx-fadeInRight">
                <div data-value="0.95" class="sp-circle">
                    <div class="sp-circle-text">game development</div>
                    <span></span>
                </div>

                <div data-value="0.98" class="sp-circle">
                    <div class="sp-circle-text">game design</div>
                    <span></span>
                </div>

                <div data-value="0.75" class="sp-circle">
                    <div class="sp-circle-text">web design</div>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <script type="text/javascript">
$(document).ready(function(){
    var count = 0;
    $(document).on('click','.show_more',function(){
        var ID = Math.round(new Date().getTime() + (Math.random() * 100));
        count += 1;
        console.log("IDDD:::",ID);
        console.log("count:::",count);
        $.ajax({
            type:'POST',
            url: "<?php echo base_url() ?>home/games",
            data:{id:ID, con:count},
            success:function(html){
                console.log("ID::::",html);
                
                $('.fetchdata').append(html).delay(5000);

                var uid = $('.uniquecount'+count).val();
                if(uid == 1){
                   $('.show_more').hide();
                }

                $('.customer-screen-'+ID).slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1500,
                arrows: false,
                dots: false,
                pauseOnHover: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
            });
            }
        });
    });
});
</script> -->
<script type="text/javascript">
$(document).ready(function(){
    var count = 0;    
    var endpoint = 0;
    var startpoint = 1;
    $(document).on('click','.show_more',function(){
      var url = "<?php echo base_url() ?>home/getindexgamelist/"+startpoint;
      var curl = "<?php echo base_url(); ;?>"
      console.log("url :::: ", url);
        $.ajax({
            type:'POST',
            url: url,
            success:function(data){
                
              data = JSON.parse(data);
              console.log("DATA::::::",data);
              var html = '';
              if(data.error == 'N'){

                var con = count++;
                console.log("CON:::::",con);
                var ak = [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49];
                if($.inArray(con, ak) > -1){
                    var color = "#f5f5f5";
                }else{
                    var color = "#fff";
                }

                for (var i = 0; i < data.data.length; i++) {

                var index_img = data.data[i].index_img.split(",");
                console.log("IIIIIII::::::",index_img);
              
                    html='<div class="container41 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s" style="animation-delay:2s;background-color:'+color+';">'
                      +'<div class="col-md-12">'
                      +'<a href="'+curl+'/home/services/games/'+data.data[i].id+'">'
                      +'<center><h1 style="font-weight:700;margin-bottom:10px;"><span style="color:#C3AC6D;font-size:40px;">'+data.data[i].title+'</span></h1></center> '
                      +'<center><h6 class="mobile_content" style="font-weight:500;margin-bottom:10px;"><span style="color:#000;">'+data.data[i].content+'</span></h6></center><br>'
                      +'<img class="border2 mobile_img" src="'+data.data[i].img+'" class="img-responsive" style="background-size:cover;width:100%;border-radius:10px;">'
                      +'<div class="no-mobile">'
                      +'<div class="container" style="width:75%;">'
                      +'<section class="customer-screen-'+data.data[i].id+' slider" style="background-color:'+color+';padding: 30px 10px;margin-top:-150px;border-radius: 10px;">';

                    for (var img = 0; img < index_img.length; img++) {
                    html+='<div class="slide"><img class="border2" src="http://more.oengines.com/uploads/screen/'+index_img[img]+'"></div>';
                    }

                    html+='</section></div></div>'
                      +'</a>'
    
                      +'<div class="col-md-12">';
                      if(data.data[i].link_android){
                    html+='<div class="btn"><div class="sp-blog-read"><a target="_blank" href="'+data.data[i].link_android+'">'
                      +'<button class="btn btn-sm btn-primary device android" style="width: 200px;background-size:auto;height:64px;">'
                      +'</button></a></div></div>';
                      }
                      if(data.data[i].link_ios){
                    html+='<div class="btn"><div class="sp-blog-read"><a target="_blank" href="'+data.data[i].link_ios+'">'
                      +'<button class="btn btn-sm btn-primary device ios" style="width: 200px;background-size:auto;height:64px;">'
                      +'</button></a></div></div>'
                      }
                    html+='</div>'
                      +'</div>'
                      +'</div>';


                    $('.fetchdata').append(html).fadeIn(1000).show('slow');
                    // $('.customer-screen-'+data.data[i].id).hide();
                    // $('.customer-screen-'+data.data[i].id).show('slow');  


                    $('.customer-screen-'+data.data[i].id).slick({
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 1500,
                        arrows: false,
                        dots: false,
                        pauseOnHover: false,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 4
                            }
                        }, {
                            breakpoint: 520,
                            settings: {
                                slidesToShow: 3
                            }
                        }]
                    });

                }

            
              }else{
                $('.show_more').hide();
              }

              if(data.isendpoint == 1){
                $('.show_more').show();
              }else{
                $('.show_more').hide();
              }

              startpoint = data.startpoint;

            },
            error: function(err){
              console.log(err);
            }
        });
    });
});

</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('.customer-screen').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>
</div>