<?php

/**
* 
*/
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Common');
      $this->load->model('mdl_home');
      require_once APPPATH.'libraries/fpdf/fpdf.php';
      // require_once APPPATH.'libraries/fpdf/rotation.php';
	}

	public function index()
	{	

      if($_POST){
         
         $data['id'] = $this->mdl_home->savedata($_POST);
         $this->load->view('index',$data); 

      }else{

         $data['needs'] = $this->mdl_home->all_need();
         $data['budget'] = $this->mdl_home->all_budget();
         $data['qna'] = $this->mdl_home->all_qna();

         $this->load->view('index',$data);   

      }
		
	}

   public function all_needs(){
      

      $all_needs = $this->mdl_home->mdl_all_needs($_POST['ids']);
      echo json_encode(
           array('error' => (boolean)false,
                 'data' => $all_needs
           )
       );
   }

 	public function sendContact(){
		echo "string";
		$this->Common->notify();
	}

	public function subscribeNewsLetter(){
		 $from_email = "oengines.studio@gmail.com"; 
         $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Oengines Studio Subscriber'); 
         $this->email->to($to_email);
         $this->email->subject('Oengines Studio Help'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()) {
         	echo json_encode('true');
         }else {
         	echo json_encode('false');
         }
         
	}

	public function send_mail() { 
		
         $from_email = "oengines.studio@gmail.com"; 
         $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Oengines Studio Support'); 
         $this->email->to($to_email);
         $this->email->subject('Oengines Studio Help'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()) {
         	echo json_encode('true');
         }else {
         	echo json_encode('false');
         }
         // redirect(base_url());
      } 
}