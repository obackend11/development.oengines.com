<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/styles/services.css?v=1'?>">


<!-- start page-content -->
<br><br><br>
<section id="sp-contact" class="sp-section" style="padding-bottom:50px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="sp-contacts-list">
                    <ul>
                        <li><i class="icon-ion-ios-location-outline"></i><b> Address:</b> 423, 4th floor Amby-Vally Arcade, Nr Manisha Garnala, Opp Santosa Heights, Utran, Surat-394105, Gujarat, India.</li>
                        <li><i class="icon-ion-ios-telephone-outline"></i><b> Phone:</b> +91-903-355-7485</li>
                        <li><i class="icon-ion-ios-email-outline"></i><b> E-mail:</b> contact@oengines.com</li>
                    </ul>
                </div>
                <br>
                <div class="col-md-12">
                    <center><button class="btn btn-primary btn-lg"><a  style="font-size:22px;font-weight: 700;letter-spacing:1px;color:#fff" href="<?php echo base_url('quote') ?>">Oengines Quotes</a></button></center>
                </div>
                <p></p>
            </div>
            
            <div class="col-lg-5 offset-lg-1 col-md-6 col-sm-12" style="display: none;">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="cf-name">Your Name</label>
                        <input class="form-control" id="cf-name" name="name" type="text" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="cf-email">Your E-mail</label>
                        <input class="form-control" id="cf-email" name="email" type="email" placeholder="Your E-mail">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="cf-phone">Your Phone</label>
                        <input class="form-control" id="cf-phone" name="phone" type="tel" placeholder="Your Phone">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="cf-message">Your Message</label>
                        <textarea class="form-control" id="cf-message" name="message" rows="4" placeholder="Your Message"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="sp-map" class="sp-section none">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d929.7287150035503!2d72.85818662918817!3d21.235224499117937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f153fc63919%3A0x825cef8b1bee1265!2sOEngines+Studio!5e0!3m2!1sen!2sin!4v1533195022928" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<!-- end page-content -->
</div>